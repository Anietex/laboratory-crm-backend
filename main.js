(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/MiniToast.ts":
/*!**************************!*\
  !*** ./src/MiniToast.ts ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var minitoast = /** @class */ (function () {
    function minitoast() {
        /**
         * Timeout for the notifications
         */
        this.notif = {
            timeout: 5000
        };
        /**
         * Default messages  & colors for the popup
         * Colors courtesy of https://flatuicolors.com/
         *
         * s - Success
         * w - Warning
         * e - Error
         * i - Information
         * d - Default
         *
         * Array Props
         * [Default Message, Message Heading, Classname]
         */
        this.msgs = {
            s: ['', 'Success', 'mt-success'],
            w: ['', 'Warning', 'mt-warning'],
            e: ['', 'Error', 'mt-error'],
            i: ['', 'Info', 'mt-info'],
            d: ['', 'Notification', 'mt-default']
        };
        this.init();
    }
    /**
     * Gets/Creates the base notification container
     */
    minitoast.prototype.init = function () {
        var cont = document.getElementById('mt-cont');
        if (!cont) {
            //Set some base styles
            cont = document.createElement('div');
            cont.id = 'mt-cont';
            cont.classList.add('mt-cont');
            document.body.appendChild(cont);
        }
        this.cont = cont;
    };
    /**
     * success message
     * @param {string|null} msg
     */
    minitoast.prototype.success = function (msg) {
        if (msg === void 0) { msg = null; }
        this.append(msg || this.msgs.s[0], 's');
    };
    /**
     * warning message
     * @param {string|null} msg
     */
    minitoast.prototype.warning = function (msg) {
        if (msg === void 0) { msg = null; }
        this.append(msg || this.msgs.w[0], 'w');
    };
    /**
     * error message
     * @param {string|null} msg
     */
    minitoast.prototype.error = function (msg) {
        if (msg === void 0) { msg = null; }
        this.append(msg || this.msgs.e[0], 'e');
    };
    /**
     * info message
     * @param {string|null} msg
     */
    minitoast.prototype.info = function (msg) {
        if (msg === void 0) { msg = null; }
        this.append(msg || this.msgs.i[0], 'i');
    };
    /**
     * default message
     * @param {string|null} msg
     */
    minitoast.prototype.default = function (msg) {
        if (msg === void 0) { msg = null; }
        this.append(msg || this.msgs.d[0], 'd');
    };
    minitoast.prototype.append = function (msg, type) {
        //Get the options for the notification container
        var tstOpts = this.notif;
        //Get the options for messages
        var msgOpts = this.msgs;
        //Create the toast element
        var tst = document.createElement('div');
        tst.classList.add(msgOpts[type][2], 'mt-notif', 'mt-slide-fade');
        tst.style.animationDuration = (tstOpts.timeout / 1000) + 's';
        //Create the heading, and the message containers, insert the text, and then append them into the notification div
        var headingP = document.createElement('p');
        headingP.innerText = msgOpts[type][1];
        tst.appendChild(headingP);
        var messageP = document.createElement('p');
        messageP.innerText = msg;
        tst.appendChild(messageP);
        //Finally insert it into the main notification container, and then set up the timeout to remove it again
        document.getElementById('mt-cont').appendChild(tst);
        setTimeout(function () {
            document.getElementById('mt-cont').removeChild(document.getElementById('mt-cont').firstChild);
        }, tstOpts.timeout);
    };
    return minitoast;
}());
/* harmony default export */ __webpack_exports__["default"] = (minitoast);


/***/ }),

/***/ "./src/app/Auth/auth-interceptor.ts":
/*!******************************************!*\
  !*** ./src/app/Auth/auth-interceptor.ts ***!
  \******************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.service */ "./src/app/Auth/auth.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(auth) {
        this.auth = auth;
    }
    AuthInterceptor.prototype.intercept = function (req, next) {
        // Get the auth token from the service.
        var authToken = this.auth.getToken();
        // Clone the request and replace the original headers with
        // cloned headers, updated with the authorization.
        var authReq = req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' + authToken)
                .set('X-Requested-With', 'XMLHttpRequest')
        });
        // send cloned request with header to the next handler.
        return next.handle(authReq);
    };
    AuthInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_0__["AuthService"]])
    ], AuthInterceptor);
    return AuthInterceptor;
}());



/***/ }),

/***/ "./src/app/Auth/auth.guard.ts":
/*!************************************!*\
  !*** ./src/app/Auth/auth.guard.ts ***!
  \************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/Auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (this.authService.isLogin()) {
            return true;
        }
        else {
            this.router.navigateByUrl('/login');
            return false;
        }
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/Auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/Auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-jwt */ "./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_jwt__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var AuthService = /** @class */ (function () {
    function AuthService(appUrl, http) {
        this.appUrl = appUrl;
        this.http = http;
        this.jwtHelper = new angular2_jwt__WEBPACK_IMPORTED_MODULE_2__["JwtHelper"]();
        this.onLogIn = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    AuthService.prototype.login = function (formData) {
        return this.http.post(this.appUrl + 'login', formData);
    };
    AuthService.prototype.setToken = function (token) {
        localStorage.setItem('auth_token', token);
    };
    AuthService.prototype.getToken = function () {
        return localStorage.getItem('auth_token');
    };
    AuthService.prototype.setUser = function (user) {
        localStorage.setItem('auth_user', JSON.stringify(user));
        this.onLogIn.emit();
    };
    AuthService.prototype.getUser = function () {
        return JSON.parse(localStorage.getItem('auth_user'));
    };
    AuthService.prototype.isLogin = function () {
        if (this.getToken()) {
            return !this.jwtHelper.isTokenExpired(this.getToken());
        }
        else {
            return false;
        }
    };
    AuthService.prototype.isAdmin = function () {
        return this.getUser() ? this.getUser().admin == 1 : false;
    };
    AuthService.prototype.logout = function () {
        return this.http.get(this.appUrl + 'logout');
    };
    AuthService.prototype.clearAuth = function () {
        localStorage.removeItem('auth_token');
        localStorage.removeItem('auth_user');
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/ManageLabs/add-lab/add-lab.component.html":
/*!***********************************************************!*\
  !*** ./src/app/ManageLabs/add-lab/add-lab.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  add-lab works!\n</p>\n"

/***/ }),

/***/ "./src/app/ManageLabs/add-lab/add-lab.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/ManageLabs/add-lab/add-lab.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/ManageLabs/add-lab/add-lab.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/ManageLabs/add-lab/add-lab.component.ts ***!
  \*********************************************************/
/*! exports provided: AddLabComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddLabComponent", function() { return AddLabComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AddLabComponent = /** @class */ (function () {
    function AddLabComponent() {
    }
    AddLabComponent.prototype.ngOnInit = function () {
    };
    AddLabComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-lab',
            template: __webpack_require__(/*! ./add-lab.component.html */ "./src/app/ManageLabs/add-lab/add-lab.component.html"),
            styles: [__webpack_require__(/*! ./add-lab.component.scss */ "./src/app/ManageLabs/add-lab/add-lab.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AddLabComponent);
    return AddLabComponent;
}());



/***/ }),

/***/ "./src/app/ManageLabs/edit-lab/edit-lab.component.html":
/*!*************************************************************!*\
  !*** ./src/app/ManageLabs/edit-lab/edit-lab.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  edit-lab works!\n</p>\n"

/***/ }),

/***/ "./src/app/ManageLabs/edit-lab/edit-lab.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/ManageLabs/edit-lab/edit-lab.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/ManageLabs/edit-lab/edit-lab.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/ManageLabs/edit-lab/edit-lab.component.ts ***!
  \***********************************************************/
/*! exports provided: EditLabComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditLabComponent", function() { return EditLabComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EditLabComponent = /** @class */ (function () {
    function EditLabComponent() {
    }
    EditLabComponent.prototype.ngOnInit = function () {
    };
    EditLabComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-lab',
            template: __webpack_require__(/*! ./edit-lab.component.html */ "./src/app/ManageLabs/edit-lab/edit-lab.component.html"),
            styles: [__webpack_require__(/*! ./edit-lab.component.scss */ "./src/app/ManageLabs/edit-lab/edit-lab.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EditLabComponent);
    return EditLabComponent;
}());



/***/ }),

/***/ "./src/app/ManageLabs/lab-form/lab-form.component.html":
/*!*************************************************************!*\
  !*** ./src/app/ManageLabs/lab-form/lab-form.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  lab-form works!\n</p>\n"

/***/ }),

/***/ "./src/app/ManageLabs/lab-form/lab-form.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/ManageLabs/lab-form/lab-form.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/ManageLabs/lab-form/lab-form.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/ManageLabs/lab-form/lab-form.component.ts ***!
  \***********************************************************/
/*! exports provided: LabFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LabFormComponent", function() { return LabFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LabFormComponent = /** @class */ (function () {
    function LabFormComponent() {
    }
    LabFormComponent.prototype.ngOnInit = function () {
    };
    LabFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-lab-form',
            template: __webpack_require__(/*! ./lab-form.component.html */ "./src/app/ManageLabs/lab-form/lab-form.component.html"),
            styles: [__webpack_require__(/*! ./lab-form.component.scss */ "./src/app/ManageLabs/lab-form/lab-form.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LabFormComponent);
    return LabFormComponent;
}());



/***/ }),

/***/ "./src/app/ManageLabs/lab-list/lab-list.component.html":
/*!*************************************************************!*\
  !*** ./src/app/ManageLabs/lab-list/lab-list.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  lab-list works!\n</p>\n"

/***/ }),

/***/ "./src/app/ManageLabs/lab-list/lab-list.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/ManageLabs/lab-list/lab-list.component.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/ManageLabs/lab-list/lab-list.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/ManageLabs/lab-list/lab-list.component.ts ***!
  \***********************************************************/
/*! exports provided: LabListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LabListComponent", function() { return LabListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LabListComponent = /** @class */ (function () {
    function LabListComponent() {
    }
    LabListComponent.prototype.ngOnInit = function () {
    };
    LabListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-lab-list',
            template: __webpack_require__(/*! ./lab-list.component.html */ "./src/app/ManageLabs/lab-list/lab-list.component.html"),
            styles: [__webpack_require__(/*! ./lab-list.component.scss */ "./src/app/ManageLabs/lab-list/lab-list.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LabListComponent);
    return LabListComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<app-header></app-header>\n<app-sidebar></app-sidebar>\n<main>\n    <div class=\"page-header\">\n        <router-outlet></router-outlet>\n    </div>\n</main>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _settings_setting_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settings/setting.service */ "./src/app/settings/setting.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(setting) {
        this.setting = setting;
    }
    AppComponent.prototype.ngOnInit = function () {
        if (!this.setting.getLocalSettings()) {
        }
    };
    AppComponent.prototype.getSettings = function () {
        var _this = this;
        this.setting.getSettings().subscribe(function (data) {
            _this.setting.saveLocalSettings(data);
        }, function (error) {
            // this.ngOnInit();
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_settings_setting_service__WEBPACK_IMPORTED_MODULE_1__["SettingService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _materialize_design_materialize_design_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./materialize-design/materialize-design.module */ "./src/app/materialize-design/materialize-design.module.ts");
/* harmony import */ var _assets_js_minitoast__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../assets/js/minitoast */ "./src/assets/js/minitoast.js");
/* harmony import */ var _assets_js_minitoast__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_assets_js_minitoast__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _node_modules_materialize_css_dist_js_materialize__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../node_modules/materialize-css/dist/js/materialize */ "./node_modules/materialize-css/dist/js/materialize.js");
/* harmony import */ var _node_modules_materialize_css_dist_js_materialize__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_node_modules_materialize_css_dist_js_materialize__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _node_modules_toastr_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../node_modules/toastr/toastr */ "./node_modules/toastr/toastr.js");
/* harmony import */ var _node_modules_toastr_toastr__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_node_modules_toastr_toastr__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./sidebar/sidebar.component */ "./src/app/sidebar/sidebar.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _title_panel_title_panel_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./title-panel/title-panel.component */ "./src/app/title-panel/title-panel.component.ts");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _route_app_routing_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./route/app-routing.module */ "./src/app/route/app-routing.module.ts");
/* harmony import */ var _shared_report_basic_info_report_basic_info_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/report-basic-info/report-basic-info.component */ "./src/app/shared/report-basic-info/report-basic-info.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _radiological_request_radiological_request_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./radiological-request/radiological-request.component */ "./src/app/radiological-request/radiological-request.component.ts");
/* harmony import */ var _haematology_haemtology_reports_haematology_reports_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./haematology/haemtology-reports/haematology-reports.component */ "./src/app/haematology/haemtology-reports/haematology-reports.component.ts");
/* harmony import */ var _haematology_haematology_report_view_haematology_report_view_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./haematology/haematology-report-view/haematology-report-view.component */ "./src/app/haematology/haematology-report-view/haematology-report-view.component.ts");
/* harmony import */ var _printed_page_printed_page_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./printed-page/printed-page.component */ "./src/app/printed-page/printed-page.component.ts");
/* harmony import */ var _shared_loading_loading_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/loading/loading.component */ "./src/app/shared/loading/loading.component.ts");
/* harmony import */ var _chemical_pathology_chemical_pathology_reports_chemical_pathology_reports_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component */ "./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.ts");
/* harmony import */ var _chemical_pathology_chemical_pathology_edit_chemical_pathology_edit_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component */ "./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.ts");
/* harmony import */ var _chemical_pathology_chemical_pathology_view_chemical_pathology_view_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./chemical-pathology/chemical-pathology-view/chemical-pathology-view.component */ "./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.ts");
/* harmony import */ var _medical_microbiology_medical_microbiology_reports_medical_microbiology_reports_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component */ "./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.ts");
/* harmony import */ var _medical_microbiology_medical_microbiology_view_medical_microbiology_view_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./medical-microbiology/medical-microbiology-view/medical-microbiology-view.component */ "./src/app/medical-microbiology/medical-microbiology-view/medical-microbiology-view.component.ts");
/* harmony import */ var _radiological_request_list_radiological_request_list_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./radiological-request-list/radiological-request-list.component */ "./src/app/radiological-request-list/radiological-request-list.component.ts");
/* harmony import */ var _radiological_request_edit_radiological_request_edit_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./radiological-request-edit/radiological-request-edit.component */ "./src/app/radiological-request-edit/radiological-request-edit.component.ts");
/* harmony import */ var _radiological_request_view_radiological_request_view_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./radiological-request-view/radiological-request-view.component */ "./src/app/radiological-request-view/radiological-request-view.component.ts");
/* harmony import */ var _patient_create_patient_create_patient_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./patient/create-patient/create-patient.component */ "./src/app/patient/create-patient/create-patient.component.ts");
/* harmony import */ var _patient_patient_form_patient_form_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./patient/patient-form/patient-form.component */ "./src/app/patient/patient-form/patient-form.component.ts");
/* harmony import */ var _shared_linear_loading_linear_loading_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./shared/linear-loading/linear-loading.component */ "./src/app/shared/linear-loading/linear-loading.component.ts");
/* harmony import */ var _shared_connection_error_connection_error_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./shared/connection-error/connection-error.component */ "./src/app/shared/connection-error/connection-error.component.ts");
/* harmony import */ var _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./patient/patient-list/patient-list.component */ "./src/app/patient/patient-list/patient-list.component.ts");
/* harmony import */ var _shared_server_error_server_error_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./shared/server-error/server-error.component */ "./src/app/shared/server-error/server-error.component.ts");
/* harmony import */ var _patient_edit_patient_edit_patient_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./patient/edit-patient/edit-patient.component */ "./src/app/patient/edit-patient/edit-patient.component.ts");
/* harmony import */ var _shared_no_record_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./shared/no-record.component */ "./src/app/shared/no-record.component.ts");
/* harmony import */ var _haematology_create_report_create_report_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./haematology/create-report/create-report.component */ "./src/app/haematology/create-report/create-report.component.ts");
/* harmony import */ var _haematology_report_form_report_form_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./haematology/report-form/report-form.component */ "./src/app/haematology/report-form/report-form.component.ts");
/* harmony import */ var _haematology_edit_report_edit_report_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./haematology/edit-report/edit-report.component */ "./src/app/haematology/edit-report/edit-report.component.ts");
/* harmony import */ var _chemical_pathology_create_chemical_pathology_create_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./chemical-pathology/create-chemical-pathology/create-chemical-pathology.component */ "./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.ts");
/* harmony import */ var _chemical_pathology_chemical_pathology_form_chemical_pathology_form_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./chemical-pathology/chemical-pathology-form/chemical-pathology-form.component */ "./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.ts");
/* harmony import */ var _medical_microbiology_create_medical_microbiology_create_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./medical-microbiology/create-medical-microbiology/create-medical-microbiology.component */ "./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.ts");
/* harmony import */ var _medical_microbiology_medical_microbiology_form_medical_microbiology_form_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./medical-microbiology/medical-microbiology-form/medical-microbiology-form.component */ "./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.ts");
/* harmony import */ var _medical_microbiology_edit_medical_microbiology_edit_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component */ "./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var _ManageLabs_add_lab_add_lab_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./ManageLabs/add-lab/add-lab.component */ "./src/app/ManageLabs/add-lab/add-lab.component.ts");
/* harmony import */ var _ManageLabs_edit_lab_edit_lab_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./ManageLabs/edit-lab/edit-lab.component */ "./src/app/ManageLabs/edit-lab/edit-lab.component.ts");
/* harmony import */ var _ManageLabs_lab_form_lab_form_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./ManageLabs/lab-form/lab-form.component */ "./src/app/ManageLabs/lab-form/lab-form.component.ts");
/* harmony import */ var _ManageLabs_lab_list_lab_list_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./ManageLabs/lab-list/lab-list.component */ "./src/app/ManageLabs/lab-list/lab-list.component.ts");
/* harmony import */ var _manage_labs_manage_labs_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./manage-labs/manage-labs.component */ "./src/app/manage-labs/manage-labs.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _Auth_auth_interceptor__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! ./Auth/auth-interceptor */ "./src/app/Auth/auth-interceptor.ts");
/* harmony import */ var _patient_patient_log_patient_log_component__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(/*! ./patient/patient-log/patient-log.component */ "./src/app/patient/patient-log/patient-log.component.ts");
/* harmony import */ var _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(/*! ./shared/modal/modal.component */ "./src/app/shared/modal/modal.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













// import { HaematologyComponent } from './haematology/haematology.component';




// import { MedicalMicrobiologyComponent } from './medical-microbiology/medical-microbiology.component';




// import { HaematologyEditComponent } from './haematology-edit/haematology-edit.component';





// import { MedicalMicrobiologyEditComponent } from './medical-microbiology-edit/medical-microbiology-edit.component';































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"],
                _sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_9__["SidebarComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_10__["HeaderComponent"],
                _title_panel_title_panel_component__WEBPACK_IMPORTED_MODULE_11__["TitlePanelComponent"],
                _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_12__["DashboardComponent"],
                // HaematologyComponent,
                _shared_report_basic_info_report_basic_info_component__WEBPACK_IMPORTED_MODULE_14__["ReportBasicInfoComponent"],
                // MedicalMicrobiologyComponent,
                _radiological_request_radiological_request_component__WEBPACK_IMPORTED_MODULE_16__["RadiologicalRequestComponent"],
                _haematology_haemtology_reports_haematology_reports_component__WEBPACK_IMPORTED_MODULE_17__["HaematologyReportsComponent"],
                _haematology_haematology_report_view_haematology_report_view_component__WEBPACK_IMPORTED_MODULE_18__["HaematologyReportViewComponent"],
                _printed_page_printed_page_component__WEBPACK_IMPORTED_MODULE_19__["PrintedPageComponent"],
                // HaematologyEditComponent,
                _shared_loading_loading_component__WEBPACK_IMPORTED_MODULE_20__["LoadingComponent"],
                _chemical_pathology_chemical_pathology_reports_chemical_pathology_reports_component__WEBPACK_IMPORTED_MODULE_21__["ChemicalPathologyReportsComponent"],
                _chemical_pathology_chemical_pathology_edit_chemical_pathology_edit_component__WEBPACK_IMPORTED_MODULE_22__["ChemicalPathologyEditComponent"],
                _chemical_pathology_chemical_pathology_view_chemical_pathology_view_component__WEBPACK_IMPORTED_MODULE_23__["ChemicalPathologyViewComponent"],
                _medical_microbiology_medical_microbiology_reports_medical_microbiology_reports_component__WEBPACK_IMPORTED_MODULE_24__["MedicalMicrobiologyReportsComponent"],
                // MedicalMicrobiologyEditComponent,
                _medical_microbiology_medical_microbiology_view_medical_microbiology_view_component__WEBPACK_IMPORTED_MODULE_25__["MedicalMicrobiologyViewComponent"],
                _radiological_request_list_radiological_request_list_component__WEBPACK_IMPORTED_MODULE_26__["RadiologicalRequestListComponent"],
                _radiological_request_edit_radiological_request_edit_component__WEBPACK_IMPORTED_MODULE_27__["RadiologicalRequestEditComponent"],
                _radiological_request_view_radiological_request_view_component__WEBPACK_IMPORTED_MODULE_28__["RadiologicalRequestViewComponent"],
                _patient_create_patient_create_patient_component__WEBPACK_IMPORTED_MODULE_29__["CreatePatientComponent"],
                _patient_patient_form_patient_form_component__WEBPACK_IMPORTED_MODULE_30__["PatientFormComponent"],
                _shared_linear_loading_linear_loading_component__WEBPACK_IMPORTED_MODULE_31__["LinearLoadingComponent"],
                _shared_connection_error_connection_error_component__WEBPACK_IMPORTED_MODULE_32__["ConnectionErrorComponent"],
                _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_33__["PatientListComponent"],
                _shared_server_error_server_error_component__WEBPACK_IMPORTED_MODULE_34__["ServerErrorComponent"],
                _patient_edit_patient_edit_patient_component__WEBPACK_IMPORTED_MODULE_35__["EditPatientComponent"],
                _shared_no_record_component__WEBPACK_IMPORTED_MODULE_36__["NoRecordComponent"],
                _haematology_create_report_create_report_component__WEBPACK_IMPORTED_MODULE_37__["HaematologyCreateReport"],
                _haematology_report_form_report_form_component__WEBPACK_IMPORTED_MODULE_38__["ReportFormComponent"],
                _haematology_edit_report_edit_report_component__WEBPACK_IMPORTED_MODULE_39__["EditReportComponent"],
                _chemical_pathology_create_chemical_pathology_create_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_40__["CreateChemicalPathologyComponent"],
                _chemical_pathology_chemical_pathology_form_chemical_pathology_form_component__WEBPACK_IMPORTED_MODULE_41__["ChemicalPathologyFormComponent"],
                _medical_microbiology_create_medical_microbiology_create_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_42__["CreateMedicalMicrobiologyComponent"],
                _medical_microbiology_medical_microbiology_form_medical_microbiology_form_component__WEBPACK_IMPORTED_MODULE_43__["MedicalMicrobiologyFormComponent"],
                _medical_microbiology_edit_medical_microbiology_edit_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_44__["EditMedicalMicrobiologyComponent"],
                _settings_settings_component__WEBPACK_IMPORTED_MODULE_45__["SettingsComponent"],
                _ManageLabs_add_lab_add_lab_component__WEBPACK_IMPORTED_MODULE_46__["AddLabComponent"],
                _ManageLabs_edit_lab_edit_lab_component__WEBPACK_IMPORTED_MODULE_47__["EditLabComponent"],
                _ManageLabs_lab_form_lab_form_component__WEBPACK_IMPORTED_MODULE_48__["LabFormComponent"],
                _ManageLabs_lab_list_lab_list_component__WEBPACK_IMPORTED_MODULE_49__["LabListComponent"],
                _manage_labs_manage_labs_component__WEBPACK_IMPORTED_MODULE_50__["ManageLabsComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_51__["LoginComponent"],
                _patient_patient_log_patient_log_component__WEBPACK_IMPORTED_MODULE_53__["PatientLogComponent"],
                _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_54__["ModalComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _route_app_routing_module__WEBPACK_IMPORTED_MODULE_13__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _materialize_design_materialize_design_module__WEBPACK_IMPORTED_MODULE_4__["MaterializeDesignModule"]
            ],
            providers: [{ provide: 'API_URL', useValue: "http://localhost/laboratory/api/" },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HTTP_INTERCEPTORS"], useClass: _Auth_auth_interceptor__WEBPACK_IMPORTED_MODULE_52__["AuthInterceptor"], multi: true }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.service.ts":
/*!**************************************!*\
  !*** ./src/app/auth/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular2-jwt */ "./node_modules/angular2-jwt/angular2-jwt.js");
/* harmony import */ var angular2_jwt__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angular2_jwt__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var AuthService = /** @class */ (function () {
    function AuthService(appUrl, http) {
        this.appUrl = appUrl;
        this.http = http;
        this.jwtHelper = new angular2_jwt__WEBPACK_IMPORTED_MODULE_2__["JwtHelper"]();
        this.onLogIn = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    AuthService.prototype.login = function (formData) {
        return this.http.post(this.appUrl + 'login', formData);
    };
    AuthService.prototype.setToken = function (token) {
        localStorage.setItem('auth_token', token);
    };
    AuthService.prototype.getToken = function () {
        return localStorage.getItem('auth_token');
    };
    AuthService.prototype.setUser = function (user) {
        localStorage.setItem('auth_user', JSON.stringify(user));
        this.onLogIn.emit();
    };
    AuthService.prototype.getUser = function () {
        return JSON.parse(localStorage.getItem('auth_user'));
    };
    AuthService.prototype.isLogin = function () {
        if (this.getToken()) {
            return !this.jwtHelper.isTokenExpired(this.getToken());
        }
        else {
            return false;
        }
    };
    AuthService.prototype.isAdmin = function () {
        return this.getUser() ? this.getUser().admin == 1 : false;
    };
    AuthService.prototype.logout = function () {
        return this.http.get(this.appUrl + 'logout');
    };
    AuthService.prototype.clearAuth = function () {
        localStorage.removeItem('auth_token');
        localStorage.removeItem('auth_user');
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/chemical-pathology/abstract-chemical-pathology-component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/chemical-pathology/abstract-chemical-pathology-component.ts ***!
  \*****************************************************************************/
/*! exports provided: AbstractChemicalPathologyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractChemicalPathologyComponent", function() { return AbstractChemicalPathologyComponent; });
/* harmony import */ var _chemical_pathology_form_chemical_pathology_form_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./chemical-pathology-form/chemical-pathology-form.component */ "./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AbstractChemicalPathologyComponent = /** @class */ (function () {
    function AbstractChemicalPathologyComponent() {
        this.saving = false;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
    }
    AbstractChemicalPathologyComponent.prototype.resetForm = function () {
        this.reportForm.reset();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_chemical_pathology_form_chemical_pathology_form_component__WEBPACK_IMPORTED_MODULE_0__["ChemicalPathologyFormComponent"]),
        __metadata("design:type", Object)
    ], AbstractChemicalPathologyComponent.prototype, "reportForm", void 0);
    return AbstractChemicalPathologyComponent;
}());



/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Chemical Pathology Edit\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/chemical-pathology/reports']\">Chemical Pathology reports</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n   <app-linear-loading [showLoading]=\"saving\"></app-linear-loading>\n\n   <connection-error [show]=\"conError\" [retryCb]=\"getReport\"></connection-error>\n    <server-error [show]=\"serverError\" [retryCb]=\"getReport\"></server-error>\n    <app-loading [showLoading]=\"loadingReport\" loadingInfo=\"Loading report data please wait\" ></app-loading>\n   <chemical-pathology-form [saving]=\"saving\"  [report]='report' *ngIf=\"!loadingReport && !conError && !serverError\"   (submit)=\"onSubmit($event)\" saveBtnText=\"Update Report\"></chemical-pathology-form>\n</div>\n"

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: ChemicalPathologyEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChemicalPathologyEditComponent", function() { return ChemicalPathologyEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _abstract_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../abstract-chemical-pathology-component */ "./src/app/chemical-pathology/abstract-chemical-pathology-component.ts");
/* harmony import */ var _chemical_pathology_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../chemical-pathology.service */ "./src/app/chemical-pathology/chemical-pathology.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChemicalPathologyEditComponent = /** @class */ (function (_super) {
    __extends(ChemicalPathologyEditComponent, _super);
    function ChemicalPathologyEditComponent(serviceProvider, route) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.route = route;
        _this.id = null;
        _this.conError = false;
        _this.serverError = false;
        _this.loadingReport = false;
        return _this;
    }
    ChemicalPathologyEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params.id;
            _this.getReport();
        });
    };
    ChemicalPathologyEditComponent.prototype.onSubmit = function (report) {
        var _this = this;
        this.saving = true;
        this.report = __assign({}, this.report, report);
        report.id = this.id;
        this.serviceProvider.updateReport(report).subscribe(function (data) {
            _this.saving = false;
            _this.toast.success('Report was updated successfully');
        }, function (error) {
            _this.saving = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else {
                _this.toast.error("An error occurred report could not be updated");
            }
        });
    };
    ChemicalPathologyEditComponent.prototype.getReport = function () {
        var _this = this;
        this.loadingReport = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReport(this.id).subscribe(function (report) {
            var keys = Object.keys(report);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                if (report[key] === null || report[key] == 'null') {
                    report[key] = '';
                }
                else if (report[key] === '') {
                    report[key] = '';
                }
            }
            _this.report = report;
            console.log(report);
            _this.loadingReport = false;
        }, function (err) {
            _this.loadingReport = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    ChemicalPathologyEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chemical-pathology-edit',
            template: __webpack_require__(/*! ./chemical-pathology-edit.component.html */ "./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.html"),
            styles: [__webpack_require__(/*! ./chemical-pathology-edit.component.scss */ "./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.scss")]
        }),
        __metadata("design:paramtypes", [_chemical_pathology_service__WEBPACK_IMPORTED_MODULE_2__["ChemicalPathologyService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], ChemicalPathologyEditComponent);
    return ChemicalPathologyEditComponent;
}(_abstract_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_1__["AbstractChemicalPathologyComponent"]));



/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-panel report-panel near-loading\">\n\t<form [formGroup]=\"chemPathForm\">\n\t\t<section [ngClass]=\"{'active':activePane===0}\" class=\"input-section animated fadeIn\">\n\t\t\t<h5 class=\"card-title\">Basic Test information</h5>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t <select (change)=\"onPatientSelected($event)\" class=\"validate\"  formControlName=\"patient_id\" required>\n\t\t\t\t\t\t \t<option value=\"\" ></option>\n\t\t\t\t\t\t \t\n\t\t\t\t\t\t     <option value=\"new\">Add new patient</option>\n\t\t\t\t\t\t     <option disabled *ngIf=\"loadingPatients\">Loading patients please wait</option>\n\t\t\t\t\t\t     <option value=\"retry\"  *ngIf=\"errorOccurred\" >An error occurred, click here to retry loading patients</option>\n\t\t\t\t\t\t\t <option *ngFor=\"let patient of patients\" value=\"{{patient.id}}\">{{patient.first_name+' '+patient.middle_name+' '+patient.surname }}</option>}\n\t\t\t\t\t\t\toption\n\t\t\t\t\t    </select>\n    \t\t\t\t\t<label>Select patient</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"requested-by\" formControlName=\"requested_by\" class=\"validate\" required>\n\t\t\t\t\t\t<label for=\"requested-by\">Requested By</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"clinical-detail\" formControlName=\"clinical_detail\">\n\t\t\t\t\t\t<label for=\"clinical-detail\">Clinical Detail</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"exam-required\" formControlName=\"exam_required\">\n\t\t\t\t\t\t<label for=\"exam-required\">Exam Required</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"specimen\" formControlName=\"specimen\">\n\t\t\t\t\t\t<label for=\"specimen\">Specimen</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"clinic\" formControlName=\"clinic\">\n\t\t\t\t\t\t<label for=\"clinic\">Clinic</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t\t<section [ngClass]=\"{'active':activePane===1}\" class=\"input-section animated fadeIn\">\n                    <h6>LIPID PROFILE</h6>\n                    <div class=\"row\">\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"cholesterol\" formControlName=\"total_cholesterol\">\n                                <label for=\"cholesterol\">Total cholesterol(0 - 5.2 mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"triglyceride\" formControlName=\"triglyceride\">\n                                <label for=\"triglyceride\">Triglyceride(0-2.3mm01/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"hdlm\" formControlName=\"hdlm\">\n                                <label for=\"hdlm\">HDLM (>0.9mmo1/L) f(>1.5mm01/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"vldl\" formControlName=\"vldl\">\n                                <label for=\"vldl\">VLDL(0.5-0.60mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"ldl\" formControlName=\"ldl\">\n                                <label for=\"ldl\">LDL (0 - 4.11mm01/ml)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"eucr\" formControlName=\"eucr\">\n                                <label for=\"eucr\">E/U/Cr</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\" >\n                                <input type=\"text\" id=\"creatinene\" formControlName=\"creatinine\">\n                                <label for=\"creatinene\">Creatinene(58-110um01/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"urea\" formControlName=\"urea\">\n                                <label for=\"urea\">Urea(2.8 - 7.2mm01/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"sodium\" formControlName=\"sodium\">\n                                <label for=\"sodium\">Sodium(132 - 145mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"potassium\" formControlName=\"potassium\">\n                                <label for=\"potassium\">Potassium(3.2 - 5.0 mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"chloride\" formControlName=\"chloride\">\n                                <label for=\"chloride\">Chloride(96 - 108mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"bicarbonate\" formControlName=\"bicarbonate\">\n                                <label for=\"bicarbonate\">Bicarbonate(22 - 88mmo1/L)</label>\n                            </div>\n                        </div>\n                    </div>\n        </section>\n        <section [ngClass]=\"{'active':activePane===2}\" class=\"input-section animated fadeIn\">\n                    <h6>Hormonal Profile</h6>\n                    <div class=\"row\">\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"fsh\" formControlName=\"fsh\">\n                                <label for=\"fsh\">FSH</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"lh\" formControlName=\"lh\">\n                                <label for=\"lh\">LH</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"prolactin\" formControlName=\"prolactin\">\n                                <label for=\"prolactin\">Prolactin</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"progesterone\" formControlName=\"progesteron\">\n                                <label for=\"progesterone\">Progesterone</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"estradiol\" formControlName=\"estradol\">\n                                <label for=\"estradiol\">Estradiol</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"psa\" formControlName=\"psa\">\n                                <label for=\"psa\">PSA (0-4ng/ml)</label>\n                            </div>\n                        </div>\n                    </div>\n        </section>\n        <section [ngClass]=\"{'active':activePane===3}\" class=\"input-section animated fadeIn\">\n                    <h6>Liver function Test</h6>\n                    <div class=\"row\">\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"ast\" formControlName=\"ast\">\n                                <label for=\"ast\">AST (0 - 35U/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"alt\" formControlName=\"alt\">\n                                <label for=\"alt\">ALT M(0 - 45U/L)F(0-34U/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"alp\" formControlName=\"alp\">\n                                <label for=\"alp\">ALP (30 - 120 U/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"bilirubin\" formControlName=\"bilirubin\">\n                                <label for=\"bilirubin\">Bilirubin(Tot) (2 - 21 umo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"conbilirubin\" formControlName=\"conj_bilirubin\">\n                                <label for=\"conbilirubin\">Conj. Bilirubin(Tot) (0 - 5.2 umo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"total-protein\" formControlName=\"total_protein\">\n                                <label for=\"total-protein\">Total Protein (66 - 83g/dl)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"albunim\" formControlName=\"albunim\">\n                                <label for=\"albunim\">Albunim (35 - 53g/dl)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"globulin\" formControlName=\"globulin\">\n                                <label for=\"globulin\">Globuline (18 - 36g/dl)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"glucose\" formControlName=\"glucose\">\n                                <label for=\"glucose\">Glucose</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"rbs\" formControlName=\"rbs\">\n                                <label for=\"rbs\">RBS (3.8 - 8.0 mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"fbs\" formControlName=\"fbs\">\n                                <label for=\"fbs\">FBS (3.8 - 8.0 mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"2hrs\" formControlName=\"hrs2ppbs\">\n                                <label for=\"2hrs\">2HRS. PPBS (3.0 - 5.5 mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"amylase\" formControlName=\"amylase\">\n                                <label for=\"amylase\">Amylase (up to 4001U/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"cpk\" formControlName=\"cpk\">\n                                <label for=\"cpk\">CPK (0-130iu/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"gamma\" formControlName=\"gamma_glutamic\">\n                                <label for=\"gamma\">Gamma Glutamic Acid (M<49F>32)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"uric-acid\" formControlName=\"uric_acid\">\n                                <label for=\"uric-acid\">Uric Acid</label>\n                                <span class=\"helper-text\">Male (214-488umo1/L) Female (137-365umo1/L)</span>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"calcium\" formControlName=\"calcium\">\n                                <label for=\"calcium\">Calcium (2.2 - 2.6 mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\" >\n                                <input type=\"text\" id=\"inorganic-phos\" formControlName=\"inorganic_phos\">\n                                <label for=\"inorganic-phos\">Inorganic Phos (0.96 - 1.44 mmo1/L)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"tsh\" formControlName=\"tsh\">\n                                <label for=\"tsh\">TSH </label>\n                            </div>\n                        </div>\n                    </div>\n        </section>\n        <section [ngClass]=\"{'active':activePane===4}\" class=\"input-section animated fadeIn\">\n                    <h6>Urinalysis</h6>\n                    <div class=\"row\">\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"appearance\" formControlName=\"appearance\">\n                                <label for=\"appearance\">Appearance</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"ph\" formControlName=\"ph\">\n                                <label for=\"ph\">PH</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"protein\" formControlName=\"protein\">\n                                <label for=\"protein\">Protein</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"ketone\" formControlName=\"ketone_bodies\">\n                                <label for=\"ketone\">Ketone Bodies</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"bilirubin-u\" formControlName=\"ur_bilirubin\">\n                                <label for=\"bilirubin-u\">Bilibrubin</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"nitric\" formControlName=\"nitric\">\n                                <label for=\"nitric\">Nitric</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"ascorbic\" formControlName=\"ascorbic\">\n                                <label for=\"ascorbic\">Ascorbic</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"leucocytes\" formControlName=\"leucocytes\">\n                                <label for=\"leucocytes\">Leucocytes</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"gravity\" formControlName=\"specific_gravity\">\n                                <label for=\"gravity\">Specific gravity</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"glucose-u\" formControlName=\"ur_glucose\">\n                                <label for=\"glucose-u\">Glucose</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"urobilinogen\" formControlName=\"urobilinogen\">\n                                <label for=\"urobilinogen\">Urobilinogen</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"blood\" formControlName=\"blood\">\n                                <label for=\"blood\">Blood</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"crystal\" formControlName=\"crystals\">\n                                <label for=\"crystal\">Crystal</label>\n                            </div>\n                        </div>\n                    </div>\n        </section >\n\t</form>\n</div>\n <div class=\"action-buttons\">\n    <div class=\"row\">\n      <div class=\"col s1\">\n        <button (click)=\"prevPane()\"  *ngIf=\"showPrevBtn\" type=\"button\" class=\"btn btn-large waves-effect\">Previous</button>\n      </div>\n      <div class=\"col offset-s8 s3\">\n        <button type=\"button\" *ngIf=\"showNextBtn\" class=\"btn btn-large waves-effect\" id=\"next-btn\" (click)=\"nextPane()\" >Next</button>\n          <button type=\"button\" *ngIf=\"showSave\" class=\"btn btn-large waves-effect\" [disabled]=\"saving\"  (click)=\"handleSubmit($event)\" >{{saveBtnText}}</button>\n      </div>\n\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: ChemicalPathologyFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChemicalPathologyFormComponent", function() { return ChemicalPathologyFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_abstract_report_form__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/abstract-report-form */ "./src/app/shared/abstract-report-form.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../patient/patient.service */ "./src/app/patient/patient.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ChemicalPathologyFormComponent = /** @class */ (function (_super) {
    __extends(ChemicalPathologyFormComponent, _super);
    function ChemicalPathologyFormComponent(router, patientService, activeRoute) {
        var _this = _super.call(this, router, patientService, activeRoute) || this;
        _this.router = router;
        _this.patientService = patientService;
        _this.activeRoute = activeRoute;
        _this.submit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        _this.chemPathForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            patient_id: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            requested_by: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            clinical_detail: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            exam_required: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            specimen: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            clinic: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](),
            total_cholesterol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            triglyceride: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            hdlm: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            vldl: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ldl: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            eucr: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            creatinine: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            urea: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            sodium: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            potassium: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            chloride: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            bicarbonate: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            fsh: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            lh: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            prolactin: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            progesteron: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            estradol: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            psa: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ast: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            alt: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            alp: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            bilirubin: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            conj_bilirubin: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            total_protein: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            albunim: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            globulin: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            glucose: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            rbs: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            fbs: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            hrs2ppbs: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            amylase: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            cpk: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            gamma_glutamic: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            uric_acid: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            calcium: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            inorganic_phos: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            tsh: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            appearance: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ph: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            protein: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ketone_bodies: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ur_bilirubin: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            nitric: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ascorbic: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            leucocytes: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            specific_gravity: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            ur_glucose: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            urobilinogen: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            blood: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            crystals: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
        });
        return _this;
    }
    ChemicalPathologyFormComponent.prototype.reset = function () {
        var _this = this;
        this.chemPathForm.reset();
        setTimeout(function () { _this.initMaterial(); M.updateTextFields(); }, 200);
        this.activePane = 0;
        this.showPane();
    };
    ChemicalPathologyFormComponent.prototype.showPane = function () {
        if (this.activePane > 3) {
            this.showNextBtn = false;
            this.showSave = true;
        }
        else {
            this.showNextBtn = true;
            this.showSave = false;
        }
        if (this.activePane > 0) {
            this.showPrevBtn = true;
        }
        else {
            this.showPrevBtn = false;
        }
    };
    ChemicalPathologyFormComponent.prototype.handleSubmit = function () {
        var patient_id = Number.parseInt(this.chemPathForm.value.patient_id);
        if (isNaN(patient_id)) {
            new _MiniToast__WEBPACK_IMPORTED_MODULE_4__["default"]().warning("You have not seleted any patient");
            this.activePane = 0;
            this.showPane();
        }
        else {
            this.submit.emit(this.chemPathForm.value);
        }
    };
    ChemicalPathologyFormComponent.prototype.ngOnChanges = function () {
        console.log(this.report);
        if (this.report) {
            this.chemPathForm.patchValue(__assign({}, this.report));
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ChemicalPathologyFormComponent.prototype, "submit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ChemicalPathologyFormComponent.prototype, "report", void 0);
    ChemicalPathologyFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'chemical-pathology-form',
            template: __webpack_require__(/*! ./chemical-pathology-form.component.html */ "./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.html"),
            styles: [__webpack_require__(/*! ./chemical-pathology-form.component.scss */ "./src/app/chemical-pathology/chemical-pathology-form/chemical-pathology-form.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__["PatientService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], ChemicalPathologyFormComponent);
    return ChemicalPathologyFormComponent;
}(_shared_abstract_report_form__WEBPACK_IMPORTED_MODULE_1__["AbstractReportForm"]));



/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Chemical Pathology Reports\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/chemical-pathology/reports']\">Chemical Pathology report</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\n    <app-loading [showLoading]='loading'></app-loading>\n     <connection-error [show]=\"!loading && conError \" [retryCb]=\"getReports\"></connection-error>\n     <server-error [show]=\"serverError && !loading\" [retryCb]=\"getReports\"></server-error>\n     <app-linear-loading [showLoading]=\"deleting\"></app-linear-loading>\n     <no-record [show]='matDatasource.data.length==0 && !loading && !conError && !serverError'>There is currently no haematology report</no-record>\n\n \n    <div class=\"card-panel near-loading\" *ngIf=\"matDatasource.data.length>0 && !loading && !conError && !serverError\">\n        <div class=\"header\">\n            <div class=\"row\">\n                <div class=\"col m3 offset-m9\">\n                    <div class=\"input-field\" *ngIf=\"reports?.length>0\">\n                        <input type=\"text\" id=\"filter\"  (keyup)=\"filterTable($event.target.value)\">\n                        <label for=\"filter\">Filter</label>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"table-contents\">\n            <div class=\"wrapper animated fadeIn\">\n            <table mat-table [dataSource]=\"matDatasource\" matSort *ngIf=\"matDatasource.data.length!==0\">\n                <ng-container matColumnDef=\"id\">\n                    <th mat-header-cell *matHeaderCellDef> ID </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.id}} </td>\n                </ng-container>\n\n\n                <ng-container matColumnDef=\"patient_name\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Patient Name </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.patient.first_name+' '+report.patient.surname}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"exam_required\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Exam Required </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.exam_required}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"specimen\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Specimen </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.specimen}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"createdOn\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Created On </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.created_at | date:mediumDate }} </td>\n                </ng-container>\n\n\n                 <ng-container matColumnDef=\"view\">\n                    <th mat-header-cell *matHeaderCellDef > View Details</th>\n                    <td mat-cell *matCellDef=\"let report\"> <a [routerLink]=\"['/chemical-pathology/reports/'+report.id]\">View Details</a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"edit\">\n                    <th mat-header-cell *matHeaderCellDef> Edit </th>\n                    <td mat-cell *matCellDef=\"let report\"> <a [routerLink]=\"['/chemical-pathology/edit/'+report.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"delete\">\n                    <th mat-header-cell *matHeaderCellDef> Delete </th>\n                    <td mat-cell *matCellDef=\"let report; let i = index;\"><button (click)=\"deleteReport(report.id,i)\" [disabled]=\"deleting\" class=\"btn red accent-3\"><i class=\"material-icons\">delete</i></button>\n                </ng-container>\n\n\n                <tr mat-header-row *matHeaderRowDef=\"columnsToDisplay\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: columnsToDisplay;\"></tr>\n\n\n               \n            </table>\n             <mat-paginator [pageSize]=\"10\" \n                            [pageSizeOptions]=\"[5, 10, 20]\" \n                            showFirstLastButtons\n                            *ngIf=\"matDatasource.data.length!==0\"\n                            >\n                        \n            </mat-paginator>\n\n            </div>\n        </div>\n    </div>\n\n\n</div>\n"

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: ChemicalPathologyReportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChemicalPathologyReportsComponent", function() { return ChemicalPathologyReportsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _chemical_pathology_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../chemical-pathology.service */ "./src/app/chemical-pathology/chemical-pathology.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/abstract-table-component */ "./src/app/shared/abstract-table-component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChemicalPathologyReportsComponent = /** @class */ (function (_super) {
    __extends(ChemicalPathologyReportsComponent, _super);
    function ChemicalPathologyReportsComponent(serviceProvider) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.loading = false;
        _this.errorOccurred = false;
        _this.deleting = false;
        _this.columnsToDisplay = ['id', 'patient_name', 'exam_required', 'specimen', 'createdOn', 'view', 'edit', 'delete'];
        _this.getReports = _this.getReports.bind(_this);
        return _this;
    }
    ChemicalPathologyReportsComponent.prototype.ngOnInit = function () {
        this.getReports();
    };
    ChemicalPathologyReportsComponent.prototype.getReports = function () {
        var _this = this;
        this.loading = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReports().subscribe(function (data) {
            _this.loading = false;
            _this.reports = data;
            _this.matDatasource.data = _this.reports;
            _this.matDatasource.paginator = _this.paginator;
        }, function (err) {
            _this.loading = false;
            _this.loading = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    ChemicalPathologyReportsComponent.prototype.deleteReport = function (id, index) {
        var _this = this;
        var toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
        var options = {
            cancel: true,
            cancelText: 'Cancel',
            cancelCallBack: function (event) { },
            confirm: true,
            confirmText: 'Delete',
            confirmCallBack: function (event) {
                _this.deleting = true;
                _this.serviceProvider.deleteReport(id).subscribe(function (res) {
                    toast.success('Report was deleted succesfully');
                    _this.deleting = false;
                    _this.reports.splice(index, 1);
                    _this.matDatasource.data = _this.reports;
                    _this.matDatasource.paginator = _this.paginator;
                }, function (err) {
                    toast.error("Report could not be deleted at the moment please try again");
                });
            }
        };
        roar('Delete Report', 'Do you realy want to  <strong>Delete </strong> this report', options);
    };
    ChemicalPathologyReportsComponent.prototype.filterTable = function (searchText) {
        this.matDatasource.filter = searchText.trim().toLowerCase();
    };
    ChemicalPathologyReportsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chemical-pathology-reports',
            template: __webpack_require__(/*! ./chemical-pathology-reports.component.html */ "./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.html"),
            styles: [__webpack_require__(/*! ./chemical-pathology-reports.component.scss */ "./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.scss")]
        }),
        __metadata("design:paramtypes", [_chemical_pathology_service__WEBPACK_IMPORTED_MODULE_1__["ChemicalPathologyService"]])
    ], ChemicalPathologyReportsComponent);
    return ChemicalPathologyReportsComponent;
}(_shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__["AbstractTableComponent"]));



/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Chemical Pathology Report details\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/chemical-pathology/reports/']\">Chemical Pathology reports </a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\n    <connection-error [show]=\"conError\" [retryCb]=\"getReport\"></connection-error>\n    <server-error [show]=\"serverError\" [retryCb]=\"getReport\"></server-error>\n    <app-loading [showLoading]=\"loadingReport\" loadingInfo=\"Loading report data please wait\" ></app-loading>\n\n\n    <div class=\"card-panel\" *ngIf=\"!loadingReport && !conError && !serverError\">\n        <app-printed-page>\n          <div class=\"view-report-table\">\n          \t<table>\n          \t\t<thead>\n          \t\t\t<tr>\n          \t \t\t\t<th class=\"\" colspan=\"12\">Chemical Pathology Report</th>\n                              <th width=\"10%\">Lab No.: {{ report.lab_id }}</th>\n          \t\t\t</tr>\n         \t\t</thead>\n         \t\t<tbody>\n          \t\t\t<tr>\n          \t\t\t\t<td colspan=\"3\">\n          \t\t\t\t\t<label>Surname</label><br>\n          \t\t\t\t\t{{report?.patient.surname}}\n          \t\t\t\t</td>\n          \t\t\t\t<td colspan=\"2\">\n          \t\t\t\t\t<label>Other Names</label><br>\n          \t\t\t\t\t{{report?.patient.first_name+' '+report?.patient.middle_name}}\n\n          \t\t\t\t</td>\n          \t\t\t\t<td width=\"5%\">\n          \t\t\t\t\t<label>Age</label><br>\n          \t\t\t\t\t{{report?.patient.age}}\n          \t\t\t\t</td>\n          \t\t\t\t<td colspan=\"2\">\n          \t\t\t\t\t<label>Sex</label><br>\n          \t\t\t\t\t{{report?.patient.sex}}\n          \t\t\t\t</td >\n          \t\t\t\t<td colspan=\"2\">\n          \t\t\t\t\t<label>Date</label><br>\n          \t\t\t\t\t{{report?.date | date:mediumDate }}\n          \t\t\t\t</td>\n          \t\t\t\t<td >\n          \t\t\t\t\t<label>Time</label><br>\n          \t\t\t\t\t{{report?.time | date:shortTime}}\n          \t\t\t\t</td>\n          \t\t\t\t<td colspan=\"2\">\n          \t\t\t\t\t<label>Requested By</label><br>\n          \t\t\t\t\t{{report?.requested_by}}\n          \t\t\t\t</td>\n          \t\t\t</tr>\n\n                    <tr>\n                        <td colspan=\"3\" width=\"20%\">\n                            <label>Clinical Details</label><br>\n                             {{ report?.clinical_detail }}\n\n                        </td>\n                        <td colspan=\"5\">\n                            <label>Exam Required</label><br>\n                            {{ report?.exam_required }}\n                        </td>\n                        <td colspan=\"2\">\n                            <label>Specimen</label><br>\n                            {{ report?.specimen }}\n                        </td>\n                        <td colspan=\"3\">\n                            <label>Clinic</label><br>\n                            {{report?.clinic}}\n                        </td>\n                    </tr>\n\n\n\n          \t\t\t<tr>\n          \t\t\t\t<td width=\"4%\"></td>\n          \t\t\t\t<th colspan=\"3\">Test</th>\n          \t\t\t\t<th colspan=\"3\">Results</th>\n          \t\t\t\t<td width=\"4%\"></td>\n          \t\t\t\t<th colspan=\"3\">Test</th>\n          \t\t\t\t<th colspan=\"3\">Results</th>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td></td>\n          \t\t\t\t<th colspan=\"3\">LIPID PROFILE</th>\n          \t\t\t\t<td colspan=\"3\"></td>\n          \t\t\t\t<td></td>\n          \t\t\t\t<th colspan=\"3\">LIVER FUNCTION TEST</th>\n          \t\t\t\t<td colspan=\"3\"></td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.total_cholesterol!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Total Cholesterol(0.0-5.2 mm01/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.total_cholesterol}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.ast!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">AST</td>\n          \t\t\t\t<td colspan=\"3\">{{ report?.ast }}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.triglyceride!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Triglyceride(0.0 - 2.3mm01/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.triglyceride}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.alt!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">ALT</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.alt}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.hdlm!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">HDLM(>0.9mmo1/L)f >(1.15mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.hdlm}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.alp!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">ALP</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.alp}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.vldl!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">VLDL(0.40-0.60mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.vldl}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.bilirubin!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Biliburin</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.bilirubin}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.ldl!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">LDL(0.0-4.11mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.ldl}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.conj_bilirubin!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Conj. Biliburin</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.conj_bilirubin}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.eucr!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">E/U/Cr</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.eucr}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.total_protein!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Total Protein</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.total_protein}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.creatinine!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Creatinene(58 - 110umo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.creatinine}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.albunim!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Albunim</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.albunim}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.urea!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Urea(2.8 -7.2mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.urea}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.globulin!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Globulin</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.globulin}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.sodium!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Sodium(132 -145mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.sodium}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.glucose!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Glucose</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.glucose}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.potassium!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Potassium(3.5 -5.0mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.potassium}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.rbs!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">RBS(3.0-8.0mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.rbs}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.chloride!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Chloride(96 - 108mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.chloride}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.fbs!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">FBS(3.0-5.5mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.fbs}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.chloride!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Bicarbonate(96 - 108mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.chloride}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.hrs2ppbs!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">2HRS PPBS(3.0-5.5mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.hrs2ppbs}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td></td>\n          \t\t\t\t<th colspan=\"3\">HORMONAL PROFILE</th>\n          \t\t\t\t<td colspan=\"3\"></td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.amylase!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Amylase (up to 4001U/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.amylase}}</td>\n          \t\t\t</tr>\n\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.fsh!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">FSH</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.fsh}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.cpk!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">CPK (0-130iu/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.cpk}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.lh!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">LH</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.lh}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.gamma_glutamic!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Gamma Glutamic Acid (M<49F>32)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.gamma_glutamic}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.progesteron!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Progesteron</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.progesteron}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.uric_acid!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Uric acid: Male(214-488umo1/L) Female(137-365 umo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.uric_acid}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.prolactin!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Prolactin</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.prolactin}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.calcium!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Calcium(2.2 - 2.6mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.calcium}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.estradol!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Estradol</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.estradol}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.inorganic_phos!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">Inorganic Phos(0.96 -1.4mmo1/L)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.inorganic_phos}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.psa!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">PSA(0-4ng/ml)</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.psa}}</td>\n          \t\t\t\t<td><i class=\"material-icons\" *ngIf=\"report?.tsh!=''\">check</i></td>\n          \t\t\t\t<td colspan=\"3\">TSH</td>\n          \t\t\t\t<td colspan=\"3\">{{report?.tsh}}</td>\n          \t\t\t</tr>\n          \t\t\t<tr>\n          \t\t\t\t<th colspan=\"12\">\n          \t\t\t\t\tURINALYSYS\n          \t\t\t\t</th>\n          \t\t\t</tr>\n                         <tr>\n                              <th colspan=\"3\">Test</th>\n                              <th colspan=\"3\">Result</th>\n                              <th colspan=\"3\">Test</th>\n                              <th colspan=\"3\">Result</th>\n                         </tr>\n                         <tr>\n                              <td colspan=\"3\">Appearance</td>\n                              <td colspan=\"3\">{{report?.appearance}}</td>\n                              <td colspan=\"3\">Leucocytes</td>\n                              <td colspan=\"3\">{{report?.leucocytes}}</td>\n                         </tr>\n                         <tr>\n                              <td colspan=\"3\">PH</td>\n                              <td colspan=\"3\">{{report?.ph}}</td>\n                              <td colspan=\"3\">Specific Gravity</td>\n                              <td colspan=\"3\">{{report?.specific_gravity}}</td>\n                         </tr>\n                         <tr>\n                              <td colspan=\"3\">Protein</td>\n                              <td colspan=\"3\">{{report?.protein}}</td>\n                              <td colspan=\"3\">Glucose</td>\n                              <td colspan=\"3\">{{report?.ur_glucose}}</td>\n                         </tr>\n                         <tr>\n                              <td colspan=\"3\">Ketone Bodies</td>\n                              <td colspan=\"3\">{{report?.ketone_bodies}}</td>\n                              <td colspan=\"3\">Urobilinogen</td>\n                              <td colspan=\"3\">{{report?.urobilinogen}}</td>\n                         </tr>\n                         <tr>\n                              <td colspan=\"3\">Bilirubin</td>\n                              <td colspan=\"3\">{{report?.ur_bilirubin}}</td>\n                              <td colspan=\"3\">Blood</td>\n                              <td colspan=\"3\">{{report?.blood}}</td>\n                         </tr>\n                         <tr>\n                              <td colspan=\"3\">Nitric</td>\n                              <td colspan=\"3\">{{report?.nitric}}</td>\n                              <td colspan=\"3\">Crystals</td>\n                              <td colspan=\"3\">{{report?.crystals}}</td>\n                         </tr>\n                         <tr>\n                              <td colspan=\"3\">Ascorbic</td>\n                              <td colspan=\"3\">{{report?.ascorbic}}</td>\n                              <td colspan=\"3\"></td>\n                              <td colspan=\"3\"></td>\n                         </tr>\n\n          \t\t</tbody>\n          \t</table>\n          </div>\n        </app-printed-page>\n\n\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: ChemicalPathologyViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChemicalPathologyViewComponent", function() { return ChemicalPathologyViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _chemical_pathology_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../chemical-pathology.service */ "./src/app/chemical-pathology/chemical-pathology.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChemicalPathologyViewComponent = /** @class */ (function () {
    function ChemicalPathologyViewComponent(route, serviceProvider) {
        this.route = route;
        this.serviceProvider = serviceProvider;
        this.loadingReport = false;
        this.conError = false;
        this.serverError = false;
        this.id = null;
        this.getReport = this.getReport.bind(this);
    }
    ChemicalPathologyViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (param) {
            _this.id = param.id;
        });
        this.getReport();
    };
    ChemicalPathologyViewComponent.prototype.getReport = function () {
        var _this = this;
        this.loadingReport = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReport(this.id).subscribe(function (report) {
            _this.loadingReport = false;
            var keys = Object.keys(report);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                if (report[key] === null || report[key] == 'null') {
                    report[key] = '';
                }
                else if (report[key] === '') {
                    report[key] = '';
                }
            }
            _this.report = report;
        }, function (err) {
            _this.loadingReport = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    ChemicalPathologyViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-chemical-pathology-view',
            template: __webpack_require__(/*! ./chemical-pathology-view.component.html */ "./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.html"),
            styles: [__webpack_require__(/*! ./chemical-pathology-view.component.scss */ "./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _chemical_pathology_service__WEBPACK_IMPORTED_MODULE_1__["ChemicalPathologyService"]])
    ], ChemicalPathologyViewComponent);
    return ChemicalPathologyViewComponent;
}());



/***/ }),

/***/ "./src/app/chemical-pathology/chemical-pathology.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/chemical-pathology/chemical-pathology.service.ts ***!
  \******************************************************************/
/*! exports provided: ChemicalPathologyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChemicalPathologyService", function() { return ChemicalPathologyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var ChemicalPathologyService = /** @class */ (function () {
    function ChemicalPathologyService(apiUrl, http) {
        this.apiUrl = apiUrl;
        this.http = http;
    }
    ChemicalPathologyService.prototype.createReport = function (report, options) {
        var formData = new FormData();
        var fields = Object.keys(report);
        for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++) {
            var field = fields_1[_i];
            if (field == 'id')
                continue;
            formData.append(field, report[field]);
        }
        return this.http.post(this.apiUrl + 'chemical-pathology', formData, { params: options });
    };
    ChemicalPathologyService.prototype.getReports = function () {
        return this.http.get(this.apiUrl + 'chemical-pathology');
    };
    ChemicalPathologyService.prototype.getReport = function (id) {
        return this.http.get(this.apiUrl + 'chemical-pathology/' + id);
    };
    ChemicalPathologyService.prototype.updateReport = function (report) {
        var formData = new FormData();
        formData.append('_method', 'PATCH');
        var fields = Object.keys(report);
        for (var _i = 0, fields_2 = fields; _i < fields_2.length; _i++) {
            var field = fields_2[_i];
            if (field == 'id')
                continue;
            formData.append(field, report[field]);
        }
        return this.http.post(this.apiUrl + 'chemical-pathology/' + report.id, formData);
    };
    ChemicalPathologyService.prototype.deleteReport = function (id) {
        var formData = new FormData();
        formData.append('_method', "DELETE");
        return this.http.post(this.apiUrl + 'chemical-pathology/' + id, formData);
    };
    ChemicalPathologyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ChemicalPathologyService);
    return ChemicalPathologyService;
}());



/***/ }),

/***/ "./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.html":
/*!*******************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"New Chemical Pathology Report\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/chemical-pathology/new']\">New Chemical Pathology Report</a> </li>\n</app-title-panel>\n\n<div class=\"page-content\">\n\t<app-linear-loading [showLoading]=\"saving\"></app-linear-loading>\n\t<chemical-pathology-form [saving]=\"saving\"   (submit)=\"onSubmit($event)\"></chemical-pathology-form>\n</div>\n\n<app-modal></app-modal>"

/***/ }),

/***/ "./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.scss":
/*!*******************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.scss ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.ts":
/*!*****************************************************************************************************!*\
  !*** ./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.ts ***!
  \*****************************************************************************************************/
/*! exports provided: CreateChemicalPathologyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateChemicalPathologyComponent", function() { return CreateChemicalPathologyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _abstract_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../abstract-chemical-pathology-component */ "./src/app/chemical-pathology/abstract-chemical-pathology-component.ts");
/* harmony import */ var _chemical_pathology_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../chemical-pathology.service */ "./src/app/chemical-pathology/chemical-pathology.service.ts");
/* harmony import */ var _settings_setting_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../settings/setting.service */ "./src/app/settings/setting.service.ts");
/* harmony import */ var _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../shared/AfterSaveActions */ "./src/app/shared/AfterSaveActions.ts");
/* harmony import */ var _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/modal/modal.component */ "./src/app/shared/modal/modal.component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CreateChemicalPathologyComponent = /** @class */ (function (_super) {
    __extends(CreateChemicalPathologyComponent, _super);
    function CreateChemicalPathologyComponent(serviceProvider, setting) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.setting = setting;
        return _this;
    }
    CreateChemicalPathologyComponent.prototype.onSubmit = function (report) {
        var _this = this;
        var settings = this.setting.getLocalSettings().after_save_action;
        if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_4__["AfterSaveActions"].ALWAYS_ASK) {
            this.modal.openModal(function (options) {
                if (options) {
                    _this.saveReport(report, options);
                }
                else {
                    _this.saveReport(report, {});
                }
            });
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_4__["AfterSaveActions"].SMS_PATIENT) {
            var options = {
                sms: true
            };
            this.saveReport(report, options);
            this.saveReport(report, options);
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_4__["AfterSaveActions"].EMAIL_PATIENT) {
            var options = {
                email: true
            };
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_4__["AfterSaveActions"].EMAIL_AND_SMS_PATIENT) {
            var options = {
                sms: true,
                email: true
            };
            this.saveReport(report, options);
        }
        else {
            this.saveReport(report, {});
        }
    };
    CreateChemicalPathologyComponent.prototype.saveReport = function (report, options) {
        var _this = this;
        this.saving = true;
        this.serviceProvider.createReport(report, options).subscribe(function (data) {
            _this.saving = false;
            _this.toast.success('Report was saved successfully');
            _this.resetForm();
        }, function (error) {
            _this.saving = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else {
                _this.toast.error("An error occurred report could not be saved");
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_5__["ModalComponent"]),
        __metadata("design:type", _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_5__["ModalComponent"])
    ], CreateChemicalPathologyComponent.prototype, "modal", void 0);
    CreateChemicalPathologyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-chemical-pathology',
            template: __webpack_require__(/*! ./create-chemical-pathology.component.html */ "./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.html"),
            styles: [__webpack_require__(/*! ./create-chemical-pathology.component.scss */ "./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.scss")]
        }),
        __metadata("design:paramtypes", [_chemical_pathology_service__WEBPACK_IMPORTED_MODULE_2__["ChemicalPathologyService"], _settings_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"]])
    ], CreateChemicalPathologyComponent);
    return CreateChemicalPathologyComponent;
}(_abstract_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_1__["AbstractChemicalPathologyComponent"]));



/***/ }),

/***/ "./src/app/dashboard/dashboard.component.html":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Dashboard\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Home</a></li>\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a> </li>\n</app-title-panel>\n\n\n\n\n\n<div class=\"page-content\">\n\n\t<connection-error [show]=\"conError\" [retryCb]=\"getSummary\"></connection-error>\n\t <server-error [show]=\"serverError\" [retryCb]=\"getSummary\"></server-error>\n\t <app-loading [showLoading]=\"loadingDetails\" loadingInfo=\"Loading summary  please wait\" ></app-loading>\n\n\t<div class=\"dashboard\" *ngIf=\"!loadingDetails && !conError && !serverError\" class=\"animated fadeIn\">\n\t\t<div class=\"summaries\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col s3\">\n\t\t\t\t\t<div class=\"summary white\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col s4\">\n\t\t\t\t\t\t\t\t<div class=\"icon blue lighten-1 z-depth-3\">\n\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">add_to_queue</i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col s8\">\n\t\t\t\t\t\t\t\t<p class=\"header\">Haematology</p>\n\t\t\t\t\t\t\t\t<p >{{total?.heamatology}} reports</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col s3\">\n\t\t\t\t\t<div class=\"summary white\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col s4\">\n\t\t\t\t\t\t\t\t<div class=\"icon  purple lighten-2 z-depth-3\">\n\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">assistant</i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col s8\">\n\t\t\t\t\t\t\t\t<p class=\"header\">Chemical Pathology</p>\n\t\t\t\t\t\t\t\t<p >{{total?.chemical_pathology}} reports</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col s3\">\n\t\t\t\t\t<div class=\"summary white\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col s4\">\n\t\t\t\t\t\t\t\t<div class=\"icon amber darken-4 z-depth-3\">\n\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">gps_fixed</i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col s8\">\n\t\t\t\t\t\t\t\t<p class=\"header\">Medical Microbiology</p>\n\t\t\t\t\t\t\t\t<p >{{total?.medical_microbiology}} reports</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col s3\">\n\t\t\t\t\t<div class=\"summary white\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col s4\">\n\t\t\t\t\t\t\t\t<div class=\"icon  pink darken-1 z-depth-3\">\n\t\t\t\t\t\t\t\t\t<i class=\"material-icons\">person</i>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col s8\">\n\t\t\t\t\t\t\t\t<p class=\"header\">Patients</p>\n\t\t\t\t\t\t\t\t<p >{{total?.patients}} Patients</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"recent-activities\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t<div class=\"card\">\n\t\t\t\t\t\t<div class=\"card-content\">\n\t\t\t\t\t\t<span class=\"card-title\">Recent Haematology Reports</span>\n\t\t\t\t\t\t<table>\n\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t<th>Patient name</th>\n\t\t\t\t\t\t\t\t\t\t<th>View</th>\n\t\t\t\t\t\t\t\t\t\t<th>Edit</th>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let report  of recent?.heamatology; let i =index\">\n\t\t\t\t\t\t\t\t\t\t<td>{{report.patient.first_name+' '+report.patient.surname }}</td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/haematology/reports/'+report.id]\">View</a></td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/haematology/edit/'+report.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a></td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t<div class=\"card\">\n\t\t\t\t\t\t<div class=\"card-content\">\n\t\t\t\t\t\t\t<span class=\"card-title\">Recent Chemical pathology Reports</span>\n\t\t\t\t\t\t\n\t\t\t\t\t\t\t<table>\n\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<th>Patient name</th>\n\t\t\t\t\t\t\t\t\t<th>View</th>\n\t\t\t\t\t\t\t\t\t<th>Edit</th>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t<tr *ngFor=\"let report  of recent?.chemical_pathology; let i =index\">\n\t\t\t\t\t\t\t\t\t\t<td>{{report?.patient?.first_name+' '+report?.patient?.surname }}</td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/chemical-pathology/reports/'+report.id]\">View</a></td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/chemical-pathology/edit/'+report.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a></td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t<div class=\"card\">\n\t\t\t\t\t\t<div class=\"card-content\">\n\t\t\t\t\t\t\t<span class=\"card-title\">Recent Medical Microbiology Reports</span>\n\t\t\t\t\t\t\n\t\t\t\t\t\t\t<table>\n\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<th>Patient name</th>\n\t\t\t\t\t\t\t\t\t<th>View</th>\n\t\t\t\t\t\t\t\t\t<th>Edit</th>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t<tr *ngFor=\"let report  of recent?.medical_microbiology; let i =index\">\n\t\t\t\t\t\t\t\t\t\t<td>{{report?.patient?.first_name+' '+report?.patient?.surname }}</td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/medical-microbiology-and-parasitology/reports/'+report.id]\">View</a></td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/medical-microbiology-and-parasitology/edit/'+report.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a></td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t<!-- <div class=\"card\">\n\t\t\t\t\t\t<div class=\"card-content\">\n\t\t\t\t\t\t\t<span class=\"card-title\">Recent Radiological Requests</span>\n\n\t\t\t\t\t\t\t<table>\n\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<th>Patient name</th>\n\t\t\t\t\t\t\t\t\t<th>View</th>\n\t\t\t\t\t\t\t\t\t<th>Edit</th>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t<tr *ngFor=\"let report  of recent?.radiological_request; let i =index\">\n\t\t\t\t\t\t\t\t\t\t<td>{{report.first_name+' '+report.surname }}</td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/radiological-request/requests/'+report.id]\">View</a></td>\n\t\t\t\t\t\t\t\t\t\t<td><a [routerLink]=\"['/radiological-request/edit/'+report.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a></td>\n\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t</table>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div> -->\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.scss":
/*!****************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".summary .icon .material-icons {\n  font-size: 50px;\n  color: white; }\n"

/***/ }),

/***/ "./src/app/dashboard/dashboard.component.ts":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_dashboard_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/dashboard.service */ "./src/app/services/dashboard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(service) {
        this.service = service;
        this.loadingDetails = false;
        this.serverError = false;
        this.conError = false;
        this.getSummary = this.getSummary.bind(this);
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.getSummary();
    };
    DashboardComponent.prototype.getSummary = function () {
        var _this = this;
        this.loadingDetails = true;
        this.serverError = false;
        this.conError = false;
        this.service.getSummary().subscribe(function (data) {
            _this.recent = data.recent;
            _this.total = data.total;
            _this.loadingDetails = false;
        }, function (err) {
            _this.loadingDetails = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_dashboard_service__WEBPACK_IMPORTED_MODULE_1__["DashboardService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/haematology/abstract-haematology-component.ts":
/*!***************************************************************!*\
  !*** ./src/app/haematology/abstract-haematology-component.ts ***!
  \***************************************************************/
/*! exports provided: AbstractHaematologyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractHaematologyComponent", function() { return AbstractHaematologyComponent; });
/* harmony import */ var _report_form_report_form_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./report-form/report-form.component */ "./src/app/haematology/report-form/report-form.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AbstractHaematologyComponent = /** @class */ (function () {
    function AbstractHaematologyComponent() {
        this.saving = false;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
    }
    AbstractHaematologyComponent.prototype.resetForm = function () {
        this.reportForm.reset();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_report_form_report_form_component__WEBPACK_IMPORTED_MODULE_0__["ReportFormComponent"]),
        __metadata("design:type", Object)
    ], AbstractHaematologyComponent.prototype, "reportForm", void 0);
    return AbstractHaematologyComponent;
}());



/***/ }),

/***/ "./src/app/haematology/create-report/create-report.component.html":
/*!************************************************************************!*\
  !*** ./src/app/haematology/create-report/create-report.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"New Haematology report\">\n  <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n  <li class=\"breadcrumb-item\"><a [routerLink]=\"['haematology/new']\">New Haematology report</a> </li>\n</app-title-panel>\n<app-modal></app-modal>\n<div class=\"page-content\">\n\t<app-linear-loading [showLoading]=\"saving\"></app-linear-loading>\n\t<hem-report-form [saving]=\"saving\"  (submit)=\"onSubmit($event)\"></hem-report-form>\n</div> \n"

/***/ }),

/***/ "./src/app/haematology/create-report/create-report.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/haematology/create-report/create-report.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/haematology/create-report/create-report.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/haematology/create-report/create-report.component.ts ***!
  \**********************************************************************/
/*! exports provided: HaematologyCreateReport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HaematologyCreateReport", function() { return CreateReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _abstract_haematology_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../abstract-haematology-component */ "./src/app/haematology/abstract-haematology-component.ts");
/* harmony import */ var _haematology_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../haematology.service */ "./src/app/haematology/haematology.service.ts");
/* harmony import */ var _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/modal/modal.component */ "./src/app/shared/modal/modal.component.ts");
/* harmony import */ var _settings_setting_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../settings/setting.service */ "./src/app/settings/setting.service.ts");
/* harmony import */ var _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/AfterSaveActions */ "./src/app/shared/AfterSaveActions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CreateReportComponent = /** @class */ (function (_super) {
    __extends(CreateReportComponent, _super);
    function CreateReportComponent(serviceProvider, setting) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.setting = setting;
        return _this;
    }
    CreateReportComponent.prototype.onSubmit = function (report) {
        var _this = this;
        var settings = this.setting.getLocalSettings().after_save_action;
        if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].ALWAYS_ASK) {
            this.modal.openModal(function (options) {
                if (options) {
                    _this.saveReport(report, options);
                }
                else {
                    _this.saveReport(report, {});
                }
            });
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].SMS_PATIENT) {
            var options = {
                sms: true
            };
            this.saveReport(report, options);
            this.saveReport(report, options);
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].EMAIL_PATIENT) {
            var options = {
                email: true
            };
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].EMAIL_AND_SMS_PATIENT) {
            var options = {
                sms: true,
                email: true
            };
            this.saveReport(report, options);
        }
        else {
            this.saveReport(report, {});
        }
    };
    CreateReportComponent.prototype.saveReport = function (report, options) {
        var _this = this;
        this.saving = true;
        this.serviceProvider.createReport(report, options).subscribe(function (data) {
            _this.saving = false;
            _this.toast.success('Report was saved successfully');
            _this.resetForm();
        }, function (error) {
            _this.saving = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else {
                _this.toast.error("An error occurred report could not be saved");
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__["ModalComponent"]),
        __metadata("design:type", _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__["ModalComponent"])
    ], CreateReportComponent.prototype, "modal", void 0);
    CreateReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'hem-create-report',
            template: __webpack_require__(/*! ./create-report.component.html */ "./src/app/haematology/create-report/create-report.component.html"),
            styles: [__webpack_require__(/*! ./create-report.component.scss */ "./src/app/haematology/create-report/create-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_haematology_service__WEBPACK_IMPORTED_MODULE_2__["HaematologyService"], _settings_setting_service__WEBPACK_IMPORTED_MODULE_4__["SettingService"]])
    ], CreateReportComponent);
    return CreateReportComponent;
}(_abstract_haematology_component__WEBPACK_IMPORTED_MODULE_1__["AbstractHaematologyComponent"]));



/***/ }),

/***/ "./src/app/haematology/edit-report/edit-report.component.html":
/*!********************************************************************!*\
  !*** ./src/app/haematology/edit-report/edit-report.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Haematology report update\">\n  <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n   <li class=\"breadcrumb-item\"><a [routerLink]=\"['/haematology/reports']\">Haematology Reports</a></li>\n  <li class=\"breadcrumb-item\"><a [routerLink]=\"['haematology/edit/'+id]\">Haematology report update</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t<app-linear-loading [showLoading]=\"saving\"></app-linear-loading>\n\t<connection-error [show]=\"conError\" [retryCb]=\"getReport\"></connection-error>\n\t<server-error [show]=\"serverError\" [retryCb]=\"getReport\"></server-error>\n\t<app-loading [showLoading]=\"loadingReport\" loadingInfo=\"Loading report data please wait\" ></app-loading>\n\t<hem-report-form [saving]=\"saving\"  *ngIf=\"!loadingReport && !conError && !serverError\" (submit)=\"onSubmit($event)\" [report]='report' saveBtnText=\"Update Report\" ></hem-report-form>\n</div>"

/***/ }),

/***/ "./src/app/haematology/edit-report/edit-report.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/haematology/edit-report/edit-report.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/haematology/edit-report/edit-report.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/haematology/edit-report/edit-report.component.ts ***!
  \******************************************************************/
/*! exports provided: EditReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditReportComponent", function() { return EditReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _abstract_haematology_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../abstract-haematology-component */ "./src/app/haematology/abstract-haematology-component.ts");
/* harmony import */ var _haematology_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../haematology.service */ "./src/app/haematology/haematology.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditReportComponent = /** @class */ (function (_super) {
    __extends(EditReportComponent, _super);
    function EditReportComponent(serviceProvider, route) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.route = route;
        _this.id = null;
        _this.conError = false;
        _this.serverError = false;
        _this.loadingReport = false;
        _this.getReport = _this.getReport.bind(_this);
        return _this;
    }
    EditReportComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params.id;
            _this.getReport();
        });
    };
    EditReportComponent.prototype.onSubmit = function (report) {
        var _this = this;
        this.saving = true;
        report.id = this.id;
        this.report = report;
        this.serviceProvider.updateReport(report).subscribe(function (data) {
            _this.saving = false;
            _this.toast.success('Report was updated successfully');
        }, function (error) {
            _this.saving = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else {
                _this.toast.error("An error occurred report could not be updated");
            }
        });
    };
    EditReportComponent.prototype.getReport = function () {
        var _this = this;
        this.loadingReport = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReport(this.id).subscribe(function (report) {
            console.log(report);
            var keys = Object.keys(report);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                if (report[key] === null || report[key] == 'null') {
                    report[key] = '';
                }
                else if (report[key] === '') {
                    report[key] = '';
                }
            }
            _this.report = report;
            _this.loadingReport = false;
        }, function (err) {
            _this.loadingReport = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    EditReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-report',
            template: __webpack_require__(/*! ./edit-report.component.html */ "./src/app/haematology/edit-report/edit-report.component.html"),
            styles: [__webpack_require__(/*! ./edit-report.component.scss */ "./src/app/haematology/edit-report/edit-report.component.scss")]
        }),
        __metadata("design:paramtypes", [_haematology_service__WEBPACK_IMPORTED_MODULE_2__["HaematologyService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], EditReportComponent);
    return EditReportComponent;
}(_abstract_haematology_component__WEBPACK_IMPORTED_MODULE_1__["AbstractHaematologyComponent"]));



/***/ }),

/***/ "./src/app/haematology/haematology-report-view/haematology-report-view.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/haematology/haematology-report-view/haematology-report-view.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Haematology Report details\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/haematology/reports/']\">Haematology reports</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\n \n\n    <connection-error [show]=\"conError\" [retryCb]=\"getReport\"></connection-error>\n    <server-error [show]=\"serverError\" [retryCb]=\"getReport\"></server-error>\n    <app-loading [showLoading]=\"loadingReport\" loadingInfo=\"Loading report data please wait\" ></app-loading>\n\n    <div class=\"card-panel\" *ngIf=\"!loadingReport && !conError && !serverError\">\n        <app-printed-page>\n            <div class=\"view-report-table\">\n                <table class=\"browser-default\">\n                    <thead>\n                    <tr>\n                        <th colspan=\"9\">Haematology Report Form</th>\n                        <th>Lab No.:   {{ report?.lab_id }}</th>\n                    <tr>\n                        <td colspan=\"4\">\n                            <label>Surname</label><br>\n                            {{ report.patient.surname }}\n                        </td>\n                        <td>\n                            <label>Other Name</label><br>\n                            {{ report?.patient?.first_name+ ' '+ report?.patient.middle_name}}\n                        </td>\n                        <td>\n                            <label>Age</label><br>\n                            {{ report?.patient?.age}}\n                        </td>\n                        <td>\n                            <label>Sex</label><br>\n                            {{ report?.patient?.sex}}\n                        </td>\n                        <td>\n                            <label>Date</label><br>\n                            {{report?.patient?.created_at | date:mediumDate}}\n                        </td>\n                        <td>\n                            <label>Time</label><br>\n                            {{ report?.created_at | date:shortTime }}\n                        </td>\n                        <td>\n                            <label>Requested By</label><br>\n                            {{ report?.requested_by }}\n                        </td>\n                    </tr>\n                    <tr>\n                        <td colspan=\"4\">\n                            <label>Clinical Details</label><br>\n                             {{ report?.clinical_detail }}\n\n                        </td>\n                        <td colspan=\"3\">\n                            <label>Exam Required</label><br>\n                            {{ report?.exam_required }}\n                        </td>\n                        <td colspan=\"2\">\n                            <label>Specimen</label><br>\n                            {{ report?.specimen }}\n                        </td>\n                        <td colspan=\"1\">\n                            <label>Clinic</label><br>\n                            {{report?.clinic}}\n                        </td>\n                    </tr>\n                    </thead>\n                    <tbody>\n                    <tr>\n                        <td></td>\n                        <!--<td><i class=\"material-icons\">check_square</i></td>-->\n                        <td></td>\n                        <th>Unit</th>\n                        <th>Test</th>\n                        <th>Normal</th>\n                        <td></td>\n                        <td colspan=\"5\"></td>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.hb!=''\">check</i> </td>\n                        <td>hb</td>\n                        <td>g/dl</td>\n                        <td width=\"8%\">{{ report?.hb}}</td>\n                        <td>11 - 17.5</td>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.blood_group!='' \">check</i></td>\n                        <td colspan=\"2\">Blood group</td>\n                        <td colspan=\"3\">{{ report?.blood_group }}</td>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.pcv!=''\">check</i> </td>\n                        <td>PCV/HCT</td>\n                        <td>%</td>\n                        <td width=\"8%\">{{ report?.pcv}}</td>\n                        <td>40 - 54</td>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.hb_genotype!='' \">check</i></td>\n                        <td colspan=\"2\">Hb Genotype</td>\n                        <td colspan=\"3\">{{ report?.hb_genotype }}</td>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.rbc!=''\">check</i> </td>\n                        <td>RBC</td>\n                        <td>X10<sup>12</sup>/1</td>\n                        <td width=\"8%\">{{ report?.rbc}}</td>\n                        <td>4.5 - 5.5</td>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.cross_matching!='' \">check</i></td>\n                        <td colspan=\"2\">Cross matching</td>\n                        <td colspan=\"3\">{{ report?.cross_matching }}</td>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.mcv!=''\">check</i> </td>\n                        <td>MCV</td>\n                        <td>F1</td>\n                        <td width=\"8%\">{{ report?.mcv}}</td>\n                        <td>76 - 93 </td>\n                        <td rowspan=\"20\" colspan=\"6\">\n                            <div class=\"title-t\">\n                                <h6>Electrophoresis</h6>\n                                <h6><u>Film Report:</u></h6>\n                                <p>{{report?.film_report}}</p>\n\n\n\n                                <div style=\"padding-top:100px;\">\n                                    \n                                <P><strong>Sign........................................</strong>\n                                <p><strong>Medical Lab.Scientist: </strong> {{report?.lab_scientist}}</p>\n                                </div>\n                            </div>\n\n\n\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.mch!=''\">check</i> </td>\n                        <td>MCH</td>\n                        <td>Pg</td>\n                        <td width=\"8%\">{{ report?.mch}}</td>\n                        <td>27 - 31 </td>\n\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.mchc!=''\">check</i> </td>\n                        <td>MCHC</td>\n                        <td>g/dl</td>\n                        <td width=\"8%\">{{ report?.mchc}}</td>\n                        <td>31 - 35 </td>\n\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.wbc!=''\">check</i> </td>\n                        <td>WBC</td>\n                        <td>X10<sup>9</sup>/1</td>\n                        <td width=\"8%\">{{ report?.wbc}}</td>\n                        <td>3.0 - 13.0 </td>\n\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.platelets!=''\">check</i> </td>\n                        <td>Platelets</td>\n                        <td>X10<sup>9</sup>/1</td>\n                        <td width=\"8%\">{{ report?.platelets}}</td>\n                        <td>76 - 93 </td>\n\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.retics!=''\">check</i> </td>\n                        <td>Retics</td>\n                        <td>%</td>\n                        <td width=\"8%\">{{ report?.retics}}</td>\n                        <td>0.2 - 0.3 </td>\n\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.esr!=''\">check</i> </td>\n                        <td>ESR</td>\n                        <td>Westergren</td>\n                        <td width=\"8%\">{{ report?.esr}}</td>\n                        <td>5.7 mm in 1Hr </td>\n\n                    </tr>\n                    <tr>\n                        <th colspan=\"4\">DIFFERENTIALS(%)</th>\n                        <td colspan=\"1\"></td>\n\n                    </tr>\n                    <tr>\n                        <th colspan=\"3\"></th>\n                        <th>Test</th>\n                        <th>Normal</th>\n                    </tr>\n\n                    <tr>\n                        <td colspan=\"3\">NUET</td>\n                        <td width=\"8%\">{{ report?.nuet}}</td>\n                        <td>40 - 75 </td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">LYMP</td>\n                        <td width=\"8%\">{{ report?.lymp}}</td>\n                        <td>20 - 45 </td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">MONO</td>\n                        <td width=\"8%\">{{ report?.mono}}</td>\n                        <td>2 - 10 </td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">EOSINO</td>\n                        <td width=\"8%\">{{ report?.eosino}}</td>\n                        <td>0 - 2 </td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">Blast</td>\n                        <td width=\"8%\">{{ report?.blast}}</td>\n                        <td></td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">Promvelo</td>\n                        <td width=\"8%\">{{ report?.promvelo}}</td>\n                        <td></td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">Myelo</td>\n                        <td width=\"8%\">{{ report?.myelo}}</td>\n                        <td></td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">Matemye</td>\n                        <td width=\"8%\">{{ report?.matemye}}</td>\n                        <td></td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">Band</td>\n                        <td width=\"8%\">{{ report?.band}}</td>\n                        <td></td>\n\n                    </tr>\n                    <tr>\n                        <td colspan=\"3\">Normod</td>\n                        <td width=\"8%\">{{ report?.normod}}</td>\n                        <td></td>\n\n                    </tr>\n                    <tr>\n                        <th colspan=\"5\">\n                            Coagulation Tests\n                        </th>\n                    </tr>\n                    <tr>\n                        <th colspan=\"3\"></th>\n                        <th>Test</th>\n                        <th>Normal</th>\n                        <th></th>\n                        <th colspan=\"4\" >G6PD TEST</th>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.bleeding_time!=''\">check</i> </td>\n                        <td colspan=\"2\">Bleeding time</td>\n                        <td>{{ report?.bleeding_time }}</td>\n                        <td>2 - 7 secs</td>\n                        <td class=\"no-border\"></td>\n                        <td colspan=\"2\">\n                            Screening\n                        </td>\n                        <td colspan=\"2\">\n                            {{report?.screening}}\n                        </td>\n\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.clotting_time!=''\">check</i> </td>\n                        <td colspan=\"2\">Clotting time</td>\n                        <td>{{ report?.clotting_time }}</td>\n                        <td>5 - 11 secs</td>\n                        <td class=\"no-border\"></td>\n                        <td colspan=\"2\">\n                            Qualitative\n                        </td>\n                        <td colspan=\"2\">\n                            {{report?.quantitative}}\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.prothrobin_time!=''\">check</i> </td>\n                        <td colspan=\"2\">Prothrobin time</td>\n                        <td>{{ report?.prothrobin_time }}</td>\n                        <td>11 - 14 secs</td>\n                    </tr>\n                    <tr>\n                        <td width=\"4%\"><i class=\"material-icons\" *ngIf=\"report?.partial_prothrobin_time!=''\">check</i> </td>\n                        <td colspan=\"2\">Partial Prothrobin time</td>\n                        <td>{{ report?.partial_prothrobin_time }}</td>\n                        <td>30 - 40 secs</td>\n                    </tr>\n\n\n                    </tbody>\n                </table>\n            </div>\n        </app-printed-page>\n\n\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/haematology/haematology-report-view/haematology-report-view.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/haematology/haematology-report-view/haematology-report-view.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/haematology/haematology-report-view/haematology-report-view.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/haematology/haematology-report-view/haematology-report-view.component.ts ***!
  \******************************************************************************************/
/*! exports provided: HaematologyReportViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HaematologyReportViewComponent", function() { return HaematologyReportViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _haematology_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../haematology.service */ "./src/app/haematology/haematology.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HaematologyReportViewComponent = /** @class */ (function () {
    function HaematologyReportViewComponent(route, serviceProvider) {
        this.route = route;
        this.serviceProvider = serviceProvider;
        this.loadingReport = false;
        this.conError = false;
        this.serverError = false;
        this.id = null;
        this.getReport = this.getReport.bind(this);
    }
    HaematologyReportViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (param) {
            _this.id = param.id;
            _this.getReport();
        });
    };
    HaematologyReportViewComponent.prototype.getReport = function () {
        var _this = this;
        this.loadingReport = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReport(this.id).subscribe(function (report) {
            _this.loadingReport = false;
            var keys = Object.keys(report);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                if (report[key] === null || report[key] == 'null') {
                    report[key] = '';
                }
                else if (report[key] === '') {
                    report[key] = '';
                }
            }
            _this.report = report;
        }, function (err) {
            _this.loadingReport = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    HaematologyReportViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-haematology-report-view',
            template: __webpack_require__(/*! ./haematology-report-view.component.html */ "./src/app/haematology/haematology-report-view/haematology-report-view.component.html"),
            styles: [__webpack_require__(/*! ./haematology-report-view.component.scss */ "./src/app/haematology/haematology-report-view/haematology-report-view.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _haematology_service__WEBPACK_IMPORTED_MODULE_1__["HaematologyService"]])
    ], HaematologyReportViewComponent);
    return HaematologyReportViewComponent;
}());



/***/ }),

/***/ "./src/app/haematology/haematology.service.ts":
/*!****************************************************!*\
  !*** ./src/app/haematology/haematology.service.ts ***!
  \****************************************************/
/*! exports provided: HaematologyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HaematologyService", function() { return HaematologyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var HaematologyService = /** @class */ (function () {
    function HaematologyService(apiUrl, http) {
        this.apiUrl = apiUrl;
        this.http = http;
    }
    HaematologyService.prototype.getReports = function () {
        return this.http.get(this.apiUrl + 'haematology-reports');
    };
    HaematologyService.prototype.createReport = function (report, options) {
        var formData = new FormData();
        var fields = Object.keys(report);
        for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++) {
            var field = fields_1[_i];
            if (field == 'id')
                continue;
            formData.append(field, report[field]);
        }
        return this.http.post(this.apiUrl + 'haematology-reports', formData, { params: options });
    };
    HaematologyService.prototype.getReport = function (id) {
        return this.http.get(this.apiUrl + 'haematology-reports/' + id);
    };
    HaematologyService.prototype.deleteReport = function (id) {
        var formData = new FormData();
        formData.append('_method', "DELETE");
        return this.http.post(this.apiUrl + 'haematology-reports/' + id, formData);
    };
    HaematologyService.prototype.updateReport = function (report) {
        var formData = new FormData();
        formData.append('_method', 'PATCH');
        var fields = Object.keys(report);
        for (var _i = 0, fields_2 = fields; _i < fields_2.length; _i++) {
            var field = fields_2[_i];
            if (field == 'id')
                continue;
            formData.append(field, report[field]);
        }
        return this.http.post(this.apiUrl + 'haematology-reports/' + report.id, formData);
    };
    HaematologyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], HaematologyService);
    return HaematologyService;
}());



/***/ }),

/***/ "./src/app/haematology/haemtology-reports/haematology-reports.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/haematology/haemtology-reports/haematology-reports.component.ts ***!
  \*********************************************************************************/
/*! exports provided: HaematologyReportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HaematologyReportsComponent", function() { return HaematologyReportsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _haematology_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../haematology.service */ "./src/app/haematology/haematology.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/abstract-table-component */ "./src/app/shared/abstract-table-component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HaematologyReportsComponent = /** @class */ (function (_super) {
    __extends(HaematologyReportsComponent, _super);
    function HaematologyReportsComponent(serviceProvider) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.loading = false;
        _this.errorOccurred = false;
        _this.deleting = false;
        _this.columnsToDisplay = ['id', 'patient_name', 'exam_required', 'specimen', 'createdOn', 'view', 'edit', 'delete'];
        _this.getReports = _this.getReports.bind(_this);
        return _this;
    }
    HaematologyReportsComponent.prototype.ngOnInit = function () {
        this.getReports();
    };
    HaematologyReportsComponent.prototype.getReports = function () {
        var _this = this;
        this.loading = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReports().subscribe(function (data) {
            _this.loading = false;
            _this.reports = data;
            _this.matDatasource.data = _this.reports;
            _this.matDatasource.paginator = _this.paginator;
        }, function (err) {
            _this.loading = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    HaematologyReportsComponent.prototype.deleteReport = function (id, index) {
        var _this = this;
        var toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
        var options = {
            cancel: true,
            cancelText: 'Cancel',
            cancelCallBack: function (event) { },
            confirm: true,
            confirmText: 'Delete',
            confirmCallBack: function (event) {
                _this.deleting = true;
                _this.serviceProvider.deleteReport(id).subscribe(function (res) {
                    toast.success('Report was deleted succesfully');
                    _this.deleting = false;
                    _this.reports.splice(index, 1);
                    _this.matDatasource.data = _this.reports;
                    _this.matDatasource.paginator = _this.paginator;
                }, function (err) {
                    toast.error("Report could not be deleted at the moment please try again");
                });
            }
        };
        roar('Delete Report', 'Do you realy want to  <strong>Delete </strong> this report', options);
    };
    HaematologyReportsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-haemtology-reports',
            template: __webpack_require__(/*! ./haemtology-reports.component.html */ "./src/app/haematology/haemtology-reports/haemtology-reports.component.html"),
            styles: [__webpack_require__(/*! ./haemtology-reports.component.scss */ "./src/app/haematology/haemtology-reports/haemtology-reports.component.scss")]
        }),
        __metadata("design:paramtypes", [_haematology_service__WEBPACK_IMPORTED_MODULE_1__["HaematologyService"]])
    ], HaematologyReportsComponent);
    return HaematologyReportsComponent;
}(_shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__["AbstractTableComponent"]));



/***/ }),

/***/ "./src/app/haematology/haemtology-reports/haemtology-reports.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/haematology/haemtology-reports/haemtology-reports.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Haematology Reports\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/haematology/reports']\">Haematology report</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\n \n\n\n     <app-loading [showLoading]='loading'></app-loading>\n     <connection-error [show]=\"!loading && conError \" [retryCb]=\"getReports\"></connection-error>\n     <server-error [show]=\"serverError && !loading\" [retryCb]=\"getReports\"></server-error>\n     <app-linear-loading [showLoading]=\"deleting\"></app-linear-loading>\n     <no-record [show]='matDatasource.data.length==0 && !loading && !conError && !serverError'>There is currently no haematology report</no-record>\n\n    <div class=\"card-panel near-loading\" *ngIf=\"matDatasource.data.length>0 && !loading && !conError && !serverError\">\n        <div class=\"header\">\n            <div class=\"row\">\n                <div class=\"col m3 offset-m9\">\n                    <div class=\"input-field\" *ngIf=\"reports?.length>0\">\n                        <input type=\"text\" id=\"filter\" (keyup)=\"filterTable($event.target.value)\">\n                        <label for=\"filter\">Filter</label>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"table-contents\">\n            <div class=\"wrapper animated fadeIn\">\n            <table mat-table [dataSource]=\"matDatasource\" matSort *ngIf=\"matDatasource.data.length!==0\">\n                <ng-container matColumnDef=\"id\">\n                    <th mat-header-cell *matHeaderCellDef> ID </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.id}} </td>\n                </ng-container>\n\n\n                <ng-container matColumnDef=\"patient_name\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Patient Name </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.patient.first_name+' '+report.patient.surname}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"exam_required\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Exam Required </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.exam_required}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"specimen\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Specimen </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.specimen}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"createdOn\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Created On </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.created_at | date:mediumDate }} </td>\n                </ng-container>\n\n\n                 <ng-container matColumnDef=\"view\">\n                    <th mat-header-cell *matHeaderCellDef > View Details</th>\n                    <td mat-cell *matCellDef=\"let report\"> <a [routerLink]=\"['/haematology/reports/'+report.id]\">View Details</a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"edit\">\n                    <th mat-header-cell *matHeaderCellDef> Edit </th>\n                    <td mat-cell *matCellDef=\"let report\"> <a [routerLink]=\"['/haematology/edit/'+report.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"delete\">\n                    <th mat-header-cell *matHeaderCellDef> Delete </th>\n                    <td mat-cell *matCellDef=\"let report;  let i = index\"><button (click)=\"deleteReport(report.id,i)\" [disabled]=\"deleting\" class=\"btn red accent-3\"><i class=\"material-icons\">delete</i></button></td>\n                </ng-container>\n\n\n                <tr mat-header-row *matHeaderRowDef=\"columnsToDisplay\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: columnsToDisplay;\"></tr>\n\n\n               \n            </table>\n             <mat-paginator [pageSize]=\"10\" \n                            [pageSizeOptions]=\"[5, 10, 20]\" \n                            showFirstLastButtons\n                            *ngIf=\"matDatasource.data.length!==0\"\n                            >\n                                \n             </mat-paginator>\n\n\n            </div>\n        </div>\n    </div>\n\n\n</div>\n"

/***/ }),

/***/ "./src/app/haematology/haemtology-reports/haemtology-reports.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/haematology/haemtology-reports/haemtology-reports.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".info-text {\n  font-size: 1.2em; }\n\n.spinner {\n  margin: auto; }\n"

/***/ }),

/***/ "./src/app/haematology/report-form/report-form.component.html":
/*!********************************************************************!*\
  !*** ./src/app/haematology/report-form/report-form.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-panel report-panel near-loading\">\n\t<form [formGroup]=\"hemForm\">\n\t\t<section [ngClass]=\"{'active':activePane===0}\" class=\"input-section animated fadeIn\">\n\t\t\t<h5 class=\"card-title\">Basic Test information</h5>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t <select (change)=\"onPatientSelected($event)\" class=\"validate\"  formControlName=\"patient_id\" required>\n\t\t\t\t\t\t \t<option value=\"\" ></option>\n\t\t\t\t\t\t \t\n\t\t\t\t\t\t     <option value=\"new\">Add new patient</option>\n\t\t\t\t\t\t     <option disabled *ngIf=\"loadingPatients\">Loading patients please wait</option>\n\t\t\t\t\t\t     <option value=\"retry\"  *ngIf=\"errorOccurred\" >An error occurred, click here to retry loading patients</option>\n\t\t\t\t\t\t\t <option *ngFor=\"let patient of patients\" value=\"{{patient.id}}\">{{patient.first_name+' '+patient.middle_name+' '+patient.surname }}</option>}\n\t\t\t\t\t\t\toption\n\t\t\t\t\t    </select>\n    \t\t\t\t\t<label>Select patient</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"requested-by\" formControlName=\"requested_by\" class=\"validate\" required>\n\t\t\t\t\t\t<label for=\"requested-by\">Requested By</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"clinical-detail\" formControlName=\"clinical_detail\">\n\t\t\t\t\t\t<label for=\"clinical-detail\">Clinical Detail</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"exam-required\" formControlName=\"exam_required\">\n\t\t\t\t\t\t<label for=\"exam-required\">Exam Required</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"specimen\" formControlName=\"specimen\">\n\t\t\t\t\t\t<label for=\"specimen\">Specimen</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"clinic\" formControlName=\"clinic\">\n\t\t\t\t\t\t<label for=\"clinic\">Clinic</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\n\t\t <section [ngClass]=\"{'active':activePane===1}\" class=\"input-section animated fadeIn\">\n                     <div class=\"row\">\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"hb\" formControlName=\"hb\">\n                                 <label for=\"hb\">Hb(g/dl)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 11 - 17.5</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"pcv\" formControlName=\"pcv\">\n                                 <label for=\"pcv\">PCV/HCT(%)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 40 - 54</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"rbc\" formControlName=\"rbc\">\n                                 <label for=\"rbc\">RBC(X10<sup>12</sup>/1)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 4.5 - 5.5</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"mcv\" formControlName=\"mcv\">\n                                 <label for=\"mcv\">MCV(F1)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 76 - 93</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"mch\" formControlName=\"mch\">\n                                 <label for=\"mch\">MCH(Pg)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 27 - 31</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"mchc\" formControlName=\"mchc\">\n                                 <label for=\"mchc\">MCHC(g/dl)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 31 - 35</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"wbc\" formControlName=\"wbc\">\n                                 <label for=\"wbc\">WBC(X 10<sup>9</sup>/1)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 3 - 13.0</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"platelets\" formControlName=\"platelets\">\n                                 <label for=\"platelets\">PLATELETS(X 10<sup>9/</sup>10)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 100 - 300</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"retics\" formControlName=\"retics\">\n                                 <label for=\"retics\">RETICS(%)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 0.2 - 2</span>\n                             </div>\n                         </div>\n\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"westergren\" formControlName=\"esr\">\n                                 <label for=\"westergren\">ESR(westergren)</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 5.7mm in 1 Hr</span>\n                             </div>\n                         </div>\n                     </div>\n                 </section>\n                 <section [ngClass]=\"{'active':activePane===2}\" class=\"input-section animated fadeIn\" >\n                     <h6>Differentials(%)</h6>\n\n                     <div class=\"row\">\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"neut\" formControlName=\"nuet\">\n                                 <label for=\"neut\">NUET</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 40 - 75</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"lymp\" formControlName=\"lymp\">\n                                 <label for=\"lymp\">LYMP</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 20 - 45</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"mono\" formControlName=\"mono\">\n                                 <label for=\"mono\">MONO</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 2 - 10</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"eosino\" formControlName=\"eosino\">\n                                 <label for=\"eosino\">EOSINO</label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 0 - 2</span>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"baso\" formControlName=\"baso\">\n                                 <label for=\"baso\">BASO</label>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"blast\" formControlName=\"blast\">\n                                 <label for=\"blast\">Blast</label>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"promvelo\" formControlName=\"promvelo\">\n                                 <label for=\"promvelo\">Promvelo</label>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"myelo\" formControlName=\"myelo\">\n                                 <label for=\"myelo\">Myelo</label>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"matemye\" formControlName=\"matemye\">\n                                 <label for=\"matemye\">Matemye</label>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"band\" formControlName=\"band\">\n                                 <label for=\"band\">Band</label>\n                             </div>\n                         </div>\n                         <div class=\"col m3\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"normob\" formControlName=\"normod\">\n                                 <label for=\"normob\">Normob</label>\n                             </div>\n                         </div>\n                     </div>\n                 </section>\n                 <section [ngClass]=\"{'active':activePane===3}\" class=\"input-section animated fadeIn\">\n                     <h6>Coagulation Tests</h6>\n                     <div class=\"row\">\n                         <div class=\"col m6\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"bleeding\" formControlName=\"bleeding_time\">\n                                 <label for=\"bleeding\">Bleeding Time </label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 2 - 7 secs</span>\n                             </div>\n                         </div>\n\n                         <div class=\"col m6\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"clotting\" formControlName=\"clotting_time\">\n                                 <label for=\"clotting\">Clotting Time </label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 5 - 11 secs</span>\n                             </div>\n                         </div>\n\n                         <div class=\"col m6\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"prothrombin\" formControlName=\"prothrobin_time\">\n                                 <label for=\"prothrombin\">Prothrobin Time </label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 11 - 14 secs</span>\n                             </div>\n                         </div>\n\n                         <div class=\"col m6\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"partial-prothrombin\" formControlName=\"partial_prothrobin_time\">\n                                 <label for=\"partial-prothrombin\">Partial Prothrobin Time </label>\n                                 <span class=\"helper-text\" data-error=\"wrong\" data-success=\"right\">Normal 30 - 40 secs</span>\n                             </div>\n                         </div>\n                     </div>\n                 </section>\n                 <section [ngClass]=\"{'active':activePane===4}\" class=\"input-section animated fadeIn\">\n                     <div class=\"row\">\n                         <div class=\"col m4\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"blood-group\" formControlName=\"blood_group\">\n                                 <label for=\"blood-group\">Blood group </label>\n                             </div>\n                         </div>\n\n                         <div class=\"col m4\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"hb-genotype\" formControlName=\"hb_genotype\">\n                                 <label for=\"hb-genotype\">HB Genotype </label>\n                             </div>\n                         </div>\n\n                         <div class=\"col m4\">\n                             <div class=\"input-field\" >\n                                 <input type=\"text\" id=\"cross-matching\" formControlName=\"cross_matching\">\n                                 <label for=\"cross-matching\">Cross matching </label>\n                             </div>\n                         </div>\n                     </div>\n                 </section>\n                 <section [ngClass]=\"{'active':activePane===5}\" class=\"input-section animated fadeIn\">\n                     <h6>ELECTROPHORESIS</h6>\n                     <div class=\"input-field\">\n                         <input type=\"text\" id=\"film-report\" formControlName=\"film_report\">\n\n                         <label for=\"film-report\">Film Report</label>\n                     </div>\n                     <div class=\"input-field\">\n                         <input type=\"text\" id=\"lab-scientist\" formControlName=\"lab_scientist\">\n                         <label for=\"lab-scientist\">Medical Lab. Scientist</label>\n                     </div>\n                     <h6>\n                         G6PD Test\n                     </h6>\n                     <div class=\"row\">\n                         <div class=\"col m6\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"screening\" formControlName=\"screening\">\n                                 <label for=\"screening\">Screening</label>\n                             </div>\n                         </div>\n\n                         <div class=\"col m6\">\n                             <div class=\"input-field\">\n                                 <input type=\"text\" id=\"quantitative\" formControlName=\"quantitative\">\n                                 <label for=\"quantitative\">Quantitative</label>\n                             </div>\n                         </div>\n                     </div>\n                 </section >\n\t</form>\n</div>\n <div class=\"action-buttons\">\n    <div class=\"row\">\n      <div class=\"col s1\">\n        <button (click)=\"prevPane()\"  *ngIf=\"showPrevBtn\" type=\"button\" class=\"btn btn-large waves-effect\">Previous</button>\n      </div>\n      <div class=\"col offset-s8 s3\">\n        <button type=\"button\" *ngIf=\"showNextBtn\" class=\"btn btn-large waves-effect\" id=\"next-btn\" (click)=\"nextPane()\" >Next</button>\n        <button type=\"button\" *ngIf=\"showSave\" class=\"btn btn-large waves-effect\" [disabled]=\"saving\"  (click)=\"handleSubmit($event)\" >{{saveBtnText}}</button>\n      </div>\n\n    </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/haematology/report-form/report-form.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/haematology/report-form/report-form.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/haematology/report-form/report-form.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/haematology/report-form/report-form.component.ts ***!
  \******************************************************************/
/*! exports provided: ReportFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportFormComponent", function() { return ReportFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../patient/patient.service */ "./src/app/patient/patient.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _shared_abstract_report_form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/abstract-report-form */ "./src/app/shared/abstract-report-form.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ReportFormComponent = /** @class */ (function (_super) {
    __extends(ReportFormComponent, _super);
    function ReportFormComponent(router, patientService, activeRoute) {
        var _this = _super.call(this, router, patientService, activeRoute) || this;
        _this.router = router;
        _this.patientService = patientService;
        _this.activeRoute = activeRoute;
        _this.submit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        _this.saveBtnText = "Save Report";
        _this.hemForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            patient_id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            requested_by: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            clinical_detail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            exam_required: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            specimen: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            clinic: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            hb: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            pcv: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            rbc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            mcv: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            mch: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            mchc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            wbc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            platelets: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            retics: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            esr: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            nuet: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            lymp: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            mono: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            eosino: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            baso: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            blast: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            band: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            promvelo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            myelo: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            matemye: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            normod: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            bleeding_time: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            clotting_time: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            prothrobin_time: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            partial_prothrobin_time: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            blood_group: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            hb_genotype: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            cross_matching: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            film_report: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            lab_scientist: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            screening: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            quantitative: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
        });
        return _this;
    }
    ReportFormComponent.prototype.ngOnChanges = function () {
        if (this.report) {
            this.hemForm.patchValue(__assign({}, this.report));
        }
    };
    ReportFormComponent.prototype.showPane = function () {
        if (this.activePane > 4) {
            this.showNextBtn = false;
            this.showSave = true;
        }
        else {
            this.showNextBtn = true;
            this.showSave = false;
        }
        if (this.activePane > 0) {
            this.showPrevBtn = true;
        }
        else {
            this.showPrevBtn = false;
        }
    };
    ReportFormComponent.prototype.handleSubmit = function () {
        var patient_id = Number.parseInt(this.hemForm.value.patient_id);
        if (isNaN(patient_id)) {
            new _MiniToast__WEBPACK_IMPORTED_MODULE_4__["default"]().warning("You have not seleted any patient");
            this.activePane = 0;
            this.showPane();
        }
        else {
            this.submit.emit(this.hemForm.value);
        }
    };
    ReportFormComponent.prototype.reset = function () {
        var _this = this;
        this.hemForm.reset();
        setTimeout(function () { _this.initMaterial(); M.updateTextFields(); }, 200);
        this.activePane = 0;
        this.showPane();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ReportFormComponent.prototype, "report", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ReportFormComponent.prototype, "submit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ReportFormComponent.prototype, "saveBtnText", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], ReportFormComponent.prototype, "saving", void 0);
    ReportFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'hem-report-form',
            template: __webpack_require__(/*! ./report-form.component.html */ "./src/app/haematology/report-form/report-form.component.html"),
            styles: [__webpack_require__(/*! ./report-form.component.scss */ "./src/app/haematology/report-form/report-form.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__["PatientService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], ReportFormComponent);
    return ReportFormComponent;
}(_shared_abstract_report_form__WEBPACK_IMPORTED_MODULE_5__["AbstractReportForm"]));



/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"navbar-fixed\">\n\n  <nav>\n    <div class=\"nav-wrapper\">\n     \n      <div class=\"row\">\n      \t <div class=\"col m2\">\n      \t <!-- \t<button class=\"toggle waves-effect\"><i class=\"material-icons\">dehaze</i></button>\n            <button class=\"toggle waves-effect\"><i class=\"material-icons\">search</i></button>\n      \t --> </div>\n         <div class=\"col m1 offset-m6\" style=\"padding-top:20px; padding-left:80px\">\n           <div class=\"preloader-wrapper small active\" *ngIf=\"loggingOut\" >\n              <div class=\"spinner-layer\">\n                <div class=\"circle-clipper left\">\n                  <div class=\"circle\"></div>\n                </div><div class=\"gap-patch\">\n                  <div class=\"circle\"></div>\n                </div><div class=\"circle-clipper right\">\n                  <div class=\"circle\"></div>\n                </div>\n              </div>\n            </div>\n\n\n\n\n\n         </div>\n      \t <div class=\"col m1 \">\n      \t \t<div>\n      \t \t\t<button class=\"toggle waves-effect dropdown-trigger\"  data-target='account' ><i class=\"material-icons\">account_circle</i></button>\t\n  \t\t\t\t<ul id='account' class='dropdown-content'>\n\t\t\t\t    <!-- <li (click)='changePassword()'><a>Change Password</a></li> -->\n\t\t\t\t    <li (click)='logOut()'><a>Log out</a></li>\n\t\t\t\t</ul>\n  \t\t\t</div>\n      \t </div>\n      </div>\n    </div>\n  </nav>\n</header>\n\n<div  class=\"modal\" #passwordModal>\n    <div class=\"modal-content\">\n      <h4>Change Password</h4>\n      <p>A bunch of text</p>\n    </div>\n    <div class=\"modal-footer\">\n      <a href=\"#!\" class=\"modal-close waves-effect waves-green btn-flat\">Agree</a>\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/header/header.component.scss":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _Auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Auth/auth.service */ "./src/app/Auth/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(auth, router) {
        this.auth = auth;
        this.router = router;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_1__["default"]();
        this.loggingOut = false;
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.ngAfterViewInit = function () {
        var elems = document.querySelectorAll('.dropdown-trigger');
        M.Dropdown.init(elems, { constrainWidth: false });
    };
    HeaderComponent.prototype.logOut = function () {
        var _this = this;
        this.loggingOut = true;
        this.toast.success("Logging you out now");
        this.auth.logout().subscribe(function (res) {
            _this.auth.clearAuth();
            _this.toast.success("Logged out successfully");
            _this.router.navigateByUrl('/login');
            _this.loggingOut = false;
        }, function (err) {
            _this.toast.error("An error, logout was not successful please try again");
            _this.loggingOut = false;
        });
    };
    HeaderComponent.prototype.changePassword = function () {
        var modal = M.Modal.init(this.passwordModal.nativeElement);
        modal.open();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('passwordModal'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], HeaderComponent.prototype, "passwordModal", void 0);
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_Auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-wrap\">\n\n\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col m6 offset-m3\">\n\t\t\t\t<div class=\"panel-wrap\">\n\t\t\t\t\t<app-linear-loading [showLoading]=\"logginIn\"></app-linear-loading>\n\t\t\t\t<div class=\"card-panel near-loading\">\n\t\t\t\t\t<div class=\"logo center-align\">\n\t\t\t\t\t\t<img [src]=\"logoUrl\" height=\"100px\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<h5 class=\"card-tile\">Sign In</h5>\n\t\t\t\t\t<hr>\n\t\t\t\t\t<form (submit)=\"login($event)\">\n\t\t\t\t\t\t<div class=\"input-field\"> \n\t\t\t\t\t\t\t<input type=\"text\" id=\"username\" name=\"username\" class=\"validate\" required>\n\t\t\t\t\t\t\t<label for=\"username\">Username</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t\t<input type=\"password\" id=\"password\" name=\"password\" class=\"validate\" required>\n\t\t\t\t\t\t\t<label for=\"password\">Password</label>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"center-align\">\n\t\t\t\t\t\t\t<button [disabled]=\"logginIn\" class=\"btn waves-effect btn-large  waves-light\">Sign In</button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</form>\n\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-wrap {\n  position: absolute;\n  z-index: 9000;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #e5e5e5; }\n\n.card-panel {\n  margin-top: 0 !important; }\n\n.panel-wrap {\n  margin-top: 100px; }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _settings_setting_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../settings/setting.service */ "./src/app/settings/setting.service.ts");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(setting, auth, router) {
        this.setting = setting;
        this.auth = auth;
        this.router = router;
        this.logoUrl = '';
        this.logginIn = false;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_4__["default"]();
    }
    LoginComponent.prototype.ngOnInit = function () {
        if (this.setting.getLocalSettings()) {
            this.logoUrl = this.setting.getLocalSettings().logo_file;
        }
        if (this.auth.isLogin()) {
            this.router.navigateByUrl('/dashboard');
        }
    };
    LoginComponent.prototype.login = function (ev) {
        var _this = this;
        this.logginIn = true;
        this.auth.login(new FormData(ev.target)).subscribe(function (res) {
            if (!_this.setting.getLocalSettings()) {
                _this.setting.getSettings().subscribe(function (data) {
                    _this.setting.saveLocalSettings(data);
                    _this.redirect(res);
                }, function () {
                    _this.logginIn = false;
                    _this.toast.error("An error occured please try again");
                });
            }
            else {
                _this.redirect(res);
            }
        }, function (error) {
            _this.logginIn = false;
            if (error.status == 401) {
                _this.toast.error("Invalid Username or password");
            }
            else if (error.status == 0) {
                _this.toast.error("Can't connect to server");
            }
            else {
                _this.toast.error("An error occured please try again");
            }
        });
    };
    LoginComponent.prototype.redirect = function (res) {
        this.logginIn = false;
        var data = res;
        this.auth.setToken(data.token);
        this.auth.setUser(data.user);
        this.toast.success("Log in was successfull");
        this.auth.onLogIn.emit();
        this.router.navigateByUrl('/dashboard');
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_settings_setting_service__WEBPACK_IMPORTED_MODULE_1__["SettingService"], _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/manage-labs/labs.service.ts":
/*!*********************************************!*\
  !*** ./src/app/manage-labs/labs.service.ts ***!
  \*********************************************/
/*! exports provided: LabsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LabsService", function() { return LabsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var LabsService = /** @class */ (function () {
    function LabsService(apiUrl, http) {
        this.apiUrl = apiUrl;
        this.http = http;
    }
    LabsService.prototype.addLab = function (lab) {
        var formData = new FormData();
        formData.append('lab-name', lab.lab_name);
        formData.append('location', lab.location);
        formData.append('username', lab.username);
        formData.append('password', lab.password);
        return this.http.post(this.apiUrl + 'laboratories', formData);
    };
    LabsService.prototype.updateLab = function (lab) {
        var formData = new FormData();
        formData.append('_method', 'PATCH');
        formData.append('lab-name', lab.lab_name);
        formData.append('location', lab.location);
        formData.append('username', lab.username);
        return this.http.post(this.apiUrl + 'laboratories/' + lab.id, formData);
    };
    LabsService.prototype.deleteLab = function (id) {
        var data = new FormData();
        data.append('_method', 'DELETE');
        return this.http.post(this.apiUrl + 'laboratories/' + id, data);
    };
    LabsService.prototype.getLabs = function () {
        return this.http.get(this.apiUrl + 'laboratories');
    };
    LabsService.prototype.updatePassword = function (password, id) {
        var formData = new FormData();
        formData.append('_method', 'PATCH');
        formData.append('password', password);
        return this.http.post(this.apiUrl + 'laboratories/change-password/' + id, formData);
    };
    LabsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], LabsService);
    return LabsService;
}());



/***/ }),

/***/ "./src/app/manage-labs/manage-labs.component.html":
/*!********************************************************!*\
  !*** ./src/app/manage-labs/manage-labs.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Dashboard\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/manage-labs']\">Manage Labs</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t<div class=\"card-panel\">\n\t\t<div class=\"right-align\">\n\t\t\t<button class=\"waves-effect waves-light btn-large btn modal-trigger\" data-target=\"lab-modal\"><i class=\"material-icons left\">add</i>Add New</button>\n\t\t</div>\n\t\t\n\t</div>\n<app-loading [showLoading]='loadingLabs'></app-loading>\n<connection-error [show]=\"!loadingLabs && conError \" [retryCb]=\"getLabs\"></connection-error>\n<server-error [show]=\"serverError && !loadingLabs\" [retryCb]=\"getLabs\"></server-error>\n<app-linear-loading [showLoading]=\"deleting\"></app-linear-loading>\n<no-record [show]='matDatasource.data.length==0 && !loadingLabs && !conError && !serverError'>There is currently no haematology report</no-record>\n<div class=\"card-panel near-loading\" *ngIf=\"matDatasource.data.length>0 && !loadingLabs && !conError && !serverError\">\n<div class=\"\">\n\t<div class=\"row\">\n\t\t<div class=\"col m3 offset-m9\">\n\t\t\t<div class=\"input-field\" >\n\t\t\t\t<input type=\"text\" id=\"filter\" (keyup)=\"filterTable($event.target.value)\">\n\t\t\t\t<label for=\"filter\">Filter</label>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\t<table mat-table [dataSource]=\"matDatasource\" matSort *ngIf=\"matDatasource.data.length!==0\">\n\t\t<ng-container matColumnDef=\"id\">\n\t\t<th mat-header-cell *matHeaderCellDef> ID </th>\n\t\t<td mat-cell *matCellDef=\"let lab\"> {{lab.id}} </td>\n\t\t</ng-container>\n\t\t<ng-container matColumnDef=\"lab_name\">\n\t\t<th mat-header-cell *matHeaderCellDef> Lab Name </th>\n\t\t<td mat-cell *matCellDef=\"let lab\"> {{ lab.lab_name}} </td>\n\t\t</ng-container>\n\t\t<ng-container matColumnDef=\"location\">\n\t\t<th mat-header-cell *matHeaderCellDef> Location </th>\n\t\t<td mat-cell *matCellDef=\"let lab\"> {{lab.location}} </td>\n\t\t</ng-container>\n\t\t<ng-container matColumnDef=\"username\">\n\t\t<th mat-header-cell *matHeaderCellDef> Username </th>\n\t\t<td mat-cell *matCellDef=\"let lab\"> {{lab.username}} </td>\n\t\t</ng-container>\n\n\t\t<ng-container matColumnDef=\"password\">\n            <th mat-header-cell *matHeaderCellDef> Password </th>\n            <td mat-cell *matCellDef=\"let lab\"><button (click)=\"changePassword(lab.id)\"  class=\"btn blue accent-3\"><i class=\"material-icons\">edit</i></button></td>\n         </ng-container>\n\n\t\t <ng-container matColumnDef=\"edit\">\n            <th mat-header-cell *matHeaderCellDef> Edit </th>\n            <td mat-cell *matCellDef=\"let lab\"><button (click)=\"editLab(lab)\"  class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></button></td>\n         </ng-container>\n\n\t\t<ng-container matColumnDef=\"delete\">\n            <th mat-header-cell *matHeaderCellDef> Delete </th>\n            <td mat-cell *matCellDef=\"let lab; let i = index;\"><button *ngIf=\"lab.admin!=1\" (click)=\"deleteLab(lab.id,i)\" [disabled]=\"deleting\" class=\"btn red accent-3\"><i class=\"material-icons\">delete</i></button></td>\n         </ng-container>\n\n        \n\n\t<tr mat-header-row *matHeaderRowDef=\"columnsToDisplay\"></tr>\n\t<tr mat-row *matRowDef=\"let row; columns: columnsToDisplay; let i = index;\"></tr>\n</table>\n<mat-paginator [pageSize]=\"10\"\n[pageSizeOptions]=\"[5, 10, 20]\"\nshowFirstLastButtons\n*ngIf=\"matDatasource.data.length!==0\"\n>\n</mat-paginator>\n</div>\n</div>\n</div>\n\n\n\n<div id=\"lab-modal\" #labmodal class=\"modal \" >\n<form [formGroup]=\"labForm\" (ngSubmit)=\"handleForm()\">\n<div class=\"modal-content\">\n<div class=\"row\">\n<div class=\"col m10\">\n\t<h4>{{modalTitle}}</h4>\n</div>\n<div class=\"col m2\">\n\t<div class=\"preloader-wrapper active\" *ngIf=\"loading\">\n\t\t<div class=\"spinner-layer spinner-blue-only\">\n\t\t\t<div class=\"circle-clipper left\">\n\t\t\t\t<div class=\"circle\"></div>\n\t\t\t</div><div class=\"gap-patch\">\n\t\t\t<div class=\"circle\"></div>\n\t\t</div><div class=\"circle-clipper right\">\n\t\t<div class=\"circle\"></div>\n\t</div>\n</div>\n</div>\n</div>\n</div>\n<div class=\"row\">\n\t\t<div class=\"col m6\">\n\t\t\t<div class=\"input-field\">\n\t\t\t\t<input type=\"text\" id=\"lab-name\" formControlName=\"lab_name\" class=\"validate\" required />\n\t\t\t\t<label for=\"lab-name\">Lab Name</label>\n\t\t\t</div>\n\t\t</div>\n\t<div class=\"col m6\">\n\t\t<div class=\"input-field\">\n\t\t\t<input type=\"text\" id=\"location\" formControlName=\"location\"  class=\"validate\" required/>\n\t\t\t<label for=\"location\">Location</label>\n\t\t</div>\n\t</div>\n\t<div class=\"col m6\">\n\t\t<div class=\"input-field\">\n\t\t\t<input type=\"text\" id=\"username\" formControlName=\"username\"  class=\"validate\" required/>\n\t\t\t<label for=\"username\">Username</label>\n\t\t</div>\n\t</div>\n\t<div class=\"col m6\" *ngIf=\"!updating\">\n\t\t<div class=\"input-field\">\n\t\t\t<input type=\"password\" id=\"password\" formControlName=\"password\"  class=\"validate\" required />\n\t\t\t<label for=\"password\">Password</label>\n\t\t</div>\n\t</div>\n\t</div>\n\t<div class=\"input-field\" *ngIf=\"!updating\">\n\t\t<input type=\"password\" id=\"confirm-password\" formControlName=\"confirmPassword\"  class=\"validate\" required />\n\t\t<label for=\"confirm-password\">Confirm Password</label>\n</div>\n</div>\n<div class=\"modal-footer\">\n\t<button type=\"button\" class=\"modal-close waves-effect waves-green btn-flat\">Close</button>\n\t<button type=\"submit\" class=\"waves-effect waves-green btn-flat\" [disabled]=\"loading\">{{modalBtnText}}</button>\n\t</div>\n</form>\n</div>\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n<div  #password class=\"modal\" >\n<form [formGroup]=\"passwordForm\" (ngSubmit)=\"updatePassword()\">\n<div class=\"modal-content\">\n<div class=\"row\">\n<div class=\"col m10\">\n\t<h4>Change Password</h4>\n</div>\n<div class=\"col m2\">\n\t<div class=\"preloader-wrapper active\" *ngIf=\"changingPassword\">\n\t\t<div class=\"spinner-layer spinner-blue-only\">\n\t\t\t<div class=\"circle-clipper left\">\n\t\t\t\t<div class=\"circle\"></div>\n\t\t\t</div>\n\t\t\t<div class=\"gap-patch\">\n\t\t\t<div class=\"circle\"></div>\n\t\t</div>\n\t\t<div class=\"circle-clipper right\">\n\t\t<div class=\"circle\"></div>\n\t\t</div>\n\t</div>\n</div>\n</div>\n</div>\n<div class=\"row\">\n\t\t<div class=\"input-field\">\n\t\t\t<input type=\"password\" id=\"password2\" formControlName=\"password\"  class=\"validate\" required />\n\t\t\t<label for=\"password2\">Password</label>\n\t\t</div>\n\t<div class=\"input-field\" *ngIf=\"!updating\">\n\t\t<input type=\"password\" id=\"confirm-password2\" formControlName=\"confirmPassword\"  class=\"validate\" required />\n\t\t<label for=\"confirm-password2\">Confirm Password</label>\n\t</div>\n</div>\n<div class=\"modal-footer\">\n\t<button type=\"button\" class=\"modal-close waves-effect waves-green btn-flat\">Close</button>\n\t<button type=\"submit\" class=\"waves-effect waves-green btn-flat\" [disabled]=\"changingPassword\">Change Password</button>\n\t</div>\n</div>\n</form>\n</div>"

/***/ }),

/***/ "./src/app/manage-labs/manage-labs.component.scss":
/*!********************************************************!*\
  !*** ./src/app/manage-labs/manage-labs.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/manage-labs/manage-labs.component.ts":
/*!******************************************************!*\
  !*** ./src/app/manage-labs/manage-labs.component.ts ***!
  \******************************************************/
/*! exports provided: ManageLabsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageLabsComponent", function() { return ManageLabsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _labs_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./labs.service */ "./src/app/manage-labs/labs.service.ts");
/* harmony import */ var _shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/abstract-table-component */ "./src/app/shared/abstract-table-component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ManageLabsComponent = /** @class */ (function (_super) {
    __extends(ManageLabsComponent, _super);
    function ManageLabsComponent(serviceProvider) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.modalTitle = "New Lab";
        _this.modalBtnText = "Add Lab";
        _this.updating = false;
        _this.loadingLabs = false;
        _this.deleting = false;
        _this.changingPassword = false;
        _this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
        _this.loading = false;
        _this.labForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            lab_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            location: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        _this.passwordForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            confirmPassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
        });
        _this.columnsToDisplay = ['id', 'lab_name', 'location', 'username', 'password', 'edit', 'delete'];
        _this.getLabs = _this.getLabs.bind(_this);
        return _this;
    }
    ManageLabsComponent.prototype.addLab = function () {
        var _this = this;
        this.serviceProvider.addLab(this.labForm.value).subscribe(function (data) {
            _this.loading = false;
            _this.toast.success("Lab was added Successfully");
            _this.labForm.reset();
            M.updateTextFields();
            _this.getLabs();
        }, function (error) {
            _this.loading = false;
            console.log(error);
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else if (error.status == 422) {
                if ('username' in error.error.errors) {
                    _this.toast.error("Username already exist");
                }
                if ('lab_name' in error.error.errors) {
                    _this.toast.error("Lab Name already exist");
                }
            }
            else {
                _this.toast.error("An error occurred lab could not be added");
            }
        });
    };
    ManageLabsComponent.prototype.updateLab = function () {
        var _this = this;
        var lab = this.labForm.value;
        lab.id = this.activeLab.id;
        this.serviceProvider.updateLab(lab).subscribe(function (data) {
            _this.loading = false;
            _this.toast.success("Lab was updated Successfully");
            M.updateTextFields();
            _this.getLabs();
        }, function (error) {
            _this.loading = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else {
                _this.toast.error("An error occurred lab could not be added");
            }
        });
    };
    ManageLabsComponent.prototype.handleForm = function () {
        if (this.labForm.valid) {
            if (this.labForm.value.password != this.labForm.value.confirmPassword) {
                this.toast.error("Password do not match");
            }
            else {
                this.loading = true;
                if (this.updating) {
                    this.updateLab();
                }
                else {
                    this.addLab();
                }
            }
        }
        else {
            this.toast.error("Please complete All the required field");
        }
    };
    ManageLabsComponent.prototype.ngOnInit = function () {
        this.getLabs();
    };
    ManageLabsComponent.prototype.ngAfterViewInit = function () {
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
    };
    ManageLabsComponent.prototype.editLab = function (lab) {
        var _this = this;
        this.updating = true;
        this.modalTitle = "Update Lab Record";
        this.modalBtnText = "Update";
        this.activeLab = lab;
        var options = {
            onCloseEnd: function () {
                _this.updating = false;
                _this.modalTitle = "New Lab";
                _this.modalBtnText = "Add Lab";
                _this.labForm.reset();
                M.updateTextFields();
            }
        };
        M.Modal.init(this.labModal.nativeElement, options, options);
        var modal = M.Modal.getInstance(this.labModal.nativeElement, options);
        this.labForm.patchValue(lab);
        this.labForm.patchValue({ password: 'null', confirmPassword: "null" });
        M.updateTextFields();
        modal.open();
    };
    ManageLabsComponent.prototype.deleteLab = function (id, index) {
        var _this = this;
        var toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
        var options = {
            cancel: true,
            cancelText: 'Cancel',
            cancelCallBack: function (event) { },
            confirm: true,
            confirmText: 'Delete',
            confirmCallBack: function (event) {
                _this.deleting = true;
                _this.serviceProvider.deleteLab(id).subscribe(function (res) {
                    toast.success('Lab record was deleted succesfully');
                    _this.deleting = false;
                    _this.labs.splice(index, 1);
                    _this.matDatasource.data = _this.labs;
                    _this.matDatasource.paginator = _this.paginator;
                }, function (err) {
                    toast.error("Lab record could not be deleted at the moment please try again");
                });
            }
        };
        roar('Delete Record', 'Do you realy want to  <strong>Delete </strong> this record', options);
    };
    ManageLabsComponent.prototype.changePassword = function (id) {
        var modal = M.Modal.init(this.passwordModal.nativeElement);
        modal.open();
        this.activeID = id;
    };
    ManageLabsComponent.prototype.updatePassword = function () {
        var _this = this;
        if (this.passwordForm.valid) {
            if (this.passwordForm.value.password != this.passwordForm.value.confirmPassword) {
                this.toast.error("Password do not match");
            }
            else {
                this.changingPassword = true;
                this.serviceProvider.updatePassword(this.passwordForm.value.password, this.activeID)
                    .subscribe(function (data) {
                    _this.changingPassword = false;
                    _this.toast.success("Password updated Successfully");
                    _this.passwordForm.reset();
                }, function (err) {
                    _this.changingPassword = false;
                    if (err.status == 0) {
                        _this.toast.error("Can't connect to server please try again");
                    }
                    else {
                        _this.toast.error("An error occurred password could not be updated");
                    }
                });
            }
        }
        else {
            this.toast.error("Please complete All the required field");
        }
    };
    ManageLabsComponent.prototype.getLabs = function () {
        var _this = this;
        this.loadingLabs = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getLabs().subscribe(function (data) {
            _this.labs = data;
            _this.matDatasource.data = _this.labs;
            _this.matDatasource.paginator = _this.paginator;
            _this.loadingLabs = false;
        }, function (err) {
            _this.loadingLabs = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('labmodal'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ManageLabsComponent.prototype, "labModal", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('password'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ManageLabsComponent.prototype, "passwordModal", void 0);
    ManageLabsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-manage-labs',
            template: __webpack_require__(/*! ./manage-labs.component.html */ "./src/app/manage-labs/manage-labs.component.html"),
            styles: [__webpack_require__(/*! ./manage-labs.component.scss */ "./src/app/manage-labs/manage-labs.component.scss")]
        }),
        __metadata("design:paramtypes", [_labs_service__WEBPACK_IMPORTED_MODULE_3__["LabsService"]])
    ], ManageLabsComponent);
    return ManageLabsComponent;
}(_shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_4__["AbstractTableComponent"]));



/***/ }),

/***/ "./src/app/materialize-design/materialize-design.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/materialize-design/materialize-design.module.ts ***!
  \*****************************************************************/
/*! exports provided: MaterializeDesignModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterializeDesignModule", function() { return MaterializeDesignModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MaterializeDesignModule = /** @class */ (function () {
    function MaterializeDesignModule() {
    }
    MaterializeDesignModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTreeModule"],
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTreeModule"],
            ],
            declarations: []
        })
    ], MaterializeDesignModule);
    return MaterializeDesignModule;
}());



/***/ }),

/***/ "./src/app/medical-microbiology/abstract-medical-microbiology-component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/medical-microbiology/abstract-medical-microbiology-component.ts ***!
  \*********************************************************************************/
/*! exports provided: AbstractMedicalMicrobiologyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractMedicalMicrobiologyComponent", function() { return AbstractMedicalMicrobiologyComponent; });
/* harmony import */ var _medical_microbiology_form_medical_microbiology_form_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./medical-microbiology-form/medical-microbiology-form.component */ "./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AbstractMedicalMicrobiologyComponent = /** @class */ (function () {
    function AbstractMedicalMicrobiologyComponent() {
        this.saving = false;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
    }
    AbstractMedicalMicrobiologyComponent.prototype.resetForm = function () {
        this.reportForm.reset();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_medical_microbiology_form_medical_microbiology_form_component__WEBPACK_IMPORTED_MODULE_0__["MedicalMicrobiologyFormComponent"]),
        __metadata("design:type", Object)
    ], AbstractMedicalMicrobiologyComponent.prototype, "reportForm", void 0);
    return AbstractMedicalMicrobiologyComponent;
}());



/***/ }),

/***/ "./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.html":
/*!*************************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.html ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"New Medical Microbiology and Parasitology report\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/medical-microbiology-and-parasitology/new']\">New Medical Microbiology and Parasitology report</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t<app-linear-loading [showLoading]=\"saving\"></app-linear-loading>\n\t<med-microbiology-form [saving]=\"saving\" (submit)=\"onSubmit($event)\"></med-microbiology-form>\n</div> \n<app-modal></app-modal>"

/***/ }),

/***/ "./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.scss":
/*!*************************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.scss ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.ts":
/*!***********************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.ts ***!
  \***********************************************************************************************************/
/*! exports provided: CreateMedicalMicrobiologyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateMedicalMicrobiologyComponent", function() { return CreateMedicalMicrobiologyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _abstract_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../abstract-medical-microbiology-component */ "./src/app/medical-microbiology/abstract-medical-microbiology-component.ts");
/* harmony import */ var _medical_microbiology_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../medical-microbiology.service */ "./src/app/medical-microbiology/medical-microbiology.service.ts");
/* harmony import */ var _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/modal/modal.component */ "./src/app/shared/modal/modal.component.ts");
/* harmony import */ var _settings_setting_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../settings/setting.service */ "./src/app/settings/setting.service.ts");
/* harmony import */ var _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/AfterSaveActions */ "./src/app/shared/AfterSaveActions.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CreateMedicalMicrobiologyComponent = /** @class */ (function (_super) {
    __extends(CreateMedicalMicrobiologyComponent, _super);
    function CreateMedicalMicrobiologyComponent(serviceProvider, setting) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.setting = setting;
        return _this;
    }
    CreateMedicalMicrobiologyComponent.prototype.ngOnInit = function () {
    };
    CreateMedicalMicrobiologyComponent.prototype.onSubmit = function (report) {
        var _this = this;
        var settings = this.setting.getLocalSettings().after_save_action;
        if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].ALWAYS_ASK) {
            this.modal.openModal(function (options) {
                if (options) {
                    _this.saveReport(report, options);
                }
                else {
                    _this.saveReport(report, {});
                }
            });
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].SMS_PATIENT) {
            var options = {
                sms: true
            };
            this.saveReport(report, options);
            this.saveReport(report, options);
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].EMAIL_PATIENT) {
            var options = {
                email: true
            };
        }
        else if (settings == _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_5__["AfterSaveActions"].EMAIL_AND_SMS_PATIENT) {
            var options = {
                sms: true,
                email: true
            };
            this.saveReport(report, options);
        }
        else {
            this.saveReport(report, {});
        }
    };
    CreateMedicalMicrobiologyComponent.prototype.saveReport = function (report, options) {
        var _this = this;
        this.saving = true;
        this.serviceProvider.createReport(report, options).subscribe(function (data) {
            _this.saving = false;
            _this.toast.success('Report was saved successfully');
            _this.resetForm();
        }, function (error) {
            _this.saving = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else {
                _this.toast.error("An error occurred report could not be saved");
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__["ModalComponent"]),
        __metadata("design:type", _shared_modal_modal_component__WEBPACK_IMPORTED_MODULE_3__["ModalComponent"])
    ], CreateMedicalMicrobiologyComponent.prototype, "modal", void 0);
    CreateMedicalMicrobiologyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-medical-microbiology',
            template: __webpack_require__(/*! ./create-medical-microbiology.component.html */ "./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.html"),
            styles: [__webpack_require__(/*! ./create-medical-microbiology.component.scss */ "./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.scss")]
        }),
        __metadata("design:paramtypes", [_medical_microbiology_service__WEBPACK_IMPORTED_MODULE_2__["MedicalMicrobiologyService"], _settings_setting_service__WEBPACK_IMPORTED_MODULE_4__["SettingService"]])
    ], CreateMedicalMicrobiologyComponent);
    return CreateMedicalMicrobiologyComponent;
}(_abstract_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_1__["AbstractMedicalMicrobiologyComponent"]));



/***/ }),

/***/ "./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Medical Microbiology and Parasitology report update\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n     <li class=\"breadcrumb-item\"><a [routerLink]=\"['/medical-microbiology-and-parasitology/reports']\" >Reports</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/medical-microbiology-and-parasitology/edit/'+id]\">Update report</a> </li>\n</app-title-panel>\n\n\t<connection-error [show]=\"conError\" [retryCb]=\"getReport\"></connection-error>\n\t<server-error [show]=\"serverError\" [retryCb]=\"getReport\"></server-error>\n\t<app-loading [showLoading]=\"loadingReport\" loadingInfo=\"Loading report data please wait\" ></app-loading>\n\n<div class=\"page-content\">\n\t<app-linear-loading [showLoading]=\"saving\"></app-linear-loading>\n\t<med-microbiology-form [saving]=\"saving\" \n\t\t\t\t\t\t\t[report]=\"report\" \n\t\t\t\t\t\t\t(submit)=\"onSubmit($event)\" \n\t\t\t\t\t\t\tsaveBtnText=\"Update Report\" \n\t\t\t\t\t\t\t*ngIf=\"!loadingReport && !conError && !serverError\">\n\t\t\t\t\t\t\t\t\n\t</med-microbiology-form>\n</div> "

/***/ }),

/***/ "./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: EditMedicalMicrobiologyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditMedicalMicrobiologyComponent", function() { return EditMedicalMicrobiologyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _abstract_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../abstract-medical-microbiology-component */ "./src/app/medical-microbiology/abstract-medical-microbiology-component.ts");
/* harmony import */ var _medical_microbiology_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../medical-microbiology.service */ "./src/app/medical-microbiology/medical-microbiology.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditMedicalMicrobiologyComponent = /** @class */ (function (_super) {
    __extends(EditMedicalMicrobiologyComponent, _super);
    function EditMedicalMicrobiologyComponent(serviceProvider, route) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.route = route;
        _this.id = null;
        _this.conError = false;
        _this.serverError = false;
        _this.loadingReport = false;
        _this.getReport = _this.getReport.bind(_this);
        return _this;
    }
    EditMedicalMicrobiologyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params.id;
            _this.getReport();
        });
    };
    EditMedicalMicrobiologyComponent.prototype.onSubmit = function (report) {
        var _this = this;
        this.saving = true;
        report.id = this.id;
        this.report = report;
        this.serviceProvider.updateReport(report).subscribe(function (data) {
            _this.saving = false;
            _this.toast.success('Report was updated successfully');
        }, function (error) {
            _this.saving = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to server please try again");
            }
            else {
                _this.toast.error("An error occurred report could not be updated");
            }
        });
    };
    EditMedicalMicrobiologyComponent.prototype.getReport = function () {
        var _this = this;
        this.loadingReport = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReport(this.id).subscribe(function (report) {
            console.log(report);
            var keys = Object.keys(report);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                if (report[key] === null || report[key] == 'null') {
                    report[key] = '';
                }
                else if (report[key] === '') {
                    report[key] = '';
                }
            }
            _this.report = report;
            _this.loadingReport = false;
        }, function (err) {
            _this.loadingReport = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    EditMedicalMicrobiologyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-medical-microbiology',
            template: __webpack_require__(/*! ./edit-medical-microbiology.component.html */ "./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.html"),
            styles: [__webpack_require__(/*! ./edit-medical-microbiology.component.scss */ "./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.scss")]
        }),
        __metadata("design:paramtypes", [_medical_microbiology_service__WEBPACK_IMPORTED_MODULE_2__["MedicalMicrobiologyService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], EditMedicalMicrobiologyComponent);
    return EditMedicalMicrobiologyComponent;
}(_abstract_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_1__["AbstractMedicalMicrobiologyComponent"]));



/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-panel report-panel near-loading\">\n\t<form [formGroup] = 'medMicroForm'>\n\t\t<section [ngClass]=\"{'active':activePane===0}\" class=\"input-section animated fadeIn\">\n\t\t\t<h5 class=\"card-title\">Basic Test information</h5>\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t <select (change)=\"onPatientSelected($event)\" class=\"validate\"  formControlName=\"patient_id\" required>\n\t\t\t\t\t\t \t<option value=\"\" ></option>\n\t\t\t\t\t\t \t\n\t\t\t\t\t\t     <option value=\"new\">Add new patient</option>\n\t\t\t\t\t\t     <option disabled *ngIf=\"loadingPatients\">Loading patients please wait</option>\n\t\t\t\t\t\t     <option value=\"retry\"  *ngIf=\"errorOccurred\" >An error occurred, click here to retry loading patients</option>\n\t\t\t\t\t\t\t <option *ngFor=\"let patient of patients\" value=\"{{patient.id}}\">{{patient.first_name+' '+patient.middle_name+' '+patient.surname }}</option>}\n\t\t\t\t\t\t\toption\n\t\t\t\t\t    </select>\n    \t\t\t\t\t<label>Select patient</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"requested-by\" formControlName=\"requested_by\" class=\"validate\" required>\n\t\t\t\t\t\t<label for=\"requested-by\">Requested By</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"clinical-detail\" formControlName=\"clinical_detail\">\n\t\t\t\t\t\t<label for=\"clinical-detail\">Clinical Detail</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"exam-required\" formControlName=\"exam_required\">\n\t\t\t\t\t\t<label for=\"exam-required\">Exam Required</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"specimen\" formControlName=\"specimen\">\n\t\t\t\t\t\t<label for=\"specimen\">Specimen</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t<input type=\"text\" id=\"clinic\" formControlName=\"clinic\">\n\t\t\t\t\t\t<label for=\"clinic\">Clinic</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</section>\n\t\t<section [ngClass]=\"{'active':activePane===1}\" class=\"input-section animated fadeIn\">\n                    <div class=\"row\">\n\n                        <div class=\"col s12\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"therapy\" formControlName=\"antibiotic_therapy\">\n                                <label for=\"therapy\">Current Antibiotics Therapy</label>\n                            </div>\n                        </div>\n\n\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"macroscopy\" formControlName=\"macroscopy\">\n                                <label for=\"macroscopy\">Macroscopy</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"cell-count\" formControlName=\"cell_count\">\n                                <label for=\"cell-count\">Cell Count</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"dif-wbc\" formControlName=\"differential_wbc\">\n                                <label for=\"dif-wbc\">Differential WBC</label>\n                            </div>\n                        </div>\n                    </div>\n                    <h6>Wet preparation (HPF)</h6>\n                    <div class=\"row\">\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"pus-cell\" formControlName=\"pus_cell\">\n                                <label for=\"pus-cell\">Pus Cell</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"rbc\" formControlName=\"rbc\">\n                                <label for=\"rbc\">RBC</label>\n                            </div>\n                        </div>\n                        <div class=\"col m5\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"epithelial\" formControlName=\"epithelial_cell\">\n                                <label for=\"epithelial\">Epithelial Cells</label>\n                            </div>\n                        </div>\n                    </div>\n        </section >\n        <section [ngClass]=\"{'active':activePane===2}\" class=\"input-section animated fadeIn\">\n                    <h6>Gramstain</h6>\n                    <div class=\"row\">\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"pus-cell-g\" formControlName=\"gram_pus_cell\">\n                                <label for=\"pus-cell-g\">Pus Cell</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"gve\" formControlName=\"gve_ccoci_pos\">\n                                <label for=\"gve\">G+ VE: COCCI</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"rods-p\" formControlName=\"gve_rods_pos\">\n                                <label for=\"rods-p\">G+ VE: RODS</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"gve-n\" formControlName=\"gve_ccoci_neg\">\n                                <label for=\"gve-n\">G- VE: COCCI</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"rods-n\" formControlName=\"gve_rods_neg\">\n                                <label for=\"rods-n\">G- VE: RODS</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"epithelial-g\" formControlName=\"gram_epithelial_cell\">\n                                <label for=\"epithelial-g\">Epithelial cells</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"znstain\" formControlName=\"zn_stain_pus_cell\">\n                                <label for=\"znstain\">ZN STAIN: Pus Cells</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"afb1\" formControlName=\"zn_stain_afb_x1\">\n                                <label for=\"afb1\">ZN STAIN:AFB x1</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"afb2\" formControlName=\"zn_stain_afb_x2\">\n                                <label for=\"afb2\">ZN STAIN:AFB x2</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"afb3\" formControlName=\"zn_stain_afb_x3\">\n                                <label for=\"afb3\">ZN STAIN:AFB x3</label>\n                            </div>\n                        </div>\n                    </div>\n        </section>\n        <section [ngClass]=\"{'active':activePane===3}\" class=\"input-section animated fadeIn\">\n                    <h6>SEMEM ANALYSIS</h6>\n                    <div class=\"row\">\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"test-time\" formControlName=\"test_time\">\n                                <label for=\"test-time\">Production/Test time</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"abstinence\" formControlName=\"abstinence\">\n                                <label for=\"abstinence\">Abstinence</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"volume\" formControlName=\"volume\">\n                                <label for=\"volume\">Volume</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"colour\" formControlName=\"colour\">\n                                <label for=\"colour\">Colour</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"liquefaction\" formControlName=\"liquefaction\">\n                                <label for=\"liquefaction\">Liquefaction</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"viscosity\" formControlName=\"viscosity\">\n                                <label for=\"viscosity\">Viscosity</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"ph\" formControlName=\"ph\">\n                                <label for=\"ph\">pH</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"viability\" formControlName=\"viability\">\n                                <label for=\"viability\">Viability</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"motility-active\" formControlName=\"motility_active\">\n                                <label for=\"motility-active\">Motility: Active </label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"motility-stuggish\" formControlName=\"motility_sluggish\">\n                                <label for=\"motility-stuggish\">Motility: Stuggish </label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"motility-non-motile\" formControlName=\"motility_non_motile\">\n                                <label for=\"motility-non-motile\">Motility: Non-motile </label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"count\" formControlName=\"count\">\n                                <label for=\"count\">COUNT(X 10<sup>6</sup> sperm/ml)</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"hpf-pus-cell\" formControlName=\"hpf_pus_cell\">\n                                <label for=\"hpf-pus-cell\">Particulate Debris(HPF): Pus cell</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"morp-pus-cell\" formControlName=\"morph_pus_cell\">\n                                <label for=\"morp-pus-cell\">Morphology(%): Pus cells</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"hpf-rbc\" formControlName=\"hpf_rbc\">\n                                <label for=\"hpf-rbc\">Particulate Debris(HPF): RBC</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"morp-rbc\" formControlName=\"morph_rbc\">\n                                <label for=\"morp-rbc\">Morphology(%): RBC</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"hpf-epithelial\" formControlName=\"hpf_epithelial_cell\">\n                                <label for=\"hpf-epithelial\">Particulate Debris(HPF): Epithelial</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"morp-epithelial\" formControlName=\"morph_epithelial_cell\">\n                                <label for=\"morp-epithelial\">Morphology(%): Epithelial</label>\n                            </div>\n                        </div>\n                    </div>\n        </section>\n        <section [ngClass]=\"{'active':activePane===4}\" class=\"input-section animated fadeIn\">\n                    <h6>STOOL MICROSCOPY</h6>\n                    <div class=\"row\">\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"macroscopy-st\" formControlName=\"stool_macroscopy\">\n                                <label for=\"macroscopy-st\">Macroscopy</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"wet-preparation\" formControlName=\"wet_preparation\">\n                                <label for=\"wet-preparation\">Wet Preparation</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"concentration\" formControlName=\"concentration\">\n                                <label for=\"concentration\">Concentration</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"malaria-parasite\" formControlName=\"malaria_parasite\">\n                                <label for=\"malaria-parasite\">Malaria Parasite</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"microfilaria-skin\" formControlName=\"microfilaria_skin\">\n                                <label for=\"microfilaria-skin\">Microfilaria: Skin</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"microfilaria-blood\" formControlName=\"microfilaria_blood\">\n                                <label for=\"microfilaria-blood\">Microfilaria: Blood</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"occult-blood\" formControlName=\"stool_occult_blood\">\n                                <label for=\"occult-blood\">Stool Occult Blood</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\" >\n                                <input type=\"text\" id=\"vdrl\" formControlName=\"vdrl_test\">\n                                <label for=\"vdrl\">VDRL Test</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"aso\" formControlName=\"aso_titre\">\n                                <label for=\"aso\">ASO titre</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"chlamydia\" formControlName=\"chlamydia_serology\">\n                                <label for=\"chlamydia\">Chlamydia Serology</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"hepb\" formControlName=\"hepbag\">\n                                <label for=\"hepb\">Hep B Ag</label>\n                            </div>\n                        </div>\n                        <div class=\"col m2\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"hcv\" formControlName=\"hcv_ab\">\n                                <label for=\"hcv\">HCV Ab</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"helicobacter\" formControlName=\"helicobacter\">\n                                <label for=\"helicobacter\">Helicobacter Pylori Ab</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"retroviral\" formControlName=\"retroviral_screening\" >\n                                <label for=\"retroviral\">Retroviral Screening Test</label>\n                            </div>\n                        </div>\n\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"retroviral_con\" formControlName=\"retroviral_confirmation\" >\n                                <label for=\"retroviral_con\">Retroviral Confirmation</label>\n                            </div>\n                        </div>\n\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"cd-count\" formControlName=\"cd4_count\">\n                                <label for=\"cd-count\">CD<sub>4</sub> Count</label>\n                            </div>\n                        </div>\n                    </div>\n        </section>\n        <section [ngClass]=\"{'active':activePane===5}\" class=\"input-section animated fadeIn\">\n                    <h6>MANTOUX TEST</h6>\n                    <div class=\"row\">\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"given\" formControlName=\"given\">\n                                <label for=\"given\">Given</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"read\" formControlName=\"read\">\n                                <label for=\"read\">Read</label>\n                            </div>\n                        </div>\n                        <div class=\"col m4\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"result\" formControlName=\"result\">\n                                <label for=\"result\">Result</label>\n                            </div>\n                        </div>\n                        <h6>SEROLOGICAL TESTS</h6>\n                        <div class=\"col m6\">\n                            <table class=\"new-report-table\">\n                                <caption>WIDAL TEST</caption>\n                                <thead>\n                                    <tr class=\"center-align\">\n                                        <th rowspan=\"2\" class=\"center-align\" width=\"30%\">\n                                            Antigen\n                                        </th>\n                                        <th colspan=\"2\" class=\"center-align\">\n                                            Antibody Titre\n                                        </th>\n                                    </tr>\n                                    <tr >\n                                        <td class=\"center-align\" width=\"35%\">\"O\"</td>\n                                        <td class=\"center-align\" width=\"35%\">\"H\"</td>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr>\n                                        <td>S. Typhi</td>\n                                        <td class=\"table-field\" (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_typhi_o\" class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                        <td class=\"table-field\"  (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_typhi_h\" class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>S. Paratyphi A</td>\n                                        <td class=\"table-field\" (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_paratyphi_a_o\" class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                        <td class=\"table-field\" (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_paratyphi_a_h\" class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>S. Paratyphi B</td>\n                                        <td class=\"table-field\" (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_paratyphi_b_o\" class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                        <td class=\"table-field\" (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_paratyphi_b_h\" class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>S. Paratyphi C</td>\n                                        <td class=\"table-field\" (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_paratyphi_c_o\"  class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                        <td class=\"table-field\" (dblclick)=\"showTextInput($event)\" title=\"Double click to edit\">\n                                            <label>Double click to edit</label>\n                                            <input type=\"text\" formControlName=\"s_paratyphi_c_h\" class=\"browser-default table-input\" (focusout)=\"hideTextInput($event)\">\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                        <div class=\"col m6\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"comment\" formControlName=\"comment\">\n                                <label for=\"comment\">Comment</label>\n                            </div>\n                        </div>\n                    </div>\n        </section>\n        <section [ngClass]=\"{'active':activePane===6}\" class=\"input-section animated fadeIn\">\n                    <h6>Culture</h6>\n                    <div class=\"input-field\">\n                        <input type=\"text\" id=\"organism\" formControlName=\"organism_isolated\">\n                        <label for=\"organism\">Organism(s) isolated</label>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col m7\">\n                            <table class=\"med-report-table\">\n                                <thead>\n                                    <tr>\n                                        <th rowspan=\"2\">Antimicrobial Agents</th>\n                                        <th colspan=\"4\">Sensitivity</th>\n                                    </tr>\n                                    <tr>\n                                        <th >+++</th>\n                                        <th>++</th>\n                                        <th>+</th>\n                                        <th>R</th>\n                                    </tr>\n                                </thead>\n                                <tbody>\n                                    <tr>\n                                        <td>Amoxyciline/clavulanate</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycillin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"amoxycillin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycillin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"amoxycillin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycillin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"amoxycillin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycillin\" value=\"r\" class=\"with-gap\" type=\"radio\"  formControlName=\"amoxycillin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Cefuroxime</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cefuroxime\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"cefuroxime\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cefuroxime\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"cefuroxime\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cefuroxime\" value=\"p\" class=\"with-gap\" type=\"radio\"  formControlName=\"cefuroxime\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cefuroxime\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"cefuroxime\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Ceftriaxone</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftriaxone\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"ceftriaxone\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftriaxone\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"ceftriaxone\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftriaxone\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"ceftriaxone\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftriaxone\" value=\"r\" class=\"with-gap\" type=\"radio\"  formControlName=\"ceftriaxone\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Ceftazidime</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftazidime\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"ceftazidime\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftazidime\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"ceftazidime\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftazidime\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"ceftazidime\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ceftazidime\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"ceftazidime\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Cotrimoxazole</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cotrimoxazole\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"cotrimoxazole\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cotrimoxazole\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"cotrimoxazole\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cotrimoxazole\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"cotrimoxazole\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"cotrimoxazole\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"cotrimoxazole\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Norfloxacin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"norfloxacin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"norfloxacin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"norfloxacin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"norfloxacin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"norfloxacin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"norfloxacin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"norfloxacin\" value=\"r\" class=\"with-gap\" type=\"radio\"  formControlName=\"norfloxacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Tebacylin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"tebacylin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"tebacylin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"tebacylin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"tebacylin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"tebacylin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"tebacylin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"tebacylin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"tebacylin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Nalidixic Acid</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nalidixic_acid\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"nalidixic_acid\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nalidixic_acid\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"nalidixic_acid\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nalidixic_acid\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"nalidixic_acid\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nalidixic_acid\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"nalidixic_acid\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Streptomycin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"streptomacycin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"streptomacycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"streptomacycin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"streptomacycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"streptomacycin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"streptomacycin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"streptomacycin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"streptomacycin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Septrin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"septrin\" value=\"ppp\" class=\"with-gap\" type=\"radio\"  formControlName=\"septrin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"septrin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"septrin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"septrin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"septrin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"septrin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"septrin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Ciprofloxacin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ciprofloxin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"ciprofloxin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ciprofloxin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"ciprofloxin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ciprofloxin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"ciprofloxin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ciprofloxin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"ciprofloxin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Chloramphenicol</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"chloramphenicol\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"chloramphenicol\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"chloramphenicol\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"chloramphenicol\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"chloramphenicol\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"chloramphenicol\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"chloramphenicol\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"chloramphenicol\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Erythromycin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"erythromycin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"erythromycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"erythromycin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"erythromycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"erythromycin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"erythromycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"erythromycin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"erythromycin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Gentamycin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"gentymacin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"gentymacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"gentymacin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"gentymacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"gentymacin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"gentymacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"gentymacin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"gentymacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Ofloxacin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ofloxacin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"ofloxacin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ofloxacin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"ofloxacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ofloxacin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"ofloxacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ofloxacin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"ofloxacin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Nitrofurantion</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nitrofuratoin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"nitrofuratoin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nitrofuratoin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"nitrofuratoin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nitrofuratoin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"nitrofuratoin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"nitrofuratoin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"nitrofuratoin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Ampiclox</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ampiclox\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"ampiclox\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ampiclox\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"ampiclox\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ampiclox\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"ampiclox\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"ampiclox\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"ampiclox\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Amoxycilin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycilin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"amoxycilin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycilin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"amoxycilin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycilin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"amoxycilin\" />\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"amoxycilin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"amoxycilin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Clindamycin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"clindamycin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"clindamycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"clindamycin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"clindamycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"clindamycin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"clindamycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"clindamycin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"clindamycin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                    <tr>\n                                        <td>Levofloxcin</td>\n                                        <td>\n                                            <label>\n                                                <input name=\"levofloxcin\" value=\"ppp\" class=\"with-gap\" type=\"radio\" formControlName=\"levofloxcin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"levofloxcin\" value=\"pp\" class=\"with-gap\" type=\"radio\" formControlName=\"levofloxcin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"levofloxcin\" value=\"p\" class=\"with-gap\" type=\"radio\" formControlName=\"levofloxcin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                        <td>\n                                            <label>\n                                                <input name=\"levofloxcin\" value=\"r\" class=\"with-gap\" type=\"radio\" formControlName=\"levofloxcin\"/>\n                                                <span></span>\n                                            </label>\n                                        </td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </div>\n                    </div>\n        </section>\n    </form>\n</div>\n<div class=\"action-buttons\">\n    <div class=\"row\">\n      <div class=\"col s1\">\n        <button (click)=\"prevPane()\"  *ngIf=\"showPrevBtn\" type=\"button\" class=\"btn btn-large waves-effect\">Previous</button>\n      </div>\n      <div class=\"col offset-s8 s3\">\n        <button type=\"button\" *ngIf=\"showNextBtn\" class=\"btn btn-large waves-effect\" id=\"next-btn\" (click)=\"nextPane()\" >Next</button>\n        <button type=\"button\" *ngIf=\"showSave\" class=\"btn btn-large waves-effect\" [disabled]=\"saving\"  (click)=\"handleSubmit($event)\" >{{saveBtnText}}</button>\n      </div>\n\n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: MedicalMicrobiologyFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalMicrobiologyFormComponent", function() { return MedicalMicrobiologyFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../patient/patient.service */ "./src/app/patient/patient.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _shared_abstract_report_form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/abstract-report-form */ "./src/app/shared/abstract-report-form.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MedicalMicrobiologyFormComponent = /** @class */ (function (_super) {
    __extends(MedicalMicrobiologyFormComponent, _super);
    function MedicalMicrobiologyFormComponent(router, patientService, activeRoute) {
        var _this = _super.call(this, router, patientService, activeRoute) || this;
        _this.router = router;
        _this.patientService = patientService;
        _this.activeRoute = activeRoute;
        _this.medMicroForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            patient_id: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            requested_by: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            clinical_detail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            exam_required: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            specimen: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            clinic: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            antibiotic_therapy: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            macroscopy: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            cell_count: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            differential_wbc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            pus_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            rbc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            epithelial_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            gram_pus_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            gve_ccoci_pos: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            gve_rods_pos: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            gve_ccoci_neg: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            gve_rods_neg: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            gram_epithelial_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            zn_stain_pus_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            zn_stain_afb_x1: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            zn_stain_afb_x2: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            zn_stain_afb_x3: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            test_time: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            abstinence: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            volume: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            colour: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            liquefaction: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            viscosity: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ph: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            viability: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            motility_active: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            motility_sluggish: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            motility_non_motile: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            count: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            hpf_pus_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            morph_pus_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            hpf_rbc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            morph_rbc: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            hpf_epithelial_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            morph_epithelial_cell: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            stool_macroscopy: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            wet_preparation: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            concentration: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            malaria_parasite: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            microfilaria_skin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            microfilaria_blood: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            stool_occult_blood: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            vdrl_test: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            aso_titre: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            chlamydia_serology: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            hepbag: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            hcv_ab: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            helicobacter: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            retroviral_screening: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            retroviral_confirmation: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            cd4_count: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            given: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            read: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            result: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_typhi_o: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_typhi_h: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_paratyphi_a_o: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_paratyphi_a_h: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_paratyphi_b_o: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_paratyphi_b_h: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_paratyphi_c_o: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            s_paratyphi_c_h: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            comment: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            organism_isolated: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](),
            amoxycillin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            cefuroxime: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ceftriaxone: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ceftazidime: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            cotrimoxazole: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            norfloxacin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            tebacylin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            nalidixic_acid: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            streptomacycin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            septrin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ciprofloxin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            chloramphenicol: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            erythromycin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            gentymacin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ofloxacin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            nitrofuratoin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            ampiclox: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            amoxycilin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            clindamycin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            levofloxcin: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
        });
        _this.submit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        _this.saveBtnText = "Save Report";
        return _this;
    }
    MedicalMicrobiologyFormComponent.prototype.ngOnChanges = function () {
        if (this.report) {
            this.medMicroForm.patchValue(__assign({}, this.report));
        }
    };
    MedicalMicrobiologyFormComponent.prototype.reset = function () {
        var _this = this;
        this.medMicroForm.reset();
        setTimeout(function () { _this.initMaterial(); M.updateTextFields(); }, 200);
        this.activePane = 0;
        this.showPane();
    };
    MedicalMicrobiologyFormComponent.prototype.showPane = function () {
        if (this.activePane > 5) {
            this.showNextBtn = false;
            this.showSave = true;
        }
        else {
            this.showNextBtn = true;
            this.showSave = false;
        }
        if (this.activePane > 0) {
            this.showPrevBtn = true;
        }
        else {
            this.showPrevBtn = false;
        }
    };
    MedicalMicrobiologyFormComponent.prototype.showTextInput = function (event) {
        event.preventDefault();
        var td = event.currentTarget;
        if (td.lastChild instanceof Text) {
            td.removeChild(td.lastChild);
        }
        td.querySelector('label').style.display = 'none';
        td.querySelector('input').style.display = 'block';
        td.querySelector('input').focus();
    };
    MedicalMicrobiologyFormComponent.prototype.hideTextInput = function (event) {
        var input = event.currentTarget;
        var td = input.parentElement;
        if (input.value.length != 0) {
            td.appendChild(document.createTextNode(input.value));
            td.querySelector('label').style.display = 'none';
            td.querySelector('input').style.display = 'none';
        }
        else {
            td.querySelector('label').style.display = 'block';
            td.querySelector('input').style.display = 'none';
        }
    };
    MedicalMicrobiologyFormComponent.prototype.ngAfterViewInit = function () {
        _super.prototype.ngAfterViewInit.call(this);
        var cells = document.getElementsByClassName('table-field');
        [].map.call(cells, function (cell) {
            var td = cell;
            var input = td.querySelector('input');
            if (input.value.length > 0) {
                td.appendChild(document.createTextNode(input.value));
                td.querySelector('label').style.display = 'none';
                td.querySelector('input').style.display = 'none';
            }
        });
    };
    MedicalMicrobiologyFormComponent.prototype.handleSubmit = function () {
        var patient_id = Number.parseInt(this.medMicroForm.value.patient_id);
        if (isNaN(patient_id)) {
            new _MiniToast__WEBPACK_IMPORTED_MODULE_4__["default"]().warning("You have not seleted any patient");
            this.activePane = 0;
            this.showPane();
        }
        else {
            this.submit.emit(this.medMicroForm.value);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MedicalMicrobiologyFormComponent.prototype, "report", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], MedicalMicrobiologyFormComponent.prototype, "submit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], MedicalMicrobiologyFormComponent.prototype, "saveBtnText", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], MedicalMicrobiologyFormComponent.prototype, "saving", void 0);
    MedicalMicrobiologyFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'med-microbiology-form',
            template: __webpack_require__(/*! ./medical-microbiology-form.component.html */ "./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.html"),
            styles: [__webpack_require__(/*! ./medical-microbiology-form.component.scss */ "./src/app/medical-microbiology/medical-microbiology-form/medical-microbiology-form.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _patient_patient_service__WEBPACK_IMPORTED_MODULE_3__["PatientService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], MedicalMicrobiologyFormComponent);
    return MedicalMicrobiologyFormComponent;
}(_shared_abstract_report_form__WEBPACK_IMPORTED_MODULE_5__["AbstractReportForm"]));



/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.html":
/*!***************************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.html ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Medical Microbiology and Parasitology Reports\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/medical-microbiology-and-parasitology/reports']\">Medical Microbiology and Parasitology Reports</a> </li>\n</app-title-panel>\n\n<div class=\"page-content\">\n\n \n\n\n     <app-loading [showLoading]='loading'></app-loading>\n     <connection-error [show]=\"!loading && conError \" [retryCb]=\"getReports\"></connection-error>\n     <server-error [show]=\"serverError && !loading\" [retryCb]=\"getReports\"></server-error>\n     <app-linear-loading [showLoading]=\"deleting\"></app-linear-loading>\n     <no-record [show]='matDatasource.data.length==0 && !loading && !conError && !serverError'>There is currently no haematology report</no-record>\n\n    <div class=\"card-panel near-loading\" *ngIf=\"matDatasource.data.length>0 && !loading && !conError && !serverError\">\n        <div class=\"header\">\n            <div class=\"row\">\n                <div class=\"col m3 offset-m9\">\n                    <div class=\"input-field\" *ngIf=\"reports?.length>0\">\n                        <input type=\"text\" id=\"filter\" (keyup)=\"filterTable($event.target.value)\">\n                        <label for=\"filter\">Filter</label>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"table-contents\">\n            <div class=\"wrapper animated fadeIn\">\n            <table mat-table [dataSource]=\"matDatasource\" matSort *ngIf=\"matDatasource.data.length!==0\">\n                <ng-container matColumnDef=\"id\">\n                    <th mat-header-cell *matHeaderCellDef> ID </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.id}} </td>\n                </ng-container>\n\n\n                <ng-container matColumnDef=\"patient_name\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Patient Name </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.patient.first_name+' '+report.patient.surname}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"exam_required\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Exam Required </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.exam_required}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"specimen\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Specimen </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.specimen}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"createdOn\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Created On </th>\n                    <td mat-cell *matCellDef=\"let report\"> {{report.created_at | date:mediumDate }} </td>\n                </ng-container>\n\n\n                 <ng-container matColumnDef=\"view\">\n                    <th mat-header-cell *matHeaderCellDef > View Details</th>\n                    <td mat-cell *matCellDef=\"let report\"> <a [routerLink]=\"['/medical-microbiology-and-parasitology/reports/'+report.id]\">View Details</a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"edit\">\n                    <th mat-header-cell *matHeaderCellDef> Edit </th>\n                    <td mat-cell *matCellDef=\"let report\"> <a [routerLink]=\"['/medical-microbiology-and-parasitology/edit/'+report.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"delete\">\n                    <th mat-header-cell *matHeaderCellDef> Delete </th>\n                    <td mat-cell *matCellDef=\"let report; let i = index;\"><button (click)=\"deleteReport(report.id,i)\" [disabled]=\"deleting\" class=\"btn red accent-3\"><i class=\"material-icons\">delete</i></button></td>\n                </ng-container>\n\n\n                <tr mat-header-row *matHeaderRowDef=\"columnsToDisplay\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: columnsToDisplay;\"></tr>\n\n\n               \n            </table>\n             <mat-paginator [pageSize]=\"10\" \n                            [pageSizeOptions]=\"[5, 10, 20]\" \n                            showFirstLastButtons\n                            *ngIf=\"matDatasource.data.length!==0\"\n                            ></mat-paginator>\n\n\n            </div>\n        </div>\n    </div>\n\n\n</div>"

/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.scss":
/*!***************************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.scss ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.ts":
/*!*************************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.ts ***!
  \*************************************************************************************************************/
/*! exports provided: MedicalMicrobiologyReportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalMicrobiologyReportsComponent", function() { return MedicalMicrobiologyReportsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _medical_microbiology_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../medical-microbiology.service */ "./src/app/medical-microbiology/medical-microbiology.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/abstract-table-component */ "./src/app/shared/abstract-table-component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MedicalMicrobiologyReportsComponent = /** @class */ (function (_super) {
    __extends(MedicalMicrobiologyReportsComponent, _super);
    function MedicalMicrobiologyReportsComponent(serviceProvider) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.loading = false;
        _this.errorOccurred = false;
        _this.deleting = false;
        _this.columnsToDisplay = ['id', 'patient_name', 'exam_required', 'specimen', 'createdOn', 'view', 'edit', 'delete'];
        _this.getReports = _this.getReports.bind(_this);
        return _this;
    }
    MedicalMicrobiologyReportsComponent.prototype.ngOnInit = function () {
        this.getReports();
    };
    MedicalMicrobiologyReportsComponent.prototype.getReports = function () {
        var _this = this;
        this.loading = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReports().subscribe(function (data) {
            _this.loading = false;
            _this.reports = data;
            _this.matDatasource.data = _this.reports;
            _this.matDatasource.paginator = _this.paginator;
        }, function (err) {
            _this.loading = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    MedicalMicrobiologyReportsComponent.prototype.deleteReport = function (id, index) {
        var _this = this;
        var toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
        var options = {
            cancel: true,
            cancelText: 'Cancel',
            cancelCallBack: function (event) { },
            confirm: true,
            confirmText: 'Delete',
            confirmCallBack: function (event) {
                _this.deleting = true;
                _this.serviceProvider.deleteReport(id).subscribe(function (res) {
                    toast.success('Report was deleted succesfully');
                    _this.deleting = false;
                    _this.reports.splice(index, 1);
                    _this.matDatasource.data = _this.reports;
                    _this.matDatasource.paginator = _this.paginator;
                }, function (err) {
                    toast.error("Report could not be deleted at the moment please try again");
                });
            }
        };
        roar('Delete Report', 'Do you realy want to  <strong>Delete </strong> this report', options);
    };
    MedicalMicrobiologyReportsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-medical-microbiology-reports',
            template: __webpack_require__(/*! ./medical-microbiology-reports.component.html */ "./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.html"),
            styles: [__webpack_require__(/*! ./medical-microbiology-reports.component.scss */ "./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.scss")]
        }),
        __metadata("design:paramtypes", [_medical_microbiology_service__WEBPACK_IMPORTED_MODULE_1__["MedicalMicrobiologyService"]])
    ], MedicalMicrobiologyReportsComponent);
    return MedicalMicrobiologyReportsComponent;
}(_shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__["AbstractTableComponent"]));



/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-view/medical-microbiology-view.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-view/medical-microbiology-view.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Medical Microbiology and Parasitology report details\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/medical-microbiology-and-parasitology/reports/']\">Medical Microbiology and Parasitology reports</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\n\n    <connection-error [show]=\"conError\" [retryCb]=\"getReport\"></connection-error>\n    <server-error [show]=\"serverError\" [retryCb]=\"getReport\"></server-error>\n    <app-loading [showLoading]=\"loadingReport\" loadingInfo=\"Loading report data please wait\" ></app-loading>\n\n    <app-printed-page>\n\t<div class=\"card-panel\" *ngIf=\"!loadingReport && !conError && !serverError\">\n\t\t\t<div class=\"view-report-table  medical-microb\">\n\t\t\t\t<table >\n\t\t\t\t\t<thead>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<th colspan=\"6\" class=\"test-title\">MEDICAL MICROBIOLOGY AND PARASITOLOGY</th>\n\t\t\t\t\t\t\t<th>LAB NO.: {{report?.lab_id}} </th>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Surname</label><br>\n\t\t\t\t\t\t\t\t{{report?.patient?.surname}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Other Names</label><br>\n\t\t\t\t\t\t\t\t{{report?.patient?.first_name+' '+report?.patient?.middle_name}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Age</label><br>\n\t\t\t\t\t\t\t\t{{report?.patient?.age}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Sex</label><br>\n\t\t\t\t\t\t\t\t{{report?.patient?.sex}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Date</label><br>\n\t\t\t\t\t\t\t\t{{report?.created_at | date:mediumDate }}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Clinic</label><br>\n\t\t\t\t\t\t\t\t{{report?.clinic}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Requested By</label><br>\n\t\t\t\t\t\t\t\t{{report?.requested_by}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t<label>Clinical Detail</label><br>\n\t\t\t\t\t\t\t\t{{report?.clinical_detail}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td colspan=\"2\">\n\t\t\t\t\t\t\t\t<label>Exam Required</label><br>\n\t\t\t\t\t\t\t\t{{report?.exam_required}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td colspan=\"2\">\n\t\t\t\t\t\t\t\t<label>Specimen</label><br>\n\t\t\t\t\t\t\t\t{{report?.specimen}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t<td colspan=\"2\">\n\t\t\t\t\t\t\t\t<label>Current Antibiotics Therapy</label><br>\n\t\t\t\t\t\t\t\t{{report?.antibiotic_therapy}}\n\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t</tr>\n\t\t\t\t\t</thead>\n\t\t\t\t\t<tbody>\n\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t<td colspan=\"2\">\n\t\t\t\t\t\t\t\t<p><strong>Microscopy: </strong>{{report?.macroscopy==''?'.................................................':report.macroscopy}}</p>\n\t\t\t\t\t\t\t\t<p><strong>Cell Count: </strong>{{report?.cell_count==''?'.................................................':report.cell_count}}</p>\n\t\t\t\t\t\t\t\t<p><strong>Differential WBC: </strong>{{report?.differential_wbc==''?'..................':report.differential_wbc}}</p>\n\t\t\t\t\t\t\t\t<h6>Wet Preparation (HPF)</h6>\n\t\t\t\t\t\t\t\t<p><strong>Pus Cells: </strong>{{report?.pus_cell==''?'.........':report.pus_cell}}</p>\n\t\t\t\t\t\t\t\t<p><strong>RBC: </strong>{{report?.rbc==''?'.........':report.rbc}}</p>\n\t\t\t\t\t\t\t\t<p><strong>Epithelial Cells: </strong>{{report?.epithelial_cell==''?'..............':report.epithelial_cell}}</p>\n\t\t\t\t\t\t\t\t<h6>Gramstain</h6>\n\t\t\t\t\t\t\t\t<p><strong>Pus Cell: </strong>{{report?.gram_pus_cell==''?'.........':report.gram_pus_cell}}</p>\n\t\t\t\t\t\t\t\t<p><strong>G +Ve COCCI: </strong>{{report?.gve_ccoci_pos==''?'.........':report.gve_ccoci_pos}}</p>\n\t\t\t\t\t\t\t\t<p><strong>G +Ve RODS: </strong>{{report?.gve_rods_pos==''?'.........':report.gve_rods_pos}}</p>\n\t\t\t\t\t\t\t\t<p><strong>G -Ve COCCI: </strong>{{report?.gve_ccoci_neg==''?'.........':report.gve_ccoci_neg}}</p>\n\t\t\t\t\t\t\t\t<p><strong>G -Ve RODS: </strong>{{report?.gve_rods_neg==''?'.........':report.gve_rods_neg}}</p>\n\t\t\t\t\t\t\t\t<p><strong>Epithelial cells: </strong>{{report?.gram_epithelial_cell==''?'.........':report.gram_epithelial_cell}}</p>\n\t\t\t\t\t\t\t\t<p><strong>ZN STAIN:</strong>\n\t\t\t\t\t\t\t\t\t<p><strong>Pus cell: </strong>{{report?.zn_stain_pus_cell==''?'.........':report.zn_stain_pus_cell}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>AFB x1: </strong>{{report?.zn_stain_afb_x1==''?'.........':report.zn_stain_afb_x1}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>AFB x2: </strong>{{report?.zn_stain_afb_x2==''?'.........':report.zn_stain_afb_x2}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>AFB x3: </strong>{{report?.zn_stain_afb_x3==''?'.........':report.zn_stain_afb_x3}}</p>\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t<td colspan=\"3\">\n\t\t\t\t\t\t\t\t\t<h6><u>SEMEM ANALYSIS</u></h6>\n\t\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\t\t<strong>Production/Test Time: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.test_time==''?'...............':report.test_time}}\n\t\t\t\t\t\t\t\t\t\t<strong>Abstinence: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.abstinence==''?'..............':report.abstinence}}\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\t\t<strong>Volume: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.volume==''?'...............':report.volume}}\n\t\t\t\t\t\t\t\t\t\t<strong>Colour: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.colour==''?'..............':report.colour }}\n\t\t\t\t\t\t\t\t\t\t<strong>Liquefaction: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.liquefaction==''?'..............':report.liquefaction }}\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\t\t<strong>Viscocity: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.viscocity==''?'...............':report.viscosity}}\n\t\t\t\t\t\t\t\t\t\t<strong>pH: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.ph==''?'..............':report.ph }}\n\t\t\t\t\t\t\t\t\t\t<strong>Viability: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.viability==''?'..............':report.viability }}\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Motility: </strong> Active: {{report?.motility_active==''?'..............':report.motility_active }}<br>\n\t\t\t\t\t\t\t\t\t\t<span style=\"margin-left:55px\">Sluggish: {{report?.motility_sluggish==''?'..............':report.motility_sluggish }}</span><br>\n\t\t\t\t\t\t\t\t\t\t<span style=\"margin-left:55px\">Non-motile: {{report?.motility_non_motile==''?'..............':report.motility_non_motile }}</span><br>\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p><strong>COUNT: </strong>{{report?.count==''?'.........':report.count}} X 10 <sup>6</sup> sperm/ml</p>\n\t\t\t\t\t\t\t\t\t<table class=\"hpf\">\n\t\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t<th>Particulate Debris(HPF)</th>\n\t\t\t\t\t\t\t\t\t\t\t\t<th>Morphology(%)</th>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>Pus Cells</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.hpf_pus_cell}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.morph_pus_cell}}</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>RBC</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.hpf_rbc}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.morph_rbc}}</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>Epithelial Cells</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.hpf_epithelial_cell}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.morph_epithelial_cell}}</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t\t\t<h6>Stool Microscopy</h6>\n\t\t\t\t\t\t\t\t\t<p><strong>Macroscopy: </strong>{{report?.stool_macroscopy==''?'..................':report.stool_macroscopy}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Wet Prepartion: </strong>{{report?.wet_preparation==''?'..................':report.wet_preparation}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Concentration: </strong>{{report?.concentration==''?'..................':report.concentration}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Malaria Parasite: </strong>{{report?.malaria_parasite==''?'..................':report.malaria_parasite}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Microfilaria Skin: </strong>{{report?.microfilaria_skin==''?'..................':report.microfilaria_skin}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Microfilaria Blood: </strong>{{report?.microfilaria_blood==''?'..................':report.microfilaria_blood}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Stool Occult Blood </strong>{{report?.stool_occult_blood==''?'..................':report.stool_occult_blood}}</p>\n\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t<td colspan=\"2\">\n\t\t\t\t\t\t\t\t\t<p><strong>VDRL Test: </strong>{{report?.vdrl_test==''?'..................................':report.vdrl_test}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>ASO Titre: </strong>{{report?.aso_titre==''?'..................................':report.aso_titre}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Chlamydia Serology: </strong>{{report?.chlamydia_serology==''?'..................................':report.chlamydia_serology}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Hep B, Ag: </strong>{{report?.hepbag==''?'..................................':report.hepbag}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>HCV Ab: </strong>{{report?.hcv_ab==''?'..................................':report.hcv_ab}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Helicobacter Pylori Ab: </strong>{{report?.helocobacter==''?'..................................':report.helicobacter}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Retroviral Screening Test: </strong>{{report?.retroviral_screening==''?'..................................':report.retroviral_screening}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Retroviral Confirmation: </strong>{{report?.retroviral_confirmation==''?'..................................':report.retroviral_confirmation}}</p>\n\t\t\t\t\t\t\t\t\t<p><strong>CD<sub>4</sub> Count: </strong>{{report?.cd4_count==''?'..................................':report.cd4_count}}</p>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<h6>MANTOUX TEST</h6>\n\t\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\t\t<strong>Given: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.given==''?'...............':report.given}}\n\t\t\t\t\t\t\t\t\t\t<strong>Read: </strong>\n\t\t\t\t\t\t\t\t\t\t{{report?.read==''?'..............':report.read}}\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p><strong>Result: </strong>{{report?.result==''?'.................................................':report.result}}</p>\n\t\t\t\t\t\t\t\t\t<h6>SEROLOGICAL TEST</h6>\n\t\t\t\t\t\t\t\t\t<h6>Widal Test</h6>\n\t\t\t\t\t\t\t\t\t<table class=\"widal-tbl\">\n\t\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Antigen</th>\n\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"2\">Antibody</th>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<th>\"O\"</th>\n\t\t\t\t\t\t\t\t\t\t\t\t<th>\"H\"</th>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>S. Typhi</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_typhi_o}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_typhi_h}}\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>S. Paratyphi A</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_paratyphi_a_o}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_paratyphi_a_h}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>S. Paratyphi B</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_paratyphi_b_o}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_paratyphi_b_h}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>S. Paratyphi C</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_paratyphi_c_o}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>{{report?.s_paratyphi_c_h}}</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t<p><strong>Comment: </strong>{{report?.comment==''?'....................................':report.comment}}</p>\n\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t<td colspan=\"7\" class=\"p-0\">\n\t\t\t\t\t\t\t\t\t\t<h6 class=\"py-0 my-0\">Culture</h6>\n\t\t\t\t\t\t\t\t\t\t<p><strong>Organism(s) isolated: </strong>{{report?.organism_isolated==''?'.........':report.organism_isolated}}</p>\n\t\t\t\t\t\t\t\t\t\t<table class=\"culture\">\n\t\t\t\t\t\t\t\t\t\t\t<thead>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th ></th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"4\">Sensitivity</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"\"></th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"4\">Sensitivity</th>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t</thead>\n\t\t\t\t\t\t\t\t\t\t\t<tbody>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Antimicrobial Agents</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>+++</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>++</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>+</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>R</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Antimicrobial Agents</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>+++</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>++</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>+</th>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<th>R</th>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tAmoxycillin/Clavulante\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycillin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycillin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycillin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycillin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tCiprofloxacin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ciprofloxin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ciprofloxin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ciprofloxin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ciprofloxin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tCefuroxime\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cefuroxime =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cefuroxime =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cefuroxime =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cefuroxime =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tChloramphenicol\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.chloramphenicol =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.chloramphenicol =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.chloramphenicol =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.chloramphenicol =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tCeftriaxone\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ceftriaxone =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ceftriaxone =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ceftriaxone =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ceftriaxone =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tErythromycin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.erythromycin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.erythromycin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.erythromycin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.erythromycin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\t\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tCotrimoxazole\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cotrimoxazole =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cotrimoxazole =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cotrimoxazole =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.cotrimoxazole =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tGentamycin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.gentymacin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.gentymacin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.gentymacin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.gentymacin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tNorfloxacin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.norfloxacin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.norfloxacin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.norfloxacin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.norfloxacin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tOfloxacin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ofloxacin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ofloxacin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ofloxacin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ofloxacin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tTebacylin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.tebacylin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.tebacylin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.tebacylin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.tebacylin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tNitrofurantoin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nitrofuratoin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nitrofuratoin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nitrofuratoin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nitrofuratoin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tNalidixic Acid\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nalidixic_acid =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nalidixic_acid =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nalidixic_acid =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.nalidixic_acid =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tAmpiclox\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ampiclox =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ampiclox =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ampiclox =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.ampiclox =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tStreptomycin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.streptomacycin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.streptomacycin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.streptomacycin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.streptomacycin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tAmoxycilin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycilin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycilin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycilin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.amoxycilin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tSeptrin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.septrin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.septrin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.septrin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.septrin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td></td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tClidamycin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.clidamycin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.clidamycin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.clidamycin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.clidamycin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tLevofloxcin\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.levofloxcin =='ppp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.levofloxcin =='pp'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.levofloxcin =='p'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<td>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"report?.levofloxcin =='r'\">check</i>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t\t\t\t\t</table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t</tbody>\n\t\t\t\t\t\t</table>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t </app-printed-page>"

/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology-view/medical-microbiology-view.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology-view/medical-microbiology-view.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: MedicalMicrobiologyViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalMicrobiologyViewComponent", function() { return MedicalMicrobiologyViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _medical_microbiology_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../medical-microbiology.service */ "./src/app/medical-microbiology/medical-microbiology.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MedicalMicrobiologyViewComponent = /** @class */ (function () {
    function MedicalMicrobiologyViewComponent(route, serviceProvider) {
        this.route = route;
        this.serviceProvider = serviceProvider;
        this.loadingReport = false;
        this.conError = false;
        this.serverError = false;
        this.id = null;
        this.getReport = this.getReport.bind(this);
    }
    MedicalMicrobiologyViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (param) {
            _this.id = param.id;
            _this.getReport();
        });
    };
    MedicalMicrobiologyViewComponent.prototype.getReport = function () {
        var _this = this;
        this.loadingReport = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getReport(this.id).subscribe(function (report) {
            _this.loadingReport = false;
            var keys = Object.keys(report);
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                if (report[key] === null || report[key] == 'null') {
                    report[key] = '';
                }
                else if (report[key] === '') {
                    report[key] = '';
                }
            }
            _this.report = report;
        }, function (err) {
            _this.loadingReport = false;
            if (err.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    MedicalMicrobiologyViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-medical-microbiology-view',
            template: __webpack_require__(/*! ./medical-microbiology-view.component.html */ "./src/app/medical-microbiology/medical-microbiology-view/medical-microbiology-view.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _medical_microbiology_service__WEBPACK_IMPORTED_MODULE_1__["MedicalMicrobiologyService"]])
    ], MedicalMicrobiologyViewComponent);
    return MedicalMicrobiologyViewComponent;
}());



/***/ }),

/***/ "./src/app/medical-microbiology/medical-microbiology.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/medical-microbiology/medical-microbiology.service.ts ***!
  \**********************************************************************/
/*! exports provided: MedicalMicrobiologyService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MedicalMicrobiologyService", function() { return MedicalMicrobiologyService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var MedicalMicrobiologyService = /** @class */ (function () {
    function MedicalMicrobiologyService(apiUrl, http) {
        this.apiUrl = apiUrl;
        this.http = http;
    }
    MedicalMicrobiologyService.prototype.createReport = function (report, options) {
        var formData = new FormData();
        var fields = Object.keys(report);
        for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++) {
            var field = fields_1[_i];
            if (field == 'id')
                continue;
            formData.append(field, report[field]);
        }
        return this.http.post(this.apiUrl + 'medical-microbiology', formData, { params: options });
    };
    MedicalMicrobiologyService.prototype.getReports = function () {
        return this.http.get(this.apiUrl + 'medical-microbiology');
    };
    MedicalMicrobiologyService.prototype.deleteReport = function (id) {
        var formData = new FormData();
        formData.append('_method', "DELETE");
        return this.http.post(this.apiUrl + 'medical-microbiology/' + id, formData);
    };
    MedicalMicrobiologyService.prototype.getReport = function (id) {
        return this.http.get(this.apiUrl + 'medical-microbiology/' + id);
    };
    MedicalMicrobiologyService.prototype.updateReport = function (report) {
        var formData = new FormData();
        formData.append('_method', 'PATCH');
        var fields = Object.keys(report);
        for (var _i = 0, fields_2 = fields; _i < fields_2.length; _i++) {
            var field = fields_2[_i];
            if (field == 'id')
                continue;
            formData.append(field, report[field]);
        }
        return this.http.post(this.apiUrl + 'medical-microbiology/' + report.id, formData);
    };
    MedicalMicrobiologyService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], MedicalMicrobiologyService);
    return MedicalMicrobiologyService;
}());



/***/ }),

/***/ "./src/app/patient/abstract-patient-component.ts":
/*!*******************************************************!*\
  !*** ./src/app/patient/abstract-patient-component.ts ***!
  \*******************************************************/
/*! exports provided: AbstractPatientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractPatientComponent", function() { return AbstractPatientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _patient_form_patient_form_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./patient-form/patient-form.component */ "./src/app/patient/patient-form/patient-form.component.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AbstractPatientComponent = /** @class */ (function () {
    function AbstractPatientComponent() {
        this.loading = false;
        this.conError = false;
        this.serverError = false;
        this.M = M;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_patient_form_patient_form_component__WEBPACK_IMPORTED_MODULE_1__["PatientFormComponent"]),
        __metadata("design:type", _patient_form_patient_form_component__WEBPACK_IMPORTED_MODULE_1__["PatientFormComponent"])
    ], AbstractPatientComponent.prototype, "patientForm", void 0);
    return AbstractPatientComponent;
}());



/***/ }),

/***/ "./src/app/patient/create-patient/create-patient.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/patient/create-patient/create-patient.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Add New Patient\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/patient/new']\">Add new patient</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t<app-linear-loading  [showLoading]=\"loading\"></app-linear-loading>\n\t<app-patient-form (submit)=\"handleSubmit($event)\" [disableBtn] =\"loading\">\n\t</app-patient-form>\n</div>"

/***/ }),

/***/ "./src/app/patient/create-patient/create-patient.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/patient/create-patient/create-patient.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/patient/create-patient/create-patient.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/patient/create-patient/create-patient.component.ts ***!
  \********************************************************************/
/*! exports provided: CreatePatientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePatientComponent", function() { return CreatePatientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _patient_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../patient.service */ "./src/app/patient/patient.service.ts");
/* harmony import */ var _abstract_patient_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../abstract-patient-component */ "./src/app/patient/abstract-patient-component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreatePatientComponent = /** @class */ (function (_super) {
    __extends(CreatePatientComponent, _super);
    function CreatePatientComponent(serviceProvider, route, router) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.route = route;
        _this.router = router;
        _this.route.queryParamMap.subscribe(function (paramMap) {
            var param = paramMap;
            if (param.params.r) {
                var next = param.params.r;
                if (/^(\/.+)+\/.+/ig.test(next)) {
                    _this.next = next;
                }
            }
        });
        _this = _super.call(this) || this;
        return _this;
    }
    CreatePatientComponent.prototype.ngOnInit = function () {
    };
    CreatePatientComponent.prototype.handleSubmit = function (patientData) {
        var _this = this;
        this.loading = true;
        this.serviceProvider.createPatient(patientData).subscribe(function (data) {
            _this.loading = false;
            _this.patientForm.resetForm();
            _this.toast.success("Patient data was saved successfully");
            if (_this.next) {
                _this.router.navigateByUrl(_this.next);
            }
        }, function (error) {
            _this.loading = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to the server please try again");
            }
            else {
                _this.toast.error("An error occured patient data could not be saved, please try again");
            }
        });
    };
    CreatePatientComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-create-patient',
            template: __webpack_require__(/*! ./create-patient.component.html */ "./src/app/patient/create-patient/create-patient.component.html"),
            styles: [__webpack_require__(/*! ./create-patient.component.scss */ "./src/app/patient/create-patient/create-patient.component.scss")]
        }),
        __metadata("design:paramtypes", [_patient_service__WEBPACK_IMPORTED_MODULE_1__["PatientService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], CreatePatientComponent);
    return CreatePatientComponent;
}(_abstract_patient_component__WEBPACK_IMPORTED_MODULE_2__["AbstractPatientComponent"]));



/***/ }),

/***/ "./src/app/patient/edit-patient/edit-patient.component.html":
/*!******************************************************************!*\
  !*** ./src/app/patient/edit-patient/edit-patient.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Update Patient record\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/patients']\" >View Patients</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/patient/edit/'+id]\">Update patient record</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\n\t<connection-error [show]=\"conError\" [retryCb]=\"getPatient\"></connection-error>\n\t<server-error [show]=\"serverError\" [retryCb]=\"getPatient\"></server-error>\n\t<app-loading [showLoading]=\"loadingPatient\" loadingInfo=\"Loading patients records please wait\" ></app-loading>\n\n\t<app-linear-loading  [showLoading]=\"loading\"></app-linear-loading>\n\t<app-patient-form (submit)=\"handleSubmit($event)\" \n\t\t\t\t\t\t[disableBtn] =\"loading\" \n\t\t\t\t\t\t[patient]=\"patient\"\n\t\t\t\t\t\t*ngIf=\"!loadingPatient && !serverError && !conError\"\n\t\t\t\t\t\t[saveBtnText]=\"saveBtnText\">\n\t</app-patient-form>\n</div>"

/***/ }),

/***/ "./src/app/patient/edit-patient/edit-patient.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/patient/edit-patient/edit-patient.component.ts ***!
  \****************************************************************/
/*! exports provided: EditPatientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPatientComponent", function() { return EditPatientComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _patient_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../patient.service */ "./src/app/patient/patient.service.ts");
/* harmony import */ var _abstract_patient_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../abstract-patient-component */ "./src/app/patient/abstract-patient-component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EditPatientComponent = /** @class */ (function (_super) {
    __extends(EditPatientComponent, _super);
    function EditPatientComponent(route, serviceProvider) {
        var _this = _super.call(this) || this;
        _this.route = route;
        _this.serviceProvider = serviceProvider;
        _this.conError = false;
        _this.serverError = false;
        _this.loadingPatient = false;
        _this.saveBtnText = "Update";
        return _this;
    }
    EditPatientComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params.id;
            _this.getPatient();
        });
    };
    EditPatientComponent.prototype.getPatient = function () {
        var _this = this;
        this.serverError = false;
        this.loadingPatient = true;
        this.conError = false;
        this.serviceProvider.getPatient(this.id).subscribe(function (patient) {
            _this.loadingPatient = false;
            _this.patient = patient;
        }, function (err) {
            _this.loadingPatient = false;
            if (err.status == 0)
                _this.conError = true;
            else
                _this.serverError = true;
        });
    };
    EditPatientComponent.prototype.handleSubmit = function (patientData) {
        var _this = this;
        this.loading = true;
        this.patient = patientData;
        patientData.id = this.id;
        this.serviceProvider.updatePatient(patientData).subscribe(function (data) {
            _this.loading = false;
            // this.patientForm.resetForm();
            _this.toast.success("Patient data was saved successfully");
        }, function (error) {
            _this.loading = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to the server please try again");
            }
            else {
                _this.toast.error("An error occured patient data could not be saved, please try again");
            }
        });
    };
    EditPatientComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-patient',
            template: __webpack_require__(/*! ./edit-patient.component.html */ "./src/app/patient/edit-patient/edit-patient.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _patient_service__WEBPACK_IMPORTED_MODULE_2__["PatientService"]])
    ], EditPatientComponent);
    return EditPatientComponent;
}(_abstract_patient_component__WEBPACK_IMPORTED_MODULE_3__["AbstractPatientComponent"]));



/***/ }),

/***/ "./src/app/patient/patient-form/patient-form.component.html":
/*!******************************************************************!*\
  !*** ./src/app/patient/patient-form/patient-form.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <div class=\"card-panel near-loading\">\n    <div class=\"card-title\">\n      <h5>Basic Information</h5>\n    </div>\n    <div class=\"card-content\">\n      <form [formGroup]=\"patientForm\" >\n        <div class=\"row\">\n          <div class=\"col m4\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"surname\" formControlName=\"surname\" required>\n              <label for=\"surname\"  >Surname</label>\n            </div>\n          </div>\n          <div class=\"col m4\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"first-name\" formControlName=\"first_name\" required>\n              <label for=\"first-name\" >First name</label>\n            </div>\n          </div>\n          <div class=\"col m4\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"middle-name\" formControlName=\"middle_name\">\n              <label for=\"middle-name\">Middle name</label>\n            </div>\n          </div>\n          <div class=\"col m4\">\n            <div class=\"input-field\">\n              <input type=\"number\" id=\"age\" formControlName=\"age\" required>\n              <label for=\"age\">Age</label>\n            </div>\n          </div>\n          <div class=\"col m4\">\n            <p>Sex</p>\n            <p><label>\n              <input name=\"sex\" type=\"radio\" class=\"with-gap\"  value=\"Male\" formControlName=\"sex\" required/>\n              <span>Male</span>\n            </label>\n              <label>\n                <input name=\"sex\" type=\"radio\" class=\"with-gap\"  value=\"Female\" formControlName=\"sex\" required/>\n                <span>Female</span>\n              </label>\n            </p>\n          </div>\n          \n          <!-- <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"requested-by\" formControlName=\"requested_by\">\n              <label for=\"requested-by\">Requested By</label>\n            </div>\n          </div> -->\n          <div class=\"col m4\">\n\t          <div class=\"input-field\">\n\t\t          <input type=\"text\" id=\"email\" formControlName=\"email\">\n\t\t          <label for=\"email\">Email</label>\n\t          </div>\n          </div>\n        </div>\n        <div class=\"row\">\n          <div class=\"col m4\">\n\t          <div class=\"input-field\">\n\t\t          <input type=\"text\" id=\"phone_no\" formControlName=\"phone_no\" required>\n\t\t          <label for=\"phone_no\">Phone number</label>\n\t          </div>\n          </div>\n          <!-- <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"clinical_detail\" formControlName=\"clinical_detail\">\n              <label for=\"clinical_detail\">Clinical details</label>\n            </div>\n          </div> -->\n        </div>\n        <div class=\"row\">\n          \n          <!-- <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"exam-required\" formControlName=\"exam_required\">\n              <label for=\"exam-required\">Exam required</label>\n            </div>\n          </div> -->\n         <!--  <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"specimen\" formControlName=\"specimen\">\n              <label for=\"specimen\">Specimen</label>\n            </div>\n          </div> -->\n         <!--  <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"clinic\" formControlName=\"clinic\">\n              <label for=\"clinic\">Clinic</label>\n            </div>\n          </div> -->\n          <div class=\"col m12\">\n          \t\t<button type=\"button\" (click)=\"onSubmit()\" class=\"btn btn-large\" [disabled]=\"!patientForm.valid || disableBtn \">{{saveBtnText}}</button>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./src/app/patient/patient-form/patient-form.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/patient/patient-form/patient-form.component.ts ***!
  \****************************************************************/
/*! exports provided: PatientFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientFormComponent", function() { return PatientFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PatientFormComponent = /** @class */ (function () {
    function PatientFormComponent() {
        this.patientForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            surname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            first_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            middle_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            age: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            sex: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            requested_by: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            phone_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            exam_required: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            specimen: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            clinic: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            clinical_detail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.submit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.disableBtn = false;
        this.saveBtnText = "Save";
    }
    PatientFormComponent.prototype.ngOnInit = function () {
    };
    PatientFormComponent.prototype.onSubmit = function () {
        this.submit.emit(this.patientForm.value);
    };
    PatientFormComponent.prototype.resetForm = function () {
        this.patientForm.reset();
    };
    PatientFormComponent.prototype.ngOnChanges = function () {
        this.patientForm.patchValue(__assign({}, this.patient));
    };
    PatientFormComponent.prototype.ngAfterViewChecked = function () {
        M.updateTextFields();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PatientFormComponent.prototype, "submit", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PatientFormComponent.prototype, "patient", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], PatientFormComponent.prototype, "disableBtn", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PatientFormComponent.prototype, "saveBtnText", void 0);
    PatientFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patient-form',
            template: __webpack_require__(/*! ./patient-form.component.html */ "./src/app/patient/patient-form/patient-form.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], PatientFormComponent);
    return PatientFormComponent;
}());



/***/ }),

/***/ "./src/app/patient/patient-list/patient-list.component.html":
/*!******************************************************************!*\
  !*** ./src/app/patient/patient-list/patient-list.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"All Patients\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/patients']\">All Patients</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t\n\t<connection-error [show]=\"conError\" [retryCb]=\"getPatients\"></connection-error>\n\t<server-error [show]=\"serverError\" [retryCb]=\"getPatients\"></server-error>\n\t<app-loading [showLoading]=\"loading\" loadingInfo=\"Loading patients records please wait\" ></app-loading>\n\t<no-record [show]='matDatasource.data.length==0 && !loading && !conError && !serverError'>There currently no patient record</no-record>\n\t<app-linear-loading  [showLoading]=\"deleting\"></app-linear-loading>\n\t<div class=\"card-panel near-loading\" *ngIf=\"!loading && !conError && !serverError && matDatasource.data.length!==0\">\n\t\t<div class=\"row\">\n                <div class=\"col m4 offset-m8\">\n                    <div class=\"input-field\">\n                        <input type=\"text\" id=\"filter\" (keyup)=\"filterTable($event.target.value)\">\n                        <label for=\"filter\">Filter</label>\n                    </div>\n                </div>\n            </div>\n\n\t\t\t<table mat-table [dataSource]=\"matDatasource\" matSort *ngIf=\"matDatasource.data.length!==0\">\n\t\t\t\t <ng-container matColumnDef=\"id\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> ID </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> {{patient.id}} </td>\n\t\t         </ng-container>\n\t\t         <ng-container matColumnDef=\"firstName\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> First Name </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> {{patient.first_name }} </td>\n\t\t         </ng-container>\n\t\t         <ng-container matColumnDef=\"middleName\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> Middle Name </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> {{patient.middle_name}} </td>\n\t\t         </ng-container>\n\t\t         <ng-container matColumnDef=\"surname\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> Surname </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> {{patient.surname}} </td>\n\t\t         </ng-container>\n\t\t         <ng-container matColumnDef=\"age\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> Age </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> {{patient.age}} </td>\n\t\t         </ng-container>\n\n\t\t         <ng-container matColumnDef=\"phone_no\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> Phone No. </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> {{patient.phone_no}} </td>\n\t\t         </ng-container>\n\n\t\t         <ng-container matColumnDef=\"email\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> Email </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> {{patient.email}} </td>\n\t\t         </ng-container>\n\n\n\t\t         <ng-container matColumnDef=\"viewTests\">\n\t\t            <th mat-header-cell *matHeaderCellDef> View Logs </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> <a [routerLink]=\"['/patient/log/'+patient.id]\" class=\"\">View Log</a> </td>\n\t\t           \n\t\t         </ng-container>\n\t\t         <ng-container matColumnDef=\"edit\">\n\t\t            <th mat-header-cell *matHeaderCellDef> Edit </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> <a [routerLink]=\"['/patient/edit/'+patient.id]\" class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a> </td>\n\t\t         </ng-container>\n\n\t\t         <ng-container matColumnDef=\"delete\">\n\t\t            <th mat-header-cell *matHeaderCellDef> Delete </th>\n\t\t            <td mat-cell *matCellDef=\"let patient\"> <button (click)=\"deletePatient(patient.id,i)\" [disabled]=\"deleting\" class=\"btn red accent-3\"><i class=\"material-icons\">delete</i></button> </td>\n\t\t         </ng-container>\n \n\t\t          <tr mat-header-row *matHeaderRowDef=\"columnsToDisplay\"></tr>\n\t\t          <tr mat-row *matRowDef=\"let row; columns: columnsToDisplay;\"></tr>\n\t        \n\t\t\t</table>\n\t\t\t<mat-paginator [pageSize]=\"10\" \n                            [pageSizeOptions]=\"[5, 10, 20]\" \n                            showFirstLastButtons\n                            *ngIf=\"matDatasource.data.length!==0\"\n                            >\n                           \n        \t </mat-paginator>\n\t</div>\n\n\t\n\t\n</div>"

/***/ }),

/***/ "./src/app/patient/patient-list/patient-list.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/patient/patient-list/patient-list.component.ts ***!
  \****************************************************************/
/*! exports provided: PatientListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientListComponent", function() { return PatientListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _patient_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../patient.service */ "./src/app/patient/patient.service.ts");
/* harmony import */ var _shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/abstract-table-component */ "./src/app/shared/abstract-table-component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PatientListComponent = /** @class */ (function (_super) {
    __extends(PatientListComponent, _super);
    function PatientListComponent(serviceProvider) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.loading = false;
        _this.serverError = false;
        _this.conError = false;
        _this.deleting = false;
        _this.matDatasource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"]();
        _this.columnsToDisplay = ['id', 'firstName', 'middleName', 'surname', 'age', 'phone_no', 'email', 'viewTests', 'edit', 'delete'];
        _this.getPatients = _this.getPatients.bind(_this);
        return _this;
    }
    PatientListComponent.prototype.ngOnInit = function () {
        this.getPatients();
    };
    PatientListComponent.prototype.getPatients = function () {
        var _this = this;
        this.loading = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getPatients().subscribe(function (data) {
            _this.patients = data;
            _this.matDatasource.data = _this.patients;
            _this.loading = false;
        }, function (error) {
            _this.loading = false;
            if (error.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    PatientListComponent.prototype.deletePatient = function (id, index) {
        var _this = this;
        var options = {
            cancel: true,
            cancelText: 'Cancel',
            cancelCallBack: function (event) { },
            confirm: true,
            confirmText: 'Delete',
            confirmCallBack: function (event) {
                _this.deleting = true;
                _this.serviceProvider.deletePatient(id).subscribe(function (res) {
                    _this.toast.success('Patient record was deleted succesfully');
                    _this.deleting = false;
                    _this.patients.splice(index, 1);
                    _this.matDatasource.data = _this.patients;
                }, function (err) {
                    _this.deleting = false;
                    _this.toast.error("Patient record could not be deleted at the moment please try again");
                });
            }
        };
        this.roar('Delete Patient record', 'Do you realy want to  <strong>Delete </strong> this record', options);
    };
    PatientListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patient-list',
            template: __webpack_require__(/*! ./patient-list.component.html */ "./src/app/patient/patient-list/patient-list.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [_patient_service__WEBPACK_IMPORTED_MODULE_2__["PatientService"]])
    ], PatientListComponent);
    return PatientListComponent;
}(_shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__["AbstractTableComponent"]));



/***/ }),

/***/ "./src/app/patient/patient-log/patient-log.component.html":
/*!****************************************************************!*\
  !*** ./src/app/patient/patient-log/patient-log.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"All Patients\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\" >Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/patients']\">All Patients</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t\n\t<connection-error [show]=\"conError\" [retryCb]=\"getPatientLog\"></connection-error>\n\t<server-error [show]=\"serverError\" [retryCb]=\"getPatientLog\"></server-error>\n\t<app-loading [showLoading]=\"loading\" loadingInfo=\"Loading patient log please wait\" ></app-loading>\n\t<no-record [show]='matDatasource.data.length==0 && !loading && !conError && !serverError'>Pateint currently have no logs</no-record>\n\t\n\t<div class=\"card-panel near-loading\" *ngIf=\"!loading && !conError && !serverError && matDatasource.data.length!==0\">\n\t\t<div class=\"row\">\n                <div class=\"col m4 offset-m8\">\n                    <div class=\"input-field\">\n                        <input type=\"text\" id=\"filter\" (keyup)=\"filterTable($event.target.value)\">\n                        <label for=\"filter\">Filter</label>\n                    </div>\n                </div>\n            </div>\n\n\t\t\t<table mat-table [dataSource]=\"matDatasource\" matSort *ngIf=\"matDatasource.data.length!==0\">\n\t\t\t\t <ng-container matColumnDef=\"type\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> Type </th>\n\t\t            <td mat-cell *matCellDef=\"let log\"> {{log.type}} </td>\n\t\t         </ng-container>\n\t\t         <ng-container matColumnDef=\"date\">\n\t\t            <th mat-header-cell *matHeaderCellDef mat-sort-header> Date </th>\n\t\t            <td mat-cell *matCellDef=\"let log\"> {{log.created_at | date:mediumDate  }} </td>\n\t\t         </ng-container>\n\t\t       \n\t\t         <ng-container matColumnDef=\"view\">\n\t\t            <th mat-header-cell *matHeaderCellDef> View</th>\n\n\t\t            <td mat-cell *matCellDef=\"let log\"> <a [routerLink]=\"getLink(log)\" class=\"btn blue accent-3\">View Details</a> </td>\n\t\t           \n\t\t         </ng-container>\n\t\t        \n \n\t\t          <tr mat-header-row *matHeaderRowDef=\"columnsToDisplay\"></tr>\n\t\t          <tr mat-row *matRowDef=\"let row; columns: columnsToDisplay;\"></tr>\n\t        \n\t\t\t</table>\n\t\t\t<mat-paginator [pageSize]=\"10\" \n                            [pageSizeOptions]=\"[5, 10, 20]\" \n                            showFirstLastButtons\n                            *ngIf=\"matDatasource.data.length!==0\"\n                            >\n                           \n        \t </mat-paginator>"

/***/ }),

/***/ "./src/app/patient/patient-log/patient-log.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/patient/patient-log/patient-log.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/patient/patient-log/patient-log.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/patient/patient-log/patient-log.component.ts ***!
  \**************************************************************/
/*! exports provided: PatientLogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientLogComponent", function() { return PatientLogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _patient_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../patient.service */ "./src/app/patient/patient.service.ts");
/* harmony import */ var _shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/abstract-table-component */ "./src/app/shared/abstract-table-component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PatientLogComponent = /** @class */ (function (_super) {
    __extends(PatientLogComponent, _super);
    function PatientLogComponent(serviceProvider, route) {
        var _this = _super.call(this) || this;
        _this.serviceProvider = serviceProvider;
        _this.route = route;
        _this.loading = false;
        _this.serverError = false;
        _this.conError = false;
        _this.deleting = false;
        _this.id = null;
        _this.columnsToDisplay = ['type', 'date', 'view'];
        _this.matDatasource = new _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableDataSource"]();
        _this.logs = [];
        _this.getPatientLog = _this.getPatientLog.bind(_this);
        return _this;
    }
    PatientLogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (param) {
            _this.id = param.id;
            _this.getPatientLog();
        });
    };
    PatientLogComponent.prototype.getPatientLog = function () {
        var _this = this;
        this.loading = true;
        this.conError = false;
        this.serverError = false;
        this.serviceProvider.getPatientLog(this.id).subscribe(function (data) {
            _this.logs = data;
            _this.matDatasource.data = _this.logs;
            _this.loading = false;
        }, function (error) {
            _this.loading = false;
            if (error.status == 0) {
                _this.conError = true;
            }
            else {
                _this.serverError = true;
            }
        });
    };
    PatientLogComponent.prototype.getLink = function (log) {
        switch (log.type) {
            case "Medical Microbiology":
                return '/medical-microbiology-and-parasitology/reports/' + log.id;
            case "Haematology":
                return '/haematology/reports/' + log.id;
            case "Chemical pathology":
                return '/chemical-pathology/reports/' + log.id;
        }
    };
    PatientLogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patient-log',
            template: __webpack_require__(/*! ./patient-log.component.html */ "./src/app/patient/patient-log/patient-log.component.html"),
            styles: [__webpack_require__(/*! ./patient-log.component.scss */ "./src/app/patient/patient-log/patient-log.component.scss")]
        }),
        __metadata("design:paramtypes", [_patient_service__WEBPACK_IMPORTED_MODULE_2__["PatientService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], PatientLogComponent);
    return PatientLogComponent;
}(_shared_abstract_table_component__WEBPACK_IMPORTED_MODULE_3__["AbstractTableComponent"]));



/***/ }),

/***/ "./src/app/patient/patient.service.ts":
/*!********************************************!*\
  !*** ./src/app/patient/patient.service.ts ***!
  \********************************************/
/*! exports provided: PatientService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientService", function() { return PatientService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var PatientService = /** @class */ (function () {
    function PatientService(apiUrl, http) {
        this.apiUrl = apiUrl;
        this.http = http;
    }
    PatientService.prototype.createPatient = function (patientData) {
        var formData = new FormData();
        var fields = Object.keys(patientData);
        for (var _i = 0, fields_1 = fields; _i < fields_1.length; _i++) {
            var field = fields_1[_i];
            if (field == 'id')
                continue;
            formData.append(field, patientData[field]);
        }
        return this.http.post(this.apiUrl + 'patient', formData);
    };
    PatientService.prototype.updatePatient = function (patientData) {
        var formData = new FormData();
        formData.append('_method', 'PATCH');
        var fields = Object.keys(patientData);
        for (var _i = 0, fields_2 = fields; _i < fields_2.length; _i++) {
            var field = fields_2[_i];
            if (field == 'id')
                continue;
            formData.append(field, patientData[field]);
        }
        return this.http.post(this.apiUrl + 'patient/' + patientData.id, formData);
    };
    PatientService.prototype.getPatients = function () {
        return this.http.get(this.apiUrl + 'patient');
    };
    PatientService.prototype.getPatient = function (id) {
        return this.http.get(this.apiUrl + 'patient/' + id);
    };
    PatientService.prototype.getPatientLog = function (id) {
        return this.http.get(this.apiUrl + 'patient/logs/' + id);
    };
    PatientService.prototype.deletePatient = function (id) {
        var formData = new FormData();
        formData.append('_method', "DELETE");
        return this.http.post(this.apiUrl + 'patient/' + id, formData);
    };
    PatientService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PatientService);
    return PatientService;
}());



/***/ }),

/***/ "./src/app/printed-page/printed-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/printed-page/printed-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"print-page\">\n  <div class=\"  fadeInUp\">\n  <div class=\"print-header print\">\n    <div class=\"wrapper\">\n      <div class=\"print-logo center-align\">\n        <img [src]=\"logo\" width=\"150px\">\n      </div>\n      <div class=\"print-brand\">\n        <h2>{{labName}}</h2>\n        <h5>{{subName}}</h5>\n      </div>\n      <div class=\"services\">\n        <ul>\n           <li>ECG</li>\n          <li>Digital Mammography</li>\n          <li>Special X-ray investigation</li>\n          <li>USS</li>\n          <li>Automated</li>\n          <li>Pap Smear</li>\n        </ul>\n      </div> \n    </div>\n  </div>\n  <ng-content></ng-content>\n  <div class=\"print-footer print\">\n    <div class=\"signatry\">\n      <div class=\"row\">\n        <div class=\"col m5\">\n          <p>MEDICAL LAB SCIENTIST ...........................................</p>\n        </div>\n        <div class=\"col m2\">\n        </div>\n        <div class=\"col m5\">\n          <p style=\"margin-left:100px;\">DATE...............................................</p>\n        </div>\n      </div>\n    </div>\n    <hr>\n    <div class=\"row \">\n      <div class=\"col m10\">\n        <div class=\"center-align print-footer\">\n          <p class=\"center-align\"><strong>Contact Us: </strong> Tel: {{phoneNo}}, Email:  {{email}} </p>\n          <p>Address: {{address}}</p>\n          <!-- <p>Plot 8 G/Line, Evet Housing Estate, (Behind Ultrafit/Along Kings & Queen Road) Uyo, Akwa Ibom State, Nigeria</p>\n          <p>Tel +234-85-290368, +234-8080627497, +234-8135115577, Email: streamtrustdianostic@yahoo.com </p>\n          <p>COLLECTION CENTRE: BENEAT CLENO By University of Uyo Teaching Hospital Gate, Abak Road, Uyo.</p>\n        --> </div>\n      </div>\n    </div>\n  </div>\n</div>\n  <div class=\"fixed-action-btn\">\n    <button class=\"btn-floating btn-large red\" (click)=\"printPage()\">\n    <i class=\"large material-icons\">print</i>\n    </button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/printed-page/printed-page.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/printed-page/printed-page.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".print-header {\n  border-bottom: solid 1px;\n  margin-bottom: 20px;\n  display: flex;\n  justify-content: space-between; }\n  .print-header .wrapper {\n    display: flex; }\n  .print-header .print-brand {\n    width: 70%;\n    text-align: center; }\n  .print-header .print-brand h2, .print-header .print-brand h5 {\n      margin: 0;\n      padding: 0 !important;\n      font-family: sans-serif; }\n  .print-footer p {\n  font-size: .9em;\n  line-height: 0; }\n  .services ul {\n  list-style: square !important; }\n  .services li {\n  font-size: .85em;\n  line-height: 15px; }\n"

/***/ }),

/***/ "./src/app/printed-page/printed-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/printed-page/printed-page.component.ts ***!
  \********************************************************/
/*! exports provided: PrintedPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintedPageComponent", function() { return PrintedPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _settings_setting_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../settings/setting.service */ "./src/app/settings/setting.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PrintedPageComponent = /** @class */ (function () {
    function PrintedPageComponent(setting) {
        this.setting = setting;
        this.labName = '';
        this.subName = '';
        this.logo = '';
        this.email = '';
        this.address = '';
        this.phoneNo = '';
        this.labName = this.setting.getLocalSettings().lab_name;
        this.subName = this.setting.getLocalSettings().sub_name;
        this.logo = this.setting.getLocalSettings().logo_file;
        this.phoneNo = this.setting.getLocalSettings().phone_no;
        this.address = this.setting.getLocalSettings().address;
        this.email = this.setting.getLocalSettings().email;
    }
    PrintedPageComponent.prototype.ngOnInit = function () {
    };
    PrintedPageComponent.prototype.printPage = function () {
        window.print();
    };
    PrintedPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-printed-page',
            template: __webpack_require__(/*! ./printed-page.component.html */ "./src/app/printed-page/printed-page.component.html"),
            styles: [__webpack_require__(/*! ./printed-page.component.scss */ "./src/app/printed-page/printed-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_settings_setting_service__WEBPACK_IMPORTED_MODULE_1__["SettingService"]])
    ], PrintedPageComponent);
    return PrintedPageComponent;
}());



/***/ }),

/***/ "./src/app/radiological-request-edit/radiological-request-edit.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/radiological-request-edit/radiological-request-edit.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Radiological Request update\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/radiological-request/requests']\">Radiological Requests </a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n    <div class=\"report-wrapper\">\n        <div *ngIf=\"showLoading\" class=\"progress\">\n            <div class=\"indeterminate\"></div>\n        </div>\n\n\n        <div class=\"customer-info  input-section animated slideInLeft\" [ngClass]=\"{'active':showBasicInfo}\">\n            <div class=\"card-panel\">\n                <div class=\"card-title\">\n                    <h5>Basic Information</h5>\n                </div>\n                <div class=\"card-content\">\n                    <form [formGroup]=\"basicForm\">\n                        <div class=\"row\">\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"surname\" formControlName=\"surname\">\n                                    <label for=\"surname\">Surname</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"first-name\" formControlName=\"first_name\">\n                                    <label for=\"first-name\">First name</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"last-name\" formControlName=\"middle_name\">\n                                    <label for=\"last-name\">Middle name</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"age\" formControlName=\"age\">\n                                    <label for=\"age\">Age</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <p>Sex</p>\n                                <p><label>\n                                    <input name=\"sex\" value=\"Male\" type=\"radio\" formControlName=\"sex\"/>\n                                    <span>Male</span>\n                                </label>\n                                <label>\n                                    <input name=\"sex\" value=\"Female\" type=\"radio\" formControlName=\"sex\"/>\n                                    <span>Female</span>\n                                </label>\n                            </p>\n                        </div>\n                        <!--<div class=\"col m3\">-->\n                        <!--<div class=\"input-field\">-->\n                        <!--<input type=\"text\" id=\"date\" class=\"datepicker\">-->\n                        <!--<label for=\"date\">Date</label>-->\n                        <!--</div>-->\n                        <!--</div>-->\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"lmp\" class=\"\"formControlName=\"lmp\">\n                                <label for=\"lmp\">LMP</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"phone-no\" formControlName=\"phone_no\">\n                                <label for=\"phone-no\">Phone No</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"address\" formControlName=\"address\">\n                                <label for=\"address\">Address</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"occupation\" formControlName=\"occupation\">\n                                <label for=\"occupation\">Occupation</label>\n                            </div>\n                        </div>\n                    </div>\n                \n                </form>\n            </div>\n        </div>\n    </div>\n        \n        <div class=\"card-panel input-section  \" [ngClass]=\"{'active':!showBasicInfo}\">\n            <div class=\"card-title\">\n                <h5>Investigation</h5>\n            </div>\n            <div class=\"divider\"></div>\n            <div class=\"card-content\">\n                <form>\n                    <div class=\"input-field col s12\">\n                        <select multiple (change)=\"handleTestsSelected($event)\" #investigationSelect name=\"investigation\" [formControl]='investigation'>\n                            <option value=\"\" disabled  >Select investigation or Digital X-ray needed</option>\n                            <optgroup label=\"Investigation\">\n                                <option value=\"1\">Pap Smear</option>\n                                <option value=\"2\">Mammography</option>\n                            </optgroup>\n                            <optgroup label=\"Digital X-Ray\">\n                                <option value=\"3\">Chest</option>\n                                <option value=\"4\">Cervical Spine</option>\n                                <option value=\"5\">Soft Tissue</option>\n                                <option value=\"6\">Thoracolumbar</option>\n                                <option value=\"7\">Lumbosacral Spin</option>\n                                <option value=\"8\">Clavicle</option>\n                                <option value=\"9\">Shoulder Join</option>\n                                <option value=\"10\">Skull</option>\n                                <option value=\"11\">Sinuses</option>\n                                <option value=\"12\">Temporamandibular (TMJ) </option>\n                                <option value=\"13\">Mandible</option>\n                                <option value=\"14\">Mastoids</option>\n                                <option value=\"15\">Sella Turcica</option>\n                                <option value=\"16\">Orbits</option>\n                                <option value=\"17\">Optic Foramina</option>\n                                <option value=\"18\">Jugular Foramina</option>\n                                <option value=\"19\">Post Nasal Space</option>\n                                <option value=\"20\">Pharynx/Upper way</option>\n                                <option value=\"21\">Thoracic Inlet</option>\n                                <option value=\"22\">Pelvis</option>\n                                <option value=\"23\">Hip Join</option>\n                                <option value=\"24\">Femur(Thigh)</option>\n                                <option value=\"25\">Knee Join</option>\n                                <option value=\"26\">Leg(Tibia/Fibula)</option>\n                                <option value=\"27\">Ankle Join</option>\n                                <option value=\"28\">Foot</option>\n                                <option value=\"29\">Hand</option>\n                                <option value=\"30\">Calcaneum</option>\n                                <option value=\"31\">Wrist Joint</option>\n                                <option value=\"32\">Elbow Joint</option>\n                                <option value=\"33\">Forearm (Radius/Ulna)</option>\n                                <option value=\"34\">Arm (Humerus)</option>\n                                <option value=\"35\">Abdomen</option>\n                                <option value=\"36\">Skeletal Survey</option>\n                            </optgroup>\n                            <optgroup label=\"Special Investigation\">\n                                <option value=\"37\">Instrvenous Urography (IVU) - Non-Ionic Contrast</option>\n                                <option value=\"38\">Hysterosalpingography (HSG)</option>\n                                <option value=\"39\">Barium Swallow</option>\n                                <option value=\"40\">Barium Meal</option>\n                                <option value=\"41\">Baruim Meal & Follow Through</option>\n                                <option value=\"42\">Double Contrast Barium Enema</option>\n                                <option value=\"43\">Retrograde Urethrocystography (RUGG)</option>\n                                <option value=\"44\">Micturating Urethrocystography (MUCG)</option>\n                                <option value=\"45\">RUCG + MUCG</option>\n                                <option value=\"46\">Fistulography</option>\n                            </optgroup>\n                            <optgroup label=\"Ultrasonography\">\n                                <option value=\"47\">Doppler/Vascular Studies</option>\n                                <option value=\"48\">Abdominopelvic Scan</option>\n                                <option value=\"49\">Pelvic Scan</option>\n                                <option value=\"50\">Abdominal</option>\n                                <option value=\"51\">Transvaginal</option>\n                                <option value=\"52\">Thyroid/Neck Scan</option>\n                                <option value=\"53\">Breast USS</option>\n                                <option value=\"54\">Scrotal USS</option>\n                                <option value=\"55\">Transrectal USS</option>\n                                <option value=\"56\">Ocular Scan</option>\n                                <option value=\"57\">Obsteric Scan</option>\n                                <option value=\"58\">Prostate Scan</option>\n                                <option value=\"59\">Biophysical Profile</option>\n                                <option value=\"60\">Musculoskeletal Scan</option>\n                                <option value=\"61\">USS Guided drainage</option>\n                                <option value=\"62\">Uss Guided biospy</option>\n                                <option value=\"63\">Follicular Tracking</option>\n                                <option value=\"64\">Transfontanelle Scan</option>\n                                <option value=\"65\">Anomaly Scan</option>\n                                <option value=\"66\">Renal scan</option>\n                            </optgroup>\n                        </select>\n                    </div>\n                    <div>\n                        <ul class=\"collection with-header\">\n                            <!--<li class=\"collection-header\"><h4>First Names</h4></li>-->\n                            <li class=\"collection-item\" *ngFor=\"let test of selectedTests; let i=index \">\n                                <div>\n                                    <span>{{test.name}}</span>\n                                    <span class=\"secondary-content\">\n                                        <span class=\"test-checkbox\" *ngFor=\"let option of test.options\">\n                                            <label>\n                                                <input type=\"checkbox\" [checked]=\"option.selected\" (change)=\"option.selected=!option.selected\"/>\n                                                <span [innerHTML]=\"option.value\"></span>\n                                            </label>\n                                        </span>\n                                    </span>\n                                </div>\n                            </li>\n                        </ul>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"action-buttons\">\n<div class=\"row\">\n    <div class=\"col s1\">\n        <button type=\"button\" *ngIf=\"!showBasicInfo\" (click)=\"prevPane()\" class=\"btn btn-large waves-effect\">Previous</button>\n    </div>\n    <div class=\"col offset-s8 s3\">\n        <button type=\"button\" *ngIf=\"showBasicInfo\" (click)=\"nextPane()\" class=\"btn btn-large waves-effect\">Next</button>\n        <button type=\"button\" [disabled]=\"showLoading\" *ngIf=\"!showBasicInfo\" class=\"btn btn-large waves-effect\" id=\"next-btn\" (click)=\"saveReport()\">Update Request</button>\n    </div>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/radiological-request-edit/radiological-request-edit.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/radiological-request-edit/radiological-request-edit.component.ts ***!
  \**********************************************************************************/
/*! exports provided: RadiologicalRequestEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadiologicalRequestEditComponent", function() { return RadiologicalRequestEditComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _radiological_test__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../radiological-test */ "./src/app/radiological-test.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_radiological_request_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/radiological-request.service */ "./src/app/services/radiological-request.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var RadiologicalRequestEditComponent = /** @class */ (function () {
    function RadiologicalRequestEditComponent(serviceProvider, route) {
        this.serviceProvider = serviceProvider;
        this.route = route;
        this.selectedTests = [];
        this.showBasicInfo = true;
        this.showLoading = false;
        this.basicForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            surname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            first_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            middle_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            sex: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            age: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            lmp: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            phone_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            occupation: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('')
        });
        this.investigation = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]([]);
    }
    RadiologicalRequestEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (param) {
            _this.id = param.id;
            _this.getRequest();
        });
    };
    RadiologicalRequestEditComponent.prototype.ngAfterViewInit = function () {
        M.AutoInit();
    };
    RadiologicalRequestEditComponent.prototype.handleTestsSelected = function (event) {
        var elm = event.currentTarget;
        var selectedOption = [].filter.call(elm.options, function (option) {
            return option.selected;
        });
        var selectedInv = [];
        // this.selectedTests.length =0;
        [].map.call(selectedOption, function (option) {
            var index = Number.parseInt(option.value);
            selectedInv.push(_radiological_test__WEBPACK_IMPORTED_MODULE_1__["radiologicalTests"][index]);
        });
        this.showSelectedOptions(selectedInv);
    };
    RadiologicalRequestEditComponent.prototype.saveReport = function () {
        var _this = this;
        var toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_4__["default"]();
        this.showLoading = true;
        this.request = __assign({ id: this.id }, this.basicForm.value, { investigations: this.selectedTests });
        this.serviceProvider.updateRequest(this.request).subscribe(function (data) {
            toast.success('Request was updated successfully');
            _this.showLoading = false;
        }, function (error) {
            toast.error('Request was not updated due to some system error please try again');
            _this.showLoading = false;
        });
    };
    RadiologicalRequestEditComponent.prototype.selectTestOption = function (index) {
    };
    RadiologicalRequestEditComponent.prototype.nextPane = function () {
        this.showBasicInfo = false;
    };
    RadiologicalRequestEditComponent.prototype.prevPane = function () {
        this.showBasicInfo = true;
    };
    RadiologicalRequestEditComponent.prototype.getRequest = function () {
        var _this = this;
        this.serviceProvider.getRequest(this.id).subscribe(function (data) {
            _this.patchForms(data);
        }, function (error) {
        });
    };
    RadiologicalRequestEditComponent.prototype.patchForms = function (data) {
        this.basicForm.patchValue(__assign({}, data));
        M.updateTextFields();
        var savedInv = [];
        Object.keys(_radiological_test__WEBPACK_IMPORTED_MODULE_1__["radiologicalTests"]).forEach(function (key) {
            data.investigations.forEach(function (save) {
                if (save.name == _radiological_test__WEBPACK_IMPORTED_MODULE_1__["radiologicalTests"][key].name) {
                    savedInv.push(key);
                }
            });
        });
        this.investigation.patchValue(savedInv);
        // this.selectedTests = data.investigations;
        M.FormSelect.init(this.invSelect.nativeElement);
        this.showSelectedOptions(data.investigations);
    };
    RadiologicalRequestEditComponent.prototype.showSelectedOptions = function (options) {
        var _this = this;
        console.log(options);
        this.selectedTests = this.selectedTests.filter(function (test) {
            for (var _i = 0, options_1 = options; _i < options_1.length; _i++) {
                var option = options_1[_i];
                if (option.name == test.name) {
                    return true;
                }
            }
            return false;
        });
        options = options.filter(function (option) {
            for (var _i = 0, _a = _this.selectedTests; _i < _a.length; _i++) {
                var test = _a[_i];
                if (test.name === option.name) {
                    return false;
                }
            }
            return true;
        });
        console.log(options);
        [].map.call(options, function (option) {
            _this.selectedTests.push(option);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('investigationSelect'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], RadiologicalRequestEditComponent.prototype, "invSelect", void 0);
    RadiologicalRequestEditComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-radiological-request-edit',
            template: __webpack_require__(/*! ./radiological-request-edit.component.html */ "./src/app/radiological-request-edit/radiological-request-edit.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [_services_radiological_request_service__WEBPACK_IMPORTED_MODULE_3__["RadiologicalRequestService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], RadiologicalRequestEditComponent);
    return RadiologicalRequestEditComponent;
}());



/***/ }),

/***/ "./src/app/radiological-request-list/radiological-request-list.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/radiological-request-list/radiological-request-list.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Radiological Request\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/radiological-request/requests']\">Radiological Requests</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t <div *ngIf=\"deleting\" class=\"progress\" style=\"margin: 0\">\n         <div class=\"indeterminate\"></div>\n     </div>\n    <div class=\"card-panel\" style=\"margin-top: 0\">\n        <div class=\"header\">\n            <div class=\"row\">\n                <div class=\"col m3 offset-m9\">\n                    <div class=\"input-field\" *ngIf=\"requests?.length>0\">\n                        <input type=\"text\" id=\"filter\" (keyup)=\"filterTable($event.target.value)\">\n                        <label for=\"filter\" >Filter</label>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"table-contents\">\n            <div class=\"center-align\" *ngIf=\"matDatasource.data.length!==0\">\n                <p class=\"info-text\">There currently no request</p>\n            </div>\n\n            <div class=\"center-align\" *ngIf=\"!loadingRequests && errorOccurred\">\n                <p class=\"info-text\">An error occurred requests could not be loaded</p>\n                <button class=\"btn\" (click)=\"getRequests()\">Try Again</button>\n            </div>\n            <app-loading [showLoading]='loadingRequests'></app-loading>\n\n\n\n           \n\n\n\n\n\n            <div class=\"wrapper animated fadeIn\">\n\n            <table mat-table [dataSource]=\"matDatasource\" matSort *ngIf=\"matDatasource.data.length!==0\">\n                <ng-container matColumnDef=\"id\">\n                    <th mat-header-cell *matHeaderCellDef> ID </th>\n                    <td mat-cell *matCellDef=\"let request\"> {{request.id}} </td>\n                </ng-container>\n\n\n                <ng-container matColumnDef=\"patient_name\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Patient Name </th>\n                    <td mat-cell *matCellDef=\"let request\"> {{request.first_name+' '+request.surname}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"age\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Age </th>\n                    <td mat-cell *matCellDef=\"let request\"> {{request.age}} </td>\n                </ng-container>\n\n                <ng-container matColumnDef=\"sex\">\n                    <th mat-header-cell *matHeaderCellDef mat-sort-header> Sex </th>\n                    <td mat-cell *matCellDef=\"let request\"> {{request.sex}} </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"view\">\n                    <th mat-header-cell *matHeaderCellDef > View Details</th>\n                    <td mat-cell *matCellDef=\"let request\"> <a [routerLink]=\"['/radiological-request/requests/'+request.id]\">View Details</a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"edit\">\n                    <th mat-header-cell *matHeaderCellDef> Edit </th>\n                    <td mat-cell *matCellDef=\"let request\"> <a [routerLink]=\"['/radiological-request/edit/'+request.id]\"class=\"btn green accent-3\"><i class=\"material-icons\">edit</i></a> </td>\n                </ng-container>\n\n                 <ng-container matColumnDef=\"delete\">\n                    <th mat-header-cell *matHeaderCellDef> Delete </th>\n                    <td mat-cell *matCellDef=\"let request\"><button (click)=\"deleteRequest(request.id,i)\" [disabled]=\"deleting\" class=\"btn red accent-3\"><i class=\"material-icons\">delete</i></button>\n                </ng-container>\n\n\n                <tr mat-header-row *matHeaderRowDef=\"columnsToDisplay\"></tr>\n                <tr mat-row *matRowDef=\"let row; columns: columnsToDisplay;\"></tr>\n\n\n               \n            </table>\n             <mat-paginator [pageSize]=\"10\" \n                            [pageSizeOptions]=\"[5, 10, 20]\" \n                            showFirstLastButtons\n                            *ngIf=\"matDatasource.data.length!==0\"\n                            ></mat-paginator>\n        \n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/radiological-request-list/radiological-request-list.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/radiological-request-list/radiological-request-list.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/radiological-request-list/radiological-request-list.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/radiological-request-list/radiological-request-list.component.ts ***!
  \**********************************************************************************/
/*! exports provided: RadiologicalRequestListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadiologicalRequestListComponent", function() { return RadiologicalRequestListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_radiological_request_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/radiological-request.service */ "./src/app/services/radiological-request.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RadiologicalRequestListComponent = /** @class */ (function () {
    function RadiologicalRequestListComponent(serviceProvider) {
        this.serviceProvider = serviceProvider;
        this.errorOccurred = true;
        this.loadingRequests = false;
        this.deleting = false;
        this.matDatasource = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableDataSource"]();
        this.columnsToDisplay = ['id', 'patient_name', 'age', 'sex', 'view', 'edit', 'delete'];
    }
    Object.defineProperty(RadiologicalRequestListComponent.prototype, "content", {
        set: function (content) {
            this.sort = content;
            if (this.sort) {
                this.matDatasource.sort = this.sort;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RadiologicalRequestListComponent.prototype, "pager", {
        set: function (content) {
            this.paginator = content;
            if (this.paginator) {
                this.matDatasource.paginator = this.paginator;
            }
        },
        enumerable: true,
        configurable: true
    });
    RadiologicalRequestListComponent.prototype.ngOnInit = function () {
        this.getRequests();
        this.matDatasource.paginator = this.paginator;
        this.matDatasource.sort = this.sort;
    };
    RadiologicalRequestListComponent.prototype.getRequests = function () {
        var _this = this;
        this.errorOccurred = false;
        this.loadingRequests = true;
        this.serviceProvider.getRequests().subscribe(function (data) {
            _this.loadingRequests = false;
            _this.requests = data;
            _this.matDatasource.data = _this.requests;
        }, function (err) {
            _this.loadingRequests = false;
            _this.errorOccurred = true;
        });
    };
    RadiologicalRequestListComponent.prototype.deleteRequest = function (id, index) {
        var _this = this;
        var toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
        var options = {
            cancel: true,
            cancelText: 'Cancel',
            cancelCallBack: function (event) { },
            confirm: true,
            confirmText: 'Delete',
            confirmCallBack: function (event) {
                _this.deleting = true;
                _this.serviceProvider.deleteRequest(id).subscribe(function (res) {
                    toast.success('Request was deleted succesfully');
                    _this.deleting = false;
                    _this.requests.splice(index, 1);
                }, function (err) {
                    toast.error("Request could not be deleted at the moment please try again");
                });
            }
        };
        roar('Delete Request', 'Do you realy want to  <strong>Delete </strong> this request', options);
    };
    RadiologicalRequestListComponent.prototype.filterTable = function (searchText) {
        this.matDatasource.filter = searchText.trim().toLowerCase();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"])
    ], RadiologicalRequestListComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"])
    ], RadiologicalRequestListComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSort"]])
    ], RadiologicalRequestListComponent.prototype, "content", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatPaginator"]])
    ], RadiologicalRequestListComponent.prototype, "pager", null);
    RadiologicalRequestListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-radiological-request-list',
            template: __webpack_require__(/*! ./radiological-request-list.component.html */ "./src/app/radiological-request-list/radiological-request-list.component.html"),
            styles: [__webpack_require__(/*! ./radiological-request-list.component.scss */ "./src/app/radiological-request-list/radiological-request-list.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_radiological_request_service__WEBPACK_IMPORTED_MODULE_1__["RadiologicalRequestService"]])
    ], RadiologicalRequestListComponent);
    return RadiologicalRequestListComponent;
}());



/***/ }),

/***/ "./src/app/radiological-request-view/radiological-request-view.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/radiological-request-view/radiological-request-view.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Radiological Request details\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/radiological-request/requests']\">Radiological Requests details</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\t<div class=\"card-panel\">\n\t<app-loading [showLoading]=\"loading\"></app-loading>\n\t<div class=\"center-align\" *ngIf=\"errorOccurred\">\n\t\t<p>An error occurred content could not be loaded</p>\n\t\t<button  class=\"btn waves-effect\" (click)=\"getRequest()\">Try again</button>\n\t</div>\n\t<div *ngIf=\"!loading && !errorOccurred\">\n\t\t<table>\n\t\t\t<thead>\n\t\t\t\t<tr>\n\t\t\t\t\t<th>S/N</th>\n\t\t\t\t\t\n\t\t\t\t\t<th width=\"40%\">\n\t\t\t\t\t\tInvestigation\n\t\t\t\t\t</th>\n\t\t\t\t\t<th>Options</th>\n\t\t\t\t\t<th>\n\t\t\t\t\t\t<i class=\"material-icons\">check</i>\n\t\t\t\t\t</th>\n\t\t\t\t</tr>\n\t\t\t</thead>\n\t\t\t<tbody>\n\t\t\t\t<tr>\n\t\t\t\t\t\n\t\t\t\t</tr>\n\t\t\t\t<tr *ngFor=\"let inv of investigations.slice(2); let i = index\" [ngClass]=\"{'print-only-row': !wasSelected(inv) }\">\n\t\t\t\t\t<td>{{i+1}}</td>\n\t\t\t\t\t<td>{{inv.name}}</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<div class=\"row option-row\">\n\t\t\t\t\t\t\t<div class=\"col\" *ngFor=\"let option of inv.options\" >\n\t\t\t\t\t\t\t\t<span class=\"check-box\">\n\t\t\t\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"option.selected\">check</i>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span [innerHTML]=\"option.value\"></span>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<i class=\"material-icons\" *ngIf=\"wasSelected(inv)\">check</i>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n\t\t\t</tbody>\n\t\t</table>\n\t</div>\n</div>\n<div class=\"fixed-action-btn\">\n\t<button class=\"btn-floating btn-large red\" (click)=\"printPage()\">\n\t<i class=\"large material-icons\">print</i>\n\t</button>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/radiological-request-view/radiological-request-view.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/radiological-request-view/radiological-request-view.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".option-row {\n  margin: 0 !important; }\n\nth, td {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  padding: 5px !important; }\n\ntr > td:first-child {\n  text-align: center; }\n\n.material-icons {\n  font-size: 18px;\n  margin: 0 !important;\n  padding: 0 !important;\n  line-height: 0;\n  height: 10px; }\n\n.print-only-row {\n  display: none; }\n\n.check-box {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  height: 20px;\n  width: 20px;\n  display: block;\n  float: left;\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/radiological-request-view/radiological-request-view.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/radiological-request-view/radiological-request-view.component.ts ***!
  \**********************************************************************************/
/*! exports provided: RadiologicalRequestViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadiologicalRequestViewComponent", function() { return RadiologicalRequestViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _radiological_test__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../radiological-test */ "./src/app/radiological-test.ts");
/* harmony import */ var _services_radiological_request_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/radiological-request.service */ "./src/app/services/radiological-request.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RadiologicalRequestViewComponent = /** @class */ (function () {
    function RadiologicalRequestViewComponent(serviceProvider, route) {
        this.serviceProvider = serviceProvider;
        this.route = route;
        this.loading = false;
        this.errorOccurred = false;
        this.investigations = [];
    }
    RadiologicalRequestViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (param) {
            _this.id = param.id;
            _this.getRequest();
        });
    };
    RadiologicalRequestViewComponent.prototype.getRequest = function () {
        var _this = this;
        this.loading = true;
        this.errorOccurred = false;
        this.serviceProvider.getRequest(this.id).subscribe(function (data) {
            _this.loading = false;
            _this.parseRequest(data);
        }, function (err) {
            _this.loading = false;
            _this.errorOccurred = true;
        });
    };
    RadiologicalRequestViewComponent.prototype.parseRequest = function (request) {
        var _this = this;
        var saveInv = _radiological_test__WEBPACK_IMPORTED_MODULE_2__["radiologicalTests"];
        var invs = Object.keys(saveInv);
        for (var _i = 0, invs_1 = invs; _i < invs_1.length; _i++) {
            var inv = invs_1[_i];
            for (var _a = 0, _b = request.investigations; _a < _b.length; _a++) {
                var invest = _b[_a];
                if (invest.name === saveInv[inv].name) {
                    saveInv[inv] = invest;
                }
            }
        }
        this.request = request;
        Object.keys(saveInv).forEach(function (index) {
            _this.investigations.push(saveInv[index]);
        });
    };
    RadiologicalRequestViewComponent.prototype.wasSelected = function (invest) {
        return this.request.investigations.find(function (inv) {
            return invest.name == inv.name;
        }) != undefined;
    };
    RadiologicalRequestViewComponent.prototype.printPage = function () {
        window.print();
    };
    RadiologicalRequestViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-radiological-request-view',
            template: __webpack_require__(/*! ./radiological-request-view.component.html */ "./src/app/radiological-request-view/radiological-request-view.component.html"),
            styles: [__webpack_require__(/*! ./radiological-request-view.component.scss */ "./src/app/radiological-request-view/radiological-request-view.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_radiological_request_service__WEBPACK_IMPORTED_MODULE_3__["RadiologicalRequestService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], RadiologicalRequestViewComponent);
    return RadiologicalRequestViewComponent;
}());



/***/ }),

/***/ "./src/app/radiological-request/radiological-request.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/radiological-request/radiological-request.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"New Radiological Request\">\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n    <li class=\"breadcrumb-item\"><a [routerLink]=\"['/radiological-request/new']\">New Radiological Request</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n    <div class=\"report-wrapper\">\n        <div *ngIf=\"showLoading\" class=\"progress\">\n            <div class=\"indeterminate\"></div>\n        </div>\n\n\n        <div class=\"customer-info  input-section animated slideInLeft\" [ngClass]=\"{'active':showBasicInfo}\">\n            <div class=\"card-panel\">\n                <div class=\"card-title\">\n                    <h5>Basic Information</h5>\n                </div>\n                <div class=\"card-content\">\n                    <form [formGroup]=\"basicForm\">\n                        <div class=\"row\">\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"surname\" formControlName=\"surname\">\n                                    <label for=\"surname\">Surname</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"first-name\" formControlName=\"first_name\">\n                                    <label for=\"first-name\">First name</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"last-name\" formControlName=\"middle_name\">\n                                    <label for=\"last-name\">Middle name</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <div class=\"input-field\">\n                                    <input type=\"text\" id=\"age\" formControlName=\"age\">\n                                    <label for=\"age\">Age</label>\n                                </div>\n                            </div>\n                            <div class=\"col m3\">\n                                <p>Sex</p>\n                                <p><label>\n                                    <input name=\"sex\" value=\"Male\" type=\"radio\" formControlName=\"sex\"/>\n                                    <span>Male</span>\n                                </label>\n                                <label>\n                                    <input name=\"sex\" value=\"Female\" type=\"radio\" formControlName=\"sex\"/>\n                                    <span>Female</span>\n                                </label>\n                            </p>\n                        </div>\n                        <!--<div class=\"col m3\">-->\n                        <!--<div class=\"input-field\">-->\n                        <!--<input type=\"text\" id=\"date\" class=\"datepicker\">-->\n                        <!--<label for=\"date\">Date</label>-->\n                        <!--</div>-->\n                        <!--</div>-->\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"lmp\" class=\"\"formControlName=\"lmp\">\n                                <label for=\"lmp\">LMP</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"phone-no\" formControlName=\"phone_no\">\n                                <label for=\"phone-no\">Phone No</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"address\" formControlName=\"address\">\n                                <label for=\"address\">Address</label>\n                            </div>\n                        </div>\n                        <div class=\"col m3\">\n                            <div class=\"input-field\">\n                                <input type=\"text\" id=\"occupation\" formControlName=\"occupation\">\n                                <label for=\"occupation\">Occupation</label>\n                            </div>\n                        </div>\n                    </div>\n                \n                </form>\n            </div>\n        </div>\n    </div>\n        \n        <div class=\"card-panel input-section  \" [ngClass]=\"{'active':!showBasicInfo}\">\n            <div class=\"card-title\">\n                <h5>Investigation</h5>\n            </div>\n            <div class=\"divider\"></div>\n            <div class=\"card-content\">\n                <form>\n                    <div class=\"input-field col s12\">\n                        <select multiple (change)=\"handleTestsSelected($event)\">\n                            <option value=\"\" disabled >Select investigation or Digital X-ray needed</option>\n                            <optgroup label=\"Investigation\">\n                                <option value=\"1\">Pap Smear</option>\n                                <option value=\"2\">Mammography</option>\n                            </optgroup>\n                            <optgroup label=\"Digital X-Ray\">\n                                <option value=\"3\">Chest</option>\n                                <option value=\"4\">Cervical Spine</option>\n                                <option value=\"5\">Soft Tissue</option>\n                                <option value=\"6\">Thoracolumbar</option>\n                                <option value=\"7\">Lumbosacral Spin</option>\n                                <option value=\"8\">Clavicle</option>\n                                <option value=\"9\">Shoulder Join</option>\n                                <option value=\"10\">Skull</option>\n                                <option value=\"11\">Sinuses</option>\n                                <option value=\"12\">Temporamandibular (TMJ) </option>\n                                <option value=\"13\">Mandible</option>\n                                <option value=\"14\">Mastoids</option>\n                                <option value=\"15\">Sella Turcica</option>\n                                <option value=\"16\">Orbits</option>\n                                <option value=\"17\">Optic Foramina</option>\n                                <option value=\"18\">Jugular Foramina</option>\n                                <option value=\"19\">Post Nasal Space</option>\n                                <option value=\"20\">Pharynx/Upper way</option>\n                                <option value=\"21\">Thoracic Inlet</option>\n                                <option value=\"22\">Pelvis</option>\n                                <option value=\"23\">Hip Join</option>\n                                <option value=\"24\">Femur(Thigh)</option>\n                                <option value=\"25\">Knee Join</option>\n                                <option value=\"26\">Leg(Tibia/Fibula)</option>\n                                <option value=\"27\">Ankle Join</option>\n                                <option value=\"28\">Foot</option>\n                                <option value=\"29\">Hand</option>\n                                <option value=\"30\">Calcaneum</option>\n                                <option value=\"31\">Wrist Joint</option>\n                                <option value=\"32\">Elbow Joint</option>\n                                <option value=\"33\">Forearm (Radius/Ulna)</option>\n                                <option value=\"34\">Arm (Humerus)</option>\n                                <option value=\"35\">Abdomen</option>\n                                <option value=\"36\">Skeletal Survey</option>\n                            </optgroup>\n                            <optgroup label=\"Special Investigation\">\n                                <option value=\"37\">Instrvenous Urography (IVU) - Non-Ionic Contrast</option>\n                                <option value=\"38\">Hysterosalpingography (HSG)</option>\n                                <option value=\"39\">Barium Swallow</option>\n                                <option value=\"40\">Barium Meal</option>\n                                <option value=\"41\">Baruim Meal & Follow Through</option>\n                                <option value=\"42\">Double Contrast Barium Enema</option>\n                                <option value=\"43\">Retrograde Urethrocystography (RUGG)</option>\n                                <option value=\"44\">Micturating Urethrocystography (MUCG)</option>\n                                <option value=\"45\">RUCG + MUCG</option>\n                                <option value=\"46\">Fistulography</option>\n                            </optgroup>\n                            <optgroup label=\"Ultrasonography\">\n                                <option value=\"47\">Doppler/Vascular Studies</option>\n                                <option value=\"48\">Abdominopelvic Scan</option>\n                                <option value=\"49\">Pelvic Scan</option>\n                                <option value=\"50\">Abdominal</option>\n                                <option value=\"51\">Transvaginal</option>\n                                <option value=\"52\">Thyroid/Neck Scan</option>\n                                <option value=\"53\">Breast USS</option>\n                                <option value=\"54\">Scrotal USS</option>\n                                <option value=\"55\">Transrectal USS</option>\n                                <option value=\"56\">Ocular Scan</option>\n                                <option value=\"57\">Obsteric Scan</option>\n                                <option value=\"58\">Prostate Scan</option>\n                                <option value=\"59\">Biophysical Profile</option>\n                                <option value=\"60\">Musculoskeletal Scan</option>\n                                <option value=\"61\">USS Guided drainage</option>\n                                <option value=\"62\">Uss Guided biospy</option>\n                                <option value=\"63\">Follicular Tracking</option>\n                                <option value=\"64\">Transfontanelle Scan</option>\n                                <option value=\"65\">Anomaly Scan</option>\n                                <option value=\"66\">Renal scan</option>\n                            </optgroup>\n                        </select>\n                    </div>\n                    <div>\n                        <ul class=\"collection with-header\">\n                            <!--<li class=\"collection-header\"><h4>First Names</h4></li>-->\n                            <li class=\"collection-item\" *ngFor=\"let test of selectedTests; let i=index \">\n                                <div>\n                                    <span>{{test.name}}</span>\n                                    <span class=\"secondary-content\">\n                                        <span class=\"test-checkbox\" *ngFor=\"let option of test.options\">\n                                            <label>\n                                                <input type=\"checkbox\" [checked]=\"option.selected\" (change)=\"option.selected=!option.selected\"/>\n                                                <span [innerHTML]=\"option.value\"></span>\n                                            </label>\n                                        </span>\n                                    </span>\n                                </div>\n                            </li>\n                        </ul>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"action-buttons\">\n<div class=\"row\">\n    <div class=\"col s1\">\n        <button type=\"button\" *ngIf=\"!showBasicInfo\" (click)=\"prevPane()\" class=\"btn waves-effect waves-light btn-large waves-effect\">Previous</button>\n    </div>\n    <div class=\"col offset-s8 s3\">\n        <button type=\"button\" *ngIf=\"showBasicInfo\" (click)=\"nextPane()\" class=\"btn waves-effect waves-light btn-large waves-effect\">Next</button>\n        <button type=\"button\" [disabled]=\"showLoading\" *ngIf=\"!showBasicInfo\" class=\"btn waves-effect waves-light btn-large waves-effect\" id=\"next-btn\" (click)=\"saveReport()\">Save Request</button>\n    </div>\n</div>\n</div>\n"

/***/ }),

/***/ "./src/app/radiological-request/radiological-request.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/radiological-request/radiological-request.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/radiological-request/radiological-request.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/radiological-request/radiological-request.component.ts ***!
  \************************************************************************/
/*! exports provided: RadiologicalRequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadiologicalRequestComponent", function() { return RadiologicalRequestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _radiological_test__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../radiological-test */ "./src/app/radiological-test.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_radiological_request_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/radiological-request.service */ "./src/app/services/radiological-request.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RadiologicalRequestComponent = /** @class */ (function () {
    function RadiologicalRequestComponent(serviceProvider) {
        this.serviceProvider = serviceProvider;
        this.selectedTests = [];
        this.showBasicInfo = true;
        this.showLoading = false;
        this.basicForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            surname: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            first_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            middle_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            sex: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            age: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            lmp: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            phone_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](''),
            occupation: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('')
        });
    }
    RadiologicalRequestComponent.prototype.ngOnInit = function () {
    };
    RadiologicalRequestComponent.prototype.ngAfterViewInit = function () {
        M.AutoInit();
    };
    RadiologicalRequestComponent.prototype.handleTestsSelected = function (event) {
        var _this = this;
        var elm = event.currentTarget;
        var selectedOption = [].filter.call(elm.options, function (option) {
            return option.selected;
        });
        this.selectedTests.length = 0;
        [].map.call(selectedOption, function (option) {
            var index = Number.parseInt(option.value);
            _this.selectedTests.push(_radiological_test__WEBPACK_IMPORTED_MODULE_1__["radiologicalTests"][index]);
        });
    };
    RadiologicalRequestComponent.prototype.saveReport = function () {
        var _this = this;
        var toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_4__["default"]();
        this.showLoading = true;
        this.request = __assign({}, this.basicForm.value, { investigations: this.selectedTests });
        this.serviceProvider.createRequest(this.request).subscribe(function (data) {
            toast.success('Request was saved successfully');
            _this.showLoading = false;
            _this.resetForm();
        }, function (error) {
            toast.error('Request was not save due to some system error please try again');
            _this.showLoading = false;
        });
    };
    RadiologicalRequestComponent.prototype.selectTestOption = function (index) {
        console.log(index);
    };
    RadiologicalRequestComponent.prototype.nextPane = function () {
        this.showBasicInfo = false;
    };
    RadiologicalRequestComponent.prototype.prevPane = function () {
        this.showBasicInfo = true;
    };
    RadiologicalRequestComponent.prototype.resetForm = function () {
    };
    RadiologicalRequestComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-radiological-request',
            template: __webpack_require__(/*! ./radiological-request.component.html */ "./src/app/radiological-request/radiological-request.component.html"),
            styles: [__webpack_require__(/*! ./radiological-request.component.scss */ "./src/app/radiological-request/radiological-request.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_radiological_request_service__WEBPACK_IMPORTED_MODULE_3__["RadiologicalRequestService"]])
    ], RadiologicalRequestComponent);
    return RadiologicalRequestComponent;
}());



/***/ }),

/***/ "./src/app/radiological-test.ts":
/*!**************************************!*\
  !*** ./src/app/radiological-test.ts ***!
  \**************************************/
/*! exports provided: radiologicalTests */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "radiologicalTests", function() { return radiologicalTests; });
var radiologicalTests = {
    1: {
        name: "Pap smear",
        options: []
    },
    2: {
        name: "Mammography",
        options: []
    },
    3: {
        name: "Chest",
        options: [
            {
                value: "PA",
                selected: false
            },
            {
                value: "Lat",
                selected: false,
            },
            {
                value: "Apical view",
                selected: false
            }
        ]
    },
    4: {
        name: "Cervical Spin",
        options: []
    },
    5: {
        name: "Soft Tissue Neck",
        options: []
    },
    6: {
        name: "Thoracolumbar Spine",
        options: []
    },
    7: {
        name: "Lumbosacral Spine",
        options: [
            {
                value: "AP",
                selected: false
            },
            {
                value: "Lat",
                selected: false,
            },
            {
                value: "Oblique",
                selected: false,
            }
        ]
    },
    8: {
        name: "Clavicle",
        options: []
    },
    9: {
        name: "Shoulder join",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            },
            {
                value: "Int. Rot.",
                selected: false,
            },
            {
                value: "Ext. Rot.",
                selected: false
            },
            {
                value: 'Lat Oblique "Y"',
                selected: false
            }
        ]
    },
    10: {
        name: "Skull",
        options: [
            {
                value: "OF",
                selected: false,
            },
            {
                value: "Lat",
                selected: false
            },
            {
                value: "Towne's",
                selected: false
            },
            {
                value: "Reverse Towne's",
                selected: false
            },
            {
                value: "SMV",
                selected: false
            }
        ]
    },
    11: {
        name: "Sinuses",
        options: [
            {
                value: "OF",
                selected: false
            },
            {
                value: "Lat",
                selected: false
            },
            {
                value: "OM",
                selected: false
            }
        ]
    },
    12: {
        name: "Temporamandibular (TMJ)",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            },
            {
                value: "Open",
                selected: false
            },
            {
                value: "Close",
                selected: false
            },
            {
                value: "Towne's",
                selected: false,
            }
        ]
    },
    13: {
        name: "Mandible",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            },
            {
                value: "Oblique",
                selected: false
            },
            {
                value: "OF",
                selected: false
            }
        ]
    },
    14: {
        name: "Mastoids/IAM",
        options: [
            {
                value: "Oblique",
                selected: false
            },
            {
                value: "Profile",
                selected: false
            },
        ]
    },
    15: {
        name: "Sella Turcica",
        options: [
            {
                value: "Leteral cone view",
                selected: false
            },
            {
                value: "OFF 20<sup>0</sup>",
                selected: false
            },
        ]
    },
    16: {
        name: "Orbits",
        options: [
            {
                value: "OM",
                selected: false
            },
            {
                value: "OF",
                selected: false
            }
        ]
    },
    17: {
        name: "Optic Foramina",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            },
            {
                value: "Oblique",
                selected: false
            }
        ]
    },
    19: {
        name: "Jugular Foramina",
        options: [
            {
                value: "SMV",
                selected: false
            }
        ]
    },
    20: {
        name: "Post Nasal Space",
        options: []
    },
    21: {
        name: "Pharynx/Upper way",
        options: []
    },
    22: {
        name: "Thoracic Inlet",
        options: []
    },
    23: {
        name: "Pelvis",
        options: []
    },
    24: {
        name: "Hip joint",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    25: {
        name: "Femur Thigh",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    26: {
        name: "Knee Joint",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    27: {
        name: "Leg (Tibia/Fibula)",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    28: {
        name: "Ankle Joint",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            },
            {
                value: "AP",
                selected: false
            },
            {
                value: "Lat",
                selected: false
            },
            {
                value: "Mortise",
                selected: false
            }
        ]
    },
    29: {
        name: "Foot",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    30: {
        name: "Calcaneum",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            },
            {
                value: "Axial",
                selected: false
            },
            {
                value: "Lateral",
                selected: false
            }
        ]
    },
    31: {
        name: "Wrist Joint",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    32: {
        name: "Elbow Joint",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    33: {
        name: "Fore arm (Radius/Ulna)",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    34: {
        name: "Arm (Humerus)",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    35: {
        name: "Abdomen",
        options: [
            {
                value: "RT",
                selected: false
            },
            {
                value: "LT",
                selected: false
            }
        ]
    },
    36: {
        name: "Skeletal Survey",
        options: [
            {
                value: "Supine",
                selected: false
            },
            {
                value: "Erect",
                selected: false
            },
            {
                value: "KUB",
                selected: false
            }
        ]
    },
    37: {
        name: "Instravenous Urography(IVU) - Non-Ionic Contrast",
        options: []
    },
    38: {
        name: "Hysterosalpingography (HSG)",
        options: []
    },
    39: {
        name: "Barium Swallow",
        options: []
    },
    40: {
        name: "Barium Meal",
        options: []
    },
    41: {
        name: "Barium Meal & Follow Through",
        options: []
    },
    42: {
        name: "Double Contrast Barium Enema",
        options: []
    },
    43: {
        name: "Retrograde Urethrocystography (RUCG)",
        options: []
    },
    44: {
        name: "Micturating Urethrocysterography (MUCCG)",
        options: []
    },
    45: {
        name: "RUCG + MUCG",
        options: []
    },
    46: {
        name: "Fistulography",
        options: []
    },
    47: {
        name: "Doppler/Vascular Studies",
        options: []
    },
    48: {
        name: "Abdominopelvic Scan",
        options: []
    },
    49: {
        name: "Pelvic Scan",
        options: []
    },
    50: {
        name: "Abdominal",
        options: []
    },
    51: {
        name: "Transvaginal",
        options: []
    },
    52: {
        name: "Throid/Neck Scan",
        options: []
    },
    53: {
        name: "Breast USS",
        options: []
    },
    54: {
        name: "Scrotal USS",
        options: []
    },
    55: {
        name: "Transcretal USS",
        options: []
    },
    56: {
        name: "Ocular Scan",
        options: []
    },
    57: {
        name: "Obsteric Scan",
        options: []
    },
    58: {
        name: "Prostate Scan",
        options: []
    },
    59: {
        name: "Biophysical Profile",
        options: []
    },
    60: {
        name: "Musculoskeletal Scan",
        options: []
    },
    61: {
        name: "USS Guided drainage",
        options: []
    },
    62: {
        name: "USS Guided biospy",
        options: []
    },
    63: {
        name: "Follicular Tracking",
        options: []
    },
    64: {
        name: "Transfontanelle Scan",
        options: []
    },
    65: {
        name: "Anomaly Scan",
        options: []
    },
    66: {
        name: "Renal Scan",
        options: []
    }
};



/***/ }),

/***/ "./src/app/route/app-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/route/app-routing.module.ts ***!
  \*********************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../dashboard/dashboard.component */ "./src/app/dashboard/dashboard.component.ts");
/* harmony import */ var _haematology_create_report_create_report_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../haematology/create-report/create-report.component */ "./src/app/haematology/create-report/create-report.component.ts");
/* harmony import */ var _chemical_pathology_create_chemical_pathology_create_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../chemical-pathology/create-chemical-pathology/create-chemical-pathology.component */ "./src/app/chemical-pathology/create-chemical-pathology/create-chemical-pathology.component.ts");
/* harmony import */ var _medical_microbiology_create_medical_microbiology_create_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../medical-microbiology/create-medical-microbiology/create-medical-microbiology.component */ "./src/app/medical-microbiology/create-medical-microbiology/create-medical-microbiology.component.ts");
/* harmony import */ var _radiological_request_radiological_request_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../radiological-request/radiological-request.component */ "./src/app/radiological-request/radiological-request.component.ts");
/* harmony import */ var _haematology_haemtology_reports_haematology_reports_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../haematology/haemtology-reports/haematology-reports.component */ "./src/app/haematology/haemtology-reports/haematology-reports.component.ts");
/* harmony import */ var _haematology_haematology_report_view_haematology_report_view_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../haematology/haematology-report-view/haematology-report-view.component */ "./src/app/haematology/haematology-report-view/haematology-report-view.component.ts");
/* harmony import */ var _chemical_pathology_chemical_pathology_reports_chemical_pathology_reports_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component */ "./src/app/chemical-pathology/chemical-pathology-reports/chemical-pathology-reports.component.ts");
/* harmony import */ var _chemical_pathology_chemical_pathology_edit_chemical_pathology_edit_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component */ "./src/app/chemical-pathology/chemical-pathology-edit/chemical-pathology-edit.component.ts");
/* harmony import */ var _chemical_pathology_chemical_pathology_view_chemical_pathology_view_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../chemical-pathology/chemical-pathology-view/chemical-pathology-view.component */ "./src/app/chemical-pathology/chemical-pathology-view/chemical-pathology-view.component.ts");
/* harmony import */ var _medical_microbiology_medical_microbiology_reports_medical_microbiology_reports_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component */ "./src/app/medical-microbiology/medical-microbiology-reports/medical-microbiology-reports.component.ts");
/* harmony import */ var _medical_microbiology_edit_medical_microbiology_edit_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component */ "./src/app/medical-microbiology/edit-medical-microbiology/edit-medical-microbiology.component.ts");
/* harmony import */ var _medical_microbiology_medical_microbiology_view_medical_microbiology_view_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../medical-microbiology/medical-microbiology-view/medical-microbiology-view.component */ "./src/app/medical-microbiology/medical-microbiology-view/medical-microbiology-view.component.ts");
/* harmony import */ var _radiological_request_list_radiological_request_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../radiological-request-list/radiological-request-list.component */ "./src/app/radiological-request-list/radiological-request-list.component.ts");
/* harmony import */ var _radiological_request_edit_radiological_request_edit_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../radiological-request-edit/radiological-request-edit.component */ "./src/app/radiological-request-edit/radiological-request-edit.component.ts");
/* harmony import */ var _radiological_request_view_radiological_request_view_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../radiological-request-view/radiological-request-view.component */ "./src/app/radiological-request-view/radiological-request-view.component.ts");
/* harmony import */ var _patient_create_patient_create_patient_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../patient/create-patient/create-patient.component */ "./src/app/patient/create-patient/create-patient.component.ts");
/* harmony import */ var _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../patient/patient-list/patient-list.component */ "./src/app/patient/patient-list/patient-list.component.ts");
/* harmony import */ var _patient_edit_patient_edit_patient_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../patient/edit-patient/edit-patient.component */ "./src/app/patient/edit-patient/edit-patient.component.ts");
/* harmony import */ var _haematology_edit_report_edit_report_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../haematology/edit-report/edit-report.component */ "./src/app/haematology/edit-report/edit-report.component.ts");
/* harmony import */ var _settings_settings_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../settings/settings.component */ "./src/app/settings/settings.component.ts");
/* harmony import */ var _manage_labs_manage_labs_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../manage-labs/manage-labs.component */ "./src/app/manage-labs/manage-labs.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _Auth_auth_guard__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ../Auth/auth.guard */ "./src/app/Auth/auth.guard.ts");
/* harmony import */ var _patient_patient_log_patient_log_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ../patient/patient-log/patient-log.component */ "./src/app/patient/patient-log/patient-log.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









// import { HaematologyEditComponent } from '../haematology-edit/haematology-edit.component';


















var routes = [
    {
        path: '',
        canActivate: [_Auth_auth_guard__WEBPACK_IMPORTED_MODULE_25__["AuthGuard"]],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"]
            },
            {
                path: 'haematology/new',
                component: _haematology_create_report_create_report_component__WEBPACK_IMPORTED_MODULE_3__["HaematologyCreateReport"],
            },
            {
                path: 'chemical-pathology/new',
                component: _chemical_pathology_create_chemical_pathology_create_chemical_pathology_component__WEBPACK_IMPORTED_MODULE_4__["CreateChemicalPathologyComponent"]
            },
            {
                path: 'medical-microbiology-and-parasitology/new',
                component: _medical_microbiology_create_medical_microbiology_create_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_5__["CreateMedicalMicrobiologyComponent"]
            },
            {
                path: 'radiological-request/new',
                component: _radiological_request_radiological_request_component__WEBPACK_IMPORTED_MODULE_6__["RadiologicalRequestComponent"],
            },
            {
                path: 'haematology/reports',
                component: _haematology_haemtology_reports_haematology_reports_component__WEBPACK_IMPORTED_MODULE_7__["HaematologyReportsComponent"]
            },
            {
                path: 'haematology/reports/:id',
                component: _haematology_haematology_report_view_haematology_report_view_component__WEBPACK_IMPORTED_MODULE_8__["HaematologyReportViewComponent"],
            },
            {
                path: 'haematology/edit/:id',
                component: _haematology_edit_report_edit_report_component__WEBPACK_IMPORTED_MODULE_21__["EditReportComponent"]
            },
            {
                path: 'chemical-pathology/reports',
                component: _chemical_pathology_chemical_pathology_reports_chemical_pathology_reports_component__WEBPACK_IMPORTED_MODULE_9__["ChemicalPathologyReportsComponent"]
            },
            {
                path: 'chemical-pathology/edit/:id',
                component: _chemical_pathology_chemical_pathology_edit_chemical_pathology_edit_component__WEBPACK_IMPORTED_MODULE_10__["ChemicalPathologyEditComponent"]
            },
            {
                path: 'chemical-pathology/reports/:id',
                component: _chemical_pathology_chemical_pathology_view_chemical_pathology_view_component__WEBPACK_IMPORTED_MODULE_11__["ChemicalPathologyViewComponent"]
            },
            {
                path: 'medical-microbiology-and-parasitology/reports',
                component: _medical_microbiology_medical_microbiology_reports_medical_microbiology_reports_component__WEBPACK_IMPORTED_MODULE_12__["MedicalMicrobiologyReportsComponent"]
            },
            {
                path: 'medical-microbiology-and-parasitology/edit/:id',
                component: _medical_microbiology_edit_medical_microbiology_edit_medical_microbiology_component__WEBPACK_IMPORTED_MODULE_13__["EditMedicalMicrobiologyComponent"]
            },
            {
                path: 'medical-microbiology-and-parasitology/reports/:id',
                component: _medical_microbiology_medical_microbiology_view_medical_microbiology_view_component__WEBPACK_IMPORTED_MODULE_14__["MedicalMicrobiologyViewComponent"]
            },
            {
                path: 'radiological-request/requests',
                component: _radiological_request_list_radiological_request_list_component__WEBPACK_IMPORTED_MODULE_15__["RadiologicalRequestListComponent"]
            },
            {
                path: 'radiological-request/edit/:id',
                component: _radiological_request_edit_radiological_request_edit_component__WEBPACK_IMPORTED_MODULE_16__["RadiologicalRequestEditComponent"]
            },
            {
                path: 'radiological-request/requests/:id',
                component: _radiological_request_view_radiological_request_view_component__WEBPACK_IMPORTED_MODULE_17__["RadiologicalRequestViewComponent"]
            },
            {
                path: 'patient/new',
                component: _patient_create_patient_create_patient_component__WEBPACK_IMPORTED_MODULE_18__["CreatePatientComponent"]
            },
            {
                path: 'patient/edit/:id',
                component: _patient_edit_patient_edit_patient_component__WEBPACK_IMPORTED_MODULE_20__["EditPatientComponent"]
            },
            {
                path: 'patients',
                component: _patient_patient_list_patient_list_component__WEBPACK_IMPORTED_MODULE_19__["PatientListComponent"]
            },
            {
                path: 'patient/log/:id',
                component: _patient_patient_log_patient_log_component__WEBPACK_IMPORTED_MODULE_26__["PatientLogComponent"]
            },
            {
                path: 'settings',
                component: _settings_settings_component__WEBPACK_IMPORTED_MODULE_22__["SettingsComponent"]
            },
            {
                path: 'manage-labs',
                component: _manage_labs_manage_labs_component__WEBPACK_IMPORTED_MODULE_23__["ManageLabsComponent"]
            }
        ]
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_24__["LoginComponent"]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/services/dashboard.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/dashboard.service.ts ***!
  \***********************************************/
/*! exports provided: DashboardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardService", function() { return DashboardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var DashboardService = /** @class */ (function () {
    function DashboardService(apiUrl, http) {
        this.apiUrl = apiUrl;
        this.http = http;
    }
    DashboardService.prototype.getSummary = function () {
        return this.http.get(this.apiUrl + 'dashboard-summary');
    };
    DashboardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DashboardService);
    return DashboardService;
}());



/***/ }),

/***/ "./src/app/services/radiological-request.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/radiological-request.service.ts ***!
  \**********************************************************/
/*! exports provided: RadiologicalRequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RadiologicalRequestService", function() { return RadiologicalRequestService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var RadiologicalRequestService = /** @class */ (function () {
    function RadiologicalRequestService(apiUrl, http) {
        this.apiUrl = apiUrl;
        this.http = http;
    }
    RadiologicalRequestService.prototype.createRequest = function (request) {
        var formData = new FormData();
        formData.append('request_data', JSON.stringify(request));
        return this.http.post(this.apiUrl + 'radiological-request', formData);
    };
    RadiologicalRequestService.prototype.getRequests = function () {
        return this.http.get(this.apiUrl + 'radiological-request');
    };
    RadiologicalRequestService.prototype.getRequest = function (id) {
        return this.http.get(this.apiUrl + 'radiological-request/' + id);
    };
    RadiologicalRequestService.prototype.updateRequest = function (request) {
        var formData = new FormData();
        formData.append('_method', 'PATCH');
        formData.append('request_data', JSON.stringify(request));
        return this.http.post(this.apiUrl + 'radiological-request/' + request.id, formData);
    };
    RadiologicalRequestService.prototype.deleteRequest = function (id) {
        var formData = new FormData();
        formData.append('_method', "DELETE");
        return this.http.post(this.apiUrl + 'radiological-request/' + id, formData);
    };
    RadiologicalRequestService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], RadiologicalRequestService);
    return RadiologicalRequestService;
}());



/***/ }),

/***/ "./src/app/settings/setting.service.ts":
/*!*********************************************!*\
  !*** ./src/app/settings/setting.service.ts ***!
  \*********************************************/
/*! exports provided: SettingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingService", function() { return SettingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var SettingService = /** @class */ (function () {
    function SettingService(appUrl, http) {
        this.appUrl = appUrl;
        this.http = http;
        this.onSettingsUpdated = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    SettingService.prototype.UpdateSetting = function (formData) {
        return this.http.post(this.appUrl + 'settings/update', formData);
    };
    SettingService.prototype.getSettings = function () {
        return this.http.get(this.appUrl + 'settings');
    };
    SettingService.prototype.emitSettingUpdated = function () {
        this.onSettingsUpdated.emit();
    };
    SettingService.prototype.saveLocalSettings = function (setting) {
        localStorage.setItem('settings', JSON.stringify(setting));
        this.emitSettingUpdated();
    };
    SettingService.prototype.getLocalSettings = function () {
        return JSON.parse(localStorage.getItem('settings'));
    };
    SettingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])('API_URL')),
        __metadata("design:paramtypes", [String, _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SettingService);
    return SettingService;
}());



/***/ }),

/***/ "./src/app/settings/settings.component.html":
/*!**************************************************!*\
  !*** ./src/app/settings/settings.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-title-panel title=\"Settings\">\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/dashboard']\">Dashboard</a></li>\n\t<li class=\"breadcrumb-item\"><a [routerLink]=\"['/settings']\">Settings</a> </li>\n</app-title-panel>\n<div class=\"page-content\">\n\n\t<app-linear-loading  [showLoading]=\"loading\"></app-linear-loading>\n\t<div class=\"card-panel near-loading\">\n\t\t<form (submit)=\"updateSettings($event)\">\n\t\t\t\n\t\t\t<div class=\"lab-info\">\n\t\t\t\t<h5>Labaoratory Information</h5>\n\t\t\t\t<hr/> \n\t\t\t\t<div class=\"\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col m3\">\n\t\t\t\t\t\t\t<div class=\"upload center-align\">\n\t\t\t\t\t\t\t\t<input type=\"file\" name=\"logo\" (change)=\"onImageSelected($event)\" #uploadInput style=\"display:none\" accept=\"image/*\">\n\t\t\t\t\t\t\t\t<button type=\"button\" class=\"waves-effect btn-block  waves-light btn\" (click)=\"showFileDialog()\"  #uploadBtn><i class=\"material-icons left\">file_upload</i>Upload Logo</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"logo center-align z-depth-2\">\n\t\t\t\t\t\t\t\t<img #logoView width=\"100px\" [src]=\"logoUrl\" >\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<div class=\"col m9\">\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"lab-name\" id=\"lab-name\" [formControl]=\"labName\">\n\t\t\t\t\t\t\t\t\t\t<label for=\"lab-name\"  >Laboratory Name</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"sub-name\" [formControl]=\"subName\"  id=\"lab-tagline\" >\n\t\t\t\t\t\t\t\t\t\t<label for=\"lab-tagline\">Laboratory sub name</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"phone-no\"  id=\"phone_no\" [formControl]=\"phoneNo\" >\n\t\t\t\t\t\t\t\t\t\t<label for=\"phone-no\">Phone Numbers</label>\n\t\t\t\t\t\t\t\t\t\t<span class=\"helper-text\">Comma separated if more than one</span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t<div class=\"col m6\">\n\t\t\t\t\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"email\"  id=\"email\" [formControl]=\"email\" >\n\t\t\t\t\t\t\t\t\t\t<label for=\"email\">Email</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t<div class=\"col m12\">\n\t\t\t\t\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"address\"  id=\"lab-address\" [formControl]=\"labAddress\">\n\t\t\t\t\t\t\t\t\t\t<label for=\"lab-address\">Laboratory Address</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"app-setting\">\n\t\t\t\t<h5>App Settings</h5>\n\t\t\t\t<hr/>\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col m4\">\n\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t<div class=\"input-field\">\n\t\t\t\t\t\t\t\t<select name=\"after-save-action\" [formControl]=\"saveAction\">\n\t\t\t\t\t\t\t\t\t<option  disabled selected>Choose your option</option>\n\t\t\t\t\t\t\t\t\t<option [value]=\"afterSaveAction.EMAIL_PATIENT\">Email Patient report</option>\n\t\t\t\t\t\t\t\t\t<option [value]=\"afterSaveAction.SMS_PATIENT\">Notify Patient Via SMS </option>\n\t\t\t\t\t\t\t\t\t<option [value]=\"afterSaveAction.EMAIL_AND_SMS_PATIENT\">Email and SMS Patient</option>\n\t\t\t\t\t\t\t\t\t<option [value]=\"afterSaveAction.ALWAYS_ASK\">Always Ask</option>\n\t\t\t\t\t\t\t\t\t<option [value]=\"afterSaveAction.DO_NOTHING\">Do nothing</option>\n\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t<label>What Should Happen when Report is Saved?</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"col m4 offset-m4\">\n\t\t\t\t\t\t<button class=\"btn btn-large waves-effect waves-light\" [disabled]=\"loading\">Update Settings</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/settings/settings.component.scss":
/*!**************************************************!*\
  !*** ./src/app/settings/settings.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".logo {\n  margin-top: 10px !important; }\n\n.upload .btn {\n  display: block;\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/settings/settings.component.ts":
/*!************************************************!*\
  !*** ./src/app/settings/settings.component.ts ***!
  \************************************************/
/*! exports provided: SettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsComponent", function() { return SettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/AfterSaveActions */ "./src/app/shared/AfterSaveActions.ts");
/* harmony import */ var _setting_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./setting.service */ "./src/app/settings/setting.service.ts");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(serviceProvider) {
        this.serviceProvider = serviceProvider;
        this.loading = false;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_4__["default"]();
        this.labName = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.subName = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.saveAction = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.phoneNo = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.labAddress = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.logoUrl = '';
        this.afterSaveAction = _shared_AfterSaveActions__WEBPACK_IMPORTED_MODULE_2__["AfterSaveActions"];
    }
    SettingsComponent.prototype.ngOnInit = function () {
        this.getSettings();
    };
    SettingsComponent.prototype.ngAfterViewInit = function () {
        this.initSelect();
    };
    SettingsComponent.prototype.initSelect = function () {
        var elems = document.querySelectorAll('select');
        M.FormSelect.init(elems);
    };
    SettingsComponent.prototype.showFileDialog = function () {
        this.uploadInput.nativeElement.click();
    };
    SettingsComponent.prototype.onImageSelected = function (event) {
        var _this = this;
        var files = event.target.files;
        if (files.length > 0) {
            if (files[0].type.startsWith('image/')) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var ev = e;
                    _this.logoView.nativeElement.src = ev.target.result;
                };
                reader.readAsDataURL(files[0]);
            }
        }
    };
    SettingsComponent.prototype.updateSettings = function (event) {
        var _this = this;
        this.loading = true;
        var formData = new FormData(event.target);
        this.serviceProvider.UpdateSetting(formData).subscribe(function (data) {
            _this.loading = false;
            _this.toast.success("Setting was updated successfully");
            _this.getSettings();
        }, function (error) {
            _this.loading = false;
            if (error.status == 0) {
                _this.toast.error("Can't connect to the server please try again");
            }
            else {
                _this.toast.error("An error occured Settings  could not be saved, please try again");
            }
        });
    };
    SettingsComponent.prototype.getSettings = function () {
        var _this = this;
        this.loading = true;
        this.serviceProvider.getSettings().subscribe(function (data) {
            _this.loading = false;
            var d = data;
            _this.labName.patchValue(d.lab_name);
            _this.subName.patchValue(d.sub_name);
            _this.saveAction.patchValue(d.after_save_action);
            _this.labAddress.patchValue(d.address);
            _this.phoneNo.patchValue(d.phone_no);
            _this.email.patchValue(d.email);
            _this.logoUrl = d.logo_file;
            _this.loading = false;
            _this.serviceProvider.saveLocalSettings(data);
            M.updateTextFields();
            _this.initSelect();
        }, function (error) {
            _this.getSettings();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('uploadInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SettingsComponent.prototype, "uploadInput", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('logoView'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], SettingsComponent.prototype, "logoView", void 0);
    SettingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-settings-component',
            template: __webpack_require__(/*! ./settings.component.html */ "./src/app/settings/settings.component.html"),
            styles: [__webpack_require__(/*! ./settings.component.scss */ "./src/app/settings/settings.component.scss")]
        }),
        __metadata("design:paramtypes", [_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"]])
    ], SettingsComponent);
    return SettingsComponent;
}());



/***/ }),

/***/ "./src/app/shared/AfterSaveActions.ts":
/*!********************************************!*\
  !*** ./src/app/shared/AfterSaveActions.ts ***!
  \********************************************/
/*! exports provided: AfterSaveActions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AfterSaveActions", function() { return AfterSaveActions; });
var AfterSaveActions;
(function (AfterSaveActions) {
    AfterSaveActions[AfterSaveActions["EMAIL_PATIENT"] = 1] = "EMAIL_PATIENT";
    AfterSaveActions[AfterSaveActions["SMS_PATIENT"] = 2] = "SMS_PATIENT";
    AfterSaveActions[AfterSaveActions["EMAIL_AND_SMS_PATIENT"] = 3] = "EMAIL_AND_SMS_PATIENT";
    AfterSaveActions[AfterSaveActions["ALWAYS_ASK"] = 4] = "ALWAYS_ASK";
    AfterSaveActions[AfterSaveActions["DO_NOTHING"] = 5] = "DO_NOTHING";
})(AfterSaveActions || (AfterSaveActions = {}));


/***/ }),

/***/ "./src/app/shared/abstract-pane-component.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/abstract-pane-component.ts ***!
  \***************************************************/
/*! exports provided: AbstractPaneComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractPaneComponent", function() { return AbstractPaneComponent; });
var AbstractPaneComponent = /** @class */ (function () {
    function AbstractPaneComponent() {
        this.activePane = 0;
        this.showNextBtn = true;
        this.showPrevBtn = false;
    }
    AbstractPaneComponent.prototype.nextPane = function () {
        this.activePane++;
        this.showPane();
    };
    AbstractPaneComponent.prototype.prevPane = function () {
        this.activePane--;
        this.showPane();
    };
    return AbstractPaneComponent;
}());



/***/ }),

/***/ "./src/app/shared/abstract-report-form.ts":
/*!************************************************!*\
  !*** ./src/app/shared/abstract-report-form.ts ***!
  \************************************************/
/*! exports provided: AbstractReportForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractReportForm", function() { return AbstractReportForm; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _abstract_pane_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./abstract-pane-component */ "./src/app/shared/abstract-pane-component.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AbstractReportForm = /** @class */ (function (_super) {
    __extends(AbstractReportForm, _super);
    function AbstractReportForm(router, patientService, activeRoute) {
        var _this = _super.call(this) || this;
        _this.router = router;
        _this.patientService = patientService;
        _this.activeRoute = activeRoute;
        _this.saveBtnText = "Save Report";
        _this.loadingPatients = false;
        _this.errorOccurred = false;
        _this.reset = _this.reset.bind(_this);
        console.log(activeRoute.routeConfig.path);
        return _this;
    }
    AbstractReportForm.prototype.ngOnInit = function () {
        this.getPatients();
    };
    AbstractReportForm.prototype.getPatients = function () {
        var _this = this;
        this.loadingPatients = true;
        this.errorOccurred = false;
        setTimeout(function () { _this.initMaterial(); }, 200);
        this.patientService.getPatients().subscribe(function (patients) {
            _this.patients = patients;
            _this.loadingPatients = false;
            setTimeout(function () { _this.initMaterial(); }, 200);
        }, function (error) {
            _this.loadingPatients = false;
            _this.errorOccurred = true;
            setTimeout(function () { _this.initMaterial(); }, 200);
        });
    };
    AbstractReportForm.prototype.initMaterial = function () {
        var elems = document.querySelectorAll('select');
        M.FormSelect.init(elems);
    };
    AbstractReportForm.prototype.ngAfterViewChecked = function () {
        M.updateTextFields();
    };
    AbstractReportForm.prototype.ngAfterViewInit = function () {
        this.initMaterial();
    };
    AbstractReportForm.prototype.onPatientSelected = function (event) {
        if (event.target.value == 'new') {
            this.router.navigateByUrl('/patient/new?r=/' + this.activeRoute.routeConfig.path);
        }
        else if (event.target.value == 'retry') {
            this.getPatients();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], AbstractReportForm.prototype, "saveBtnText", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], AbstractReportForm.prototype, "saving", void 0);
    return AbstractReportForm;
}(_abstract_pane_component__WEBPACK_IMPORTED_MODULE_1__["AbstractPaneComponent"]));



/***/ }),

/***/ "./src/app/shared/abstract-table-component.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/abstract-table-component.ts ***!
  \****************************************************/
/*! exports provided: AbstractTableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AbstractTableComponent", function() { return AbstractTableComponent; });
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _MiniToast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../MiniToast */ "./src/MiniToast.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AbstractTableComponent = /** @class */ (function () {
    function AbstractTableComponent() {
        this.conError = false;
        this.serverError = false;
        this.matDatasource = new _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatTableDataSource"]();
        this.roar = roar;
        this.toast = new _MiniToast__WEBPACK_IMPORTED_MODULE_2__["default"]();
    }
    Object.defineProperty(AbstractTableComponent.prototype, "content", {
        set: function (content) {
            this.sort = content;
            if (this.sort) {
                this.matDatasource.sort = this.sort;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AbstractTableComponent.prototype, "pager", {
        set: function (content) {
            this.paginator = content;
            if (this.paginator) {
                this.matDatasource.paginator = this.paginator;
            }
        },
        enumerable: true,
        configurable: true
    });
    AbstractTableComponent.prototype.filterTable = function (searchText) {
        this.matDatasource.filter = searchText.trim().toLowerCase();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatPaginator"])
    ], AbstractTableComponent.prototype, "paginator", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSort"])
    ], AbstractTableComponent.prototype, "sort", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSort"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSort"]),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSort"]])
    ], AbstractTableComponent.prototype, "content", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatPaginator"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatPaginator"]),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatPaginator"]])
    ], AbstractTableComponent.prototype, "pager", null);
    return AbstractTableComponent;
}());



/***/ }),

/***/ "./src/app/shared/connection-error/connection-error.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/shared/connection-error/connection-error.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-panel\" *ngIf=\"show\">\n\t<div class=\"center-align\">\n\t\t<p>Can't connect to server at the moment please try again </p>\n\t\t <button class=\"btn\" (click)=\"retry()\">Retry</button>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/shared/connection-error/connection-error.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/shared/connection-error/connection-error.component.ts ***!
  \***********************************************************************/
/*! exports provided: ConnectionErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionErrorComponent", function() { return ConnectionErrorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ConnectionErrorComponent = /** @class */ (function () {
    function ConnectionErrorComponent() {
    }
    ConnectionErrorComponent.prototype.ngOnInit = function () {
    };
    ConnectionErrorComponent.prototype.retry = function () {
        if (this.retryCb)
            this.retryCb();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ConnectionErrorComponent.prototype, "retryCb", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], ConnectionErrorComponent.prototype, "show", void 0);
    ConnectionErrorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'connection-error',
            template: __webpack_require__(/*! ./connection-error.component.html */ "./src/app/shared/connection-error/connection-error.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], ConnectionErrorComponent);
    return ConnectionErrorComponent;
}());



/***/ }),

/***/ "./src/app/shared/linear-loading/linear-loading.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/shared/linear-loading/linear-loading.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"showLoading\" class=\"progress\">\n         <div class=\"indeterminate\"></div>\n</div>"

/***/ }),

/***/ "./src/app/shared/linear-loading/linear-loading.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/linear-loading/linear-loading.component.ts ***!
  \*******************************************************************/
/*! exports provided: LinearLoadingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinearLoadingComponent", function() { return LinearLoadingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LinearLoadingComponent = /** @class */ (function () {
    function LinearLoadingComponent() {
    }
    LinearLoadingComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], LinearLoadingComponent.prototype, "showLoading", void 0);
    LinearLoadingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-linear-loading',
            template: __webpack_require__(/*! ./linear-loading.component.html */ "./src/app/shared/linear-loading/linear-loading.component.html"),
            styles: [".progress{margin-bottom:0}"]
        }),
        __metadata("design:paramtypes", [])
    ], LinearLoadingComponent);
    return LinearLoadingComponent;
}());



/***/ }),

/***/ "./src/app/shared/loading/loading.component.html":
/*!*******************************************************!*\
  !*** ./src/app/shared/loading/loading.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-panel\" *ngIf=\"showLoading\">\n    <div class=\"center-align\" >\n        <div class=\"preloader-wrapper big active spinner\">\n            <div class=\"spinner-layer spinner-blue-only\">\n                <div class=\"circle-clipper left\">\n                    <div class=\"circle\"></div>\n                </div><div class=\"gap-patch\">\n                <div class=\"circle\"></div>\n            </div><div class=\"circle-clipper right\">\n            <div class=\"circle\"></div>\n        </div>\n    </div>\n</div>\n<p>{{loadingInfo}}</p>\n</div>\n</div>"

/***/ }),

/***/ "./src/app/shared/loading/loading.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/shared/loading/loading.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".info-text {\n  font-size: 1.2em; }\n\n.spinner {\n  margin: auto; }\n"

/***/ }),

/***/ "./src/app/shared/loading/loading.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/shared/loading/loading.component.ts ***!
  \*****************************************************/
/*! exports provided: LoadingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingComponent", function() { return LoadingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadingComponent = /** @class */ (function () {
    function LoadingComponent() {
        this.loadingInfo = "Loading content please wait";
    }
    LoadingComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], LoadingComponent.prototype, "showLoading", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], LoadingComponent.prototype, "loadingInfo", void 0);
    LoadingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-loading',
            template: __webpack_require__(/*! ./loading.component.html */ "./src/app/shared/loading/loading.component.html"),
            styles: [__webpack_require__(/*! ./loading.component.scss */ "./src/app/shared/loading/loading.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LoadingComponent);
    return LoadingComponent;
}());



/***/ }),

/***/ "./src/app/shared/modal/modal.component.html":
/*!***************************************************!*\
  !*** ./src/app/shared/modal/modal.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"m-modal\" #modal (click)=\"onCancel()\">\n\t<div class=\"wrapper animated  zoomIn\">\n\t\t<div class=\"content \" (click)=\"$event.stopPropagation()\">\n\t\t\t<div class=\"header\">\n\t\t\t\t<h5>Notify Patient</h5>\n\t\t\t</div>\n\t\t\t<div class=\"body\">\n\t\t\t\t<p>\n\t\t\t      <label>\n\t\t\t        <input type=\"checkbox\" value=\"sms\"  [formControl]=\"sms\" />\n\t\t\t        <span>Notify  Patient Via SMS</span>\n\t\t\t      </label>\n\t\t\t    </p>\n\n\t\t\t    <p>\n\t\t\t      <label>\n\t\t\t        <input type=\"checkbox\" value=\"email\"  [formControl]=\"email\"/>\n\t\t\t        <span>Email Patient Report</span>\n\t\t\t      </label>\n\t\t\t    </p>\n\n\t\t\t</div>\n\t\t\t<div class=\"footer\">\n\t\t\t\t<div class=\"btns\">\n\t\t\t\t\t<button (click)=\"onCancel()\">Cancel</button>\n\t\t\t\t\t<button (click)=\"onContinue()\">Continue</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/shared/modal/modal.component.scss":
/*!***************************************************!*\
  !*** ./src/app/shared/modal/modal.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".m-modal {\n  position: absolute;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  background: rgba(128, 128, 128, 0.2);\n  z-index: 1000;\n  display: none; }\n\n.wrapper {\n  height: 100%;\n  width: 100%;\n  padding-top: 100px; }\n\n.wrapper .content {\n    padding: 20px;\n    background: white;\n    width: 400px;\n    height: 300px;\n    margin: auto;\n    position: relative; }\n\n.btns {\n  display: flex;\n  justify-content: space-between;\n  position: absolute;\n  bottom: 0;\n  width: 90%;\n  padding-bottom: 10px; }\n\n.btns button {\n    background: none;\n    border: none;\n    font-size: 20px;\n    padding: 20px;\n    font-weight: 500;\n    color: #30aadc;\n    cursor: pointer; }\n"

/***/ }),

/***/ "./src/app/shared/modal/modal.component.ts":
/*!*************************************************!*\
  !*** ./src/app/shared/modal/modal.component.ts ***!
  \*************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalComponent = /** @class */ (function () {
    function ModalComponent() {
        this.sms = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('');
    }
    ModalComponent.prototype.ngOnInit = function () {
    };
    ModalComponent.prototype.openModal = function (continueCb) {
        this.continueCb = continueCb;
        this.modal.nativeElement.style.display = 'block';
    };
    ModalComponent.prototype.closeModal = function () {
        this.modal.nativeElement.style.display = 'none';
    };
    ModalComponent.prototype.onContinue = function () {
        this.closeModal();
        var options = {
            sms: this.sms.value ? true : false,
            email: this.email.value ? true : false
        };
        if (this.continueCb) {
            this.continueCb(options);
        }
    };
    ModalComponent.prototype.onCancel = function () {
        this.closeModal();
        if (this.continueCb) {
            this.continueCb();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('modal'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], ModalComponent.prototype, "modal", void 0);
    ModalComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! ./modal.component.html */ "./src/app/shared/modal/modal.component.html"),
            styles: [__webpack_require__(/*! ./modal.component.scss */ "./src/app/shared/modal/modal.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/shared/no-record.component.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/no-record.component.ts ***!
  \***********************************************/
/*! exports provided: NoRecordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoRecordComponent", function() { return NoRecordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NoRecordComponent = /** @class */ (function () {
    function NoRecordComponent() {
    }
    NoRecordComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], NoRecordComponent.prototype, "show", void 0);
    NoRecordComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'no-record',
            template: "\n    <div class=\"card-panel\" *ngIf=\"show\">\n      <div class=\"center-align\">\n        <span><ng-content></ng-content></span>\n      </div>\n    </div>\n  ",
            styles: ["\n  span{\n    font-size:1.2em;\n  }\n\n  .card-panel{\n    padding:60px 0;\n  }\n  "
            ]
        }),
        __metadata("design:paramtypes", [])
    ], NoRecordComponent);
    return NoRecordComponent;
}());



/***/ }),

/***/ "./src/app/shared/report-basic-info/report-basic-info.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/shared/report-basic-info/report-basic-info.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"customer-info animated slideInLeft report-panel\">\n  <div class=\"card-panel\">\n    <div class=\"card-title\">\n      <h5>Basic Information</h5>\n    </div>\n    <div class=\"card-content\">\n      <form [formGroup]=\"basicInfoForm\">\n        <div class=\"row\">\n           <div class=\"input-field\">\n              <input type=\"text\" id=\"lab-no\" formControlName=\"lab_no\">\n              <label for=\"lab-no\"  >Laboratory Number</label>\n            </div>\n          <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"surname\" formControlName=\"surname\">\n              <label for=\"surname\"  >Surname</label>\n            </div>\n          </div>\n          <div class=\"col m2\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"first-name\" formControlName=\"first_name\">\n              <label for=\"first-name\" >First name</label>\n            </div>\n          </div>\n          <div class=\"col m2\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"middle-name\" formControlName=\"middle_name\">\n              <label for=\"middle-name\">Middle name</label>\n            </div>\n          </div>\n          <div class=\"col m2\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"age\" formControlName=\"age\">\n              <label for=\"age\">Age</label>\n            </div>\n          </div>\n          <div class=\"col m3\">\n            <p>Sex</p>\n            <p><label>\n              <input name=\"sex\" type=\"radio\" class=\"with-gap\"  value=\"Male\" formControlName=\"sex\"/>\n              <span>Male</span>\n            </label>\n              <label>\n                <input name=\"sex\" type=\"radio\" class=\"with-gap\"  value=\"Female\" formControlName=\"sex\"/>\n                <span>Female</span>\n              </label>\n            </p>\n          </div>\n          <div class=\"col m3\">\n            <div>\n                <div class=\"input-field\">\n                    <input type=\"text\" id=\"date\" class=\"datepicker\" formControlName=\"date\">\n                    <label for=\"date\" class=\"active\">Date</label>\n                </div>\n            </div>\n          </div>\n          <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"time\" class=\"timepicker\" formControlName=\"time\">\n              <label for=\"time\">Time</label>\n            </div>\n          </div>\n          <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"requested-by\" formControlName=\"requested_by\">\n              <label for=\"requested-by\">Requested By</label>\n            </div>\n          </div>\n          <div class=\"col m3\">\n          <div class=\"input-field\">\n          <input type=\"text\" id=\"email\" formControlName=\"email\">\n          <label for=\"email\">Email</label>\n          </div>\n          </div>\n        </div>\n        <!-- <h6>Clinical Details</h6> -->\n        <div class=\"row\">\n          <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"clinical_detail\" formControlName=\"clinical_detail\">\n              <label for=\"clinical_detail\">Clinical details</label>\n            </div>\n          </div>\n          <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"exam-required\" formControlName=\"exam_required\">\n              <label for=\"exam-required\">Exam required</label>\n            </div>\n          </div>\n          <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"specimen\" formControlName=\"specimen\">\n              <label for=\"specimen\">Specimen</label>\n            </div>\n          </div>\n          <div class=\"col m3\">\n            <div class=\"input-field\">\n              <input type=\"text\" id=\"clinic\" formControlName=\"clinic\">\n              <label for=\"clinic\">Clinic</label>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/shared/report-basic-info/report-basic-info.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/shared/report-basic-info/report-basic-info.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/shared/report-basic-info/report-basic-info.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/shared/report-basic-info/report-basic-info.component.ts ***!
  \*************************************************************************/
/*! exports provided: ReportBasicInfoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportBasicInfoComponent", function() { return ReportBasicInfoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReportBasicInfoComponent = /** @class */ (function () {
    function ReportBasicInfoComponent() {
        this.basicInfoForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            lab_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            surname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            first_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            middle_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            age: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            sex: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            date: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            time: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            requested_by: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            exam_required: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            specimen: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            clinic: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            clinical_detail: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        this.onHidden = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ReportBasicInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.basicInfoForm.valueChanges.subscribe(function (value) {
            if (_this.basicInfo == null) {
                localStorage.setItem(_this.storage, JSON.stringify(value));
            }
        });
    };
    ReportBasicInfoComponent.prototype.ngAfterViewInit = function () {
        this.getStorageValue();
        this.initMaterial();
    };
    ReportBasicInfoComponent.prototype.ngOnChanges = function () {
        this.getStorageValue();
    };
    ReportBasicInfoComponent.prototype.ngOnDestroy = function () {
        var basicInfo = this.basicInfoForm.value;
        if (!this.basicInfo) {
            localStorage.setItem(this.storage, JSON.stringify(basicInfo));
        }
        this.onHidden.emit(basicInfo);
    };
    ReportBasicInfoComponent.prototype.ngAfterViewChecked = function () {
        M.updateTextFields();
    };
    ReportBasicInfoComponent.prototype.getStorageValue = function () {
        if (this.basicInfo != null) {
            this.basicInfoForm.patchValue(__assign({}, this.basicInfo));
        }
        else {
            var info = JSON.parse(localStorage.getItem(this.storage));
            if (info != null) {
                this.basicInfoForm.patchValue(__assign({}, info));
            }
        }
    };
    ReportBasicInfoComponent.prototype.initMaterial = function () {
        M.AutoInit();
        var elems = document.querySelectorAll('.datepicker');
        var that = this;
        M.Datepicker.init(elems, {
            container: document.body,
            format: 'mm/dd/yyyy',
            onClose: function () {
                var date = this.date;
                if (date) {
                    that.basicInfoForm.patchValue({ date: date.toLocaleDateString() });
                }
            }
        });
        var timeElm = document.querySelectorAll('.timepicker');
        M.Timepicker.init(timeElm, {
            container: 'body',
            onCloseEnd: function () {
                that.basicInfoForm.patchValue({ time: this.time });
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ReportBasicInfoComponent.prototype, "storage", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], ReportBasicInfoComponent.prototype, "onHidden", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ReportBasicInfoComponent.prototype, "basicInfo", void 0);
    ReportBasicInfoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-report-basic-info',
            template: __webpack_require__(/*! ./report-basic-info.component.html */ "./src/app/shared/report-basic-info/report-basic-info.component.html"),
            styles: [__webpack_require__(/*! ./report-basic-info.component.scss */ "./src/app/shared/report-basic-info/report-basic-info.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ReportBasicInfoComponent);
    return ReportBasicInfoComponent;
}());



/***/ }),

/***/ "./src/app/shared/server-error/server-error.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/shared/server-error/server-error.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card-panel\" *ngIf=\"show\">\n\t<div class=\"center-align\">\n\t\t<p>An error occured on the server request cant be process, please try again. If error persist contact the app admin </p>\n\t\t <button class=\"btn\" (click)=\"retry()\">Retry</button>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/shared/server-error/server-error.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/shared/server-error/server-error.component.ts ***!
  \***************************************************************/
/*! exports provided: ServerErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServerErrorComponent", function() { return ServerErrorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ServerErrorComponent = /** @class */ (function () {
    function ServerErrorComponent() {
    }
    ServerErrorComponent.prototype.ngOnInit = function () {
    };
    ServerErrorComponent.prototype.retry = function () {
        if (this.retryCb)
            this.retryCb();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], ServerErrorComponent.prototype, "retryCb", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean)
    ], ServerErrorComponent.prototype, "show", void 0);
    ServerErrorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'server-error',
            template: __webpack_require__(/*! ./server-error.component.html */ "./src/app/shared/server-error/server-error.component.html"),
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], ServerErrorComponent);
    return ServerErrorComponent;
}());



/***/ }),

/***/ "./src/app/sidebar/sidebar.component.html":
/*!************************************************!*\
  !*** ./src/app/sidebar/sidebar.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside class=\"sidebar \">\n  <div class=\"brand\">\n    <a [routerLink]=\"['/dashboard']\" class=\"brand-logo\">\n     {{labName}}\n    </a>\n    <p>{{subName}}</p>\n  </div>\n  <div class=\"sidebar-body z-depth-5\">\n    <div class=\"sidebar-time z-depth-2\">\n      <div class=\" animated fadeInDown\">\n        <p class=\"time\">{{hour}}:{{minute}}<small class=\"seconds\">:{{second}}</small> </p>\n        <p class=\"date\">{{day}}, {{month}} {{date}} {{year}}</p>\n      </div>\n    </div>\n    <div class=\"sidebar-nav\">\n      <div class=\"overlay\">\n        <div class=\"wrapper\">\n          <ul class=\"links\">\n            <li>\n              <a [routerLink]=\"['dashboard']\" class=\"waves-effect\"  data-drop-down='dropdown1'>\n                <span class=\"material-icons \">dashboard</span>\n                <span class=\"link-text\"> Dashboard</span>\n                <!--<span class=\"material-icons arrow\">keyboard_arrow_right</span>-->\n              </a>\n            </li>\n            <li class=\"waves-effect\">\n              <a href=\"#\" class=\"dropdown-toggle \"  data-target='patients'>\n                <span class=\"material-icons\">person</span>\n                <span class=\"link-text\"> Patients</span>\n                <span class=\"material-icons\">keyboard_arrow_right</span>\n              </a>\n              <ul id='patients' class='dropdown'>\n                <li><a [routerLink]=\"['patient/new']\" class=\"waves-effect\"> <i class=\"material-icons\">person_add</i> New Patient</a></li>\n                <li><a [routerLink]=\"['patients']\" class=\"waves-effect\"><i class=\"material-icons\">view_headline</i> View Patients</a></li>\n              </ul>\n            </li>\n            <li class=\"waves-effect\">\n              <a href=\"#\" class=\"dropdown-toggle \"  data-target='haematology '>\n                <span class=\"material-icons\">line_style</span>\n                <span class=\"link-text\"> Haematology</span>\n                <span class=\"material-icons\">keyboard_arrow_right</span>\n              </a>\n              <ul id='haematology' class='dropdown'>\n                <li><a [routerLink]=\"['haematology/new']\" class=\"waves-effect\"> <i class=\"material-icons\">add</i> New Report</a></li>\n                <li><a [routerLink]=\"['haematology/reports']\" class=\"waves-effect\"><i class=\"material-icons\">receipt</i> View Reports</a></li>\n              </ul>\n            </li>\n            <li class=\"waves-effect\">\n              <a href=\"#\" data-target=\"pathology\" class=\"dropdown-toggle\">\n                <span class=\"material-icons\">track_changes</span>\n                <span class=\"link-text\"> Chemical Pathology</span>\n                <span class=\"material-icons\">keyboard_arrow_right</span>\n              </a>\n              <ul id='pathology' class='dropdown'>\n                <li><a [routerLink]=\"['chemical-pathology/new']\" class=\"waves-effect\"> <i class=\"material-icons\">add</i> New Report</a></li>\n                <li><a [routerLink]=\"['chemical-pathology/reports']\" class=\"waves-effect\"><i class=\"material-icons\">receipt</i> View Reports</a></li>\n              </ul>\n            </li>\n            <li class=\"waves-effect\">\n              <a href=\"#\" data-target=\"microbiology\"  title=\"Medical Microbiology and Parasitology\" class=\"dropdown-toggle\">\n                <span class=\"material-icons\">track_changes</span>\n                <span class=\"link-text\"> Medical Microbiology </span>\n                <span class=\"material-icons\">keyboard_arrow_right</span>\n              </a>\n              <ul id='microbiology' class='dropdown'>\n                <li><a [routerLink]=\"['medical-microbiology-and-parasitology/new']\" class=\"waves-effect\"> <i class=\"material-icons\">add</i> New Report</a></li>\n                <li><a [routerLink]=\"['medical-microbiology-and-parasitology/reports']\" class=\"waves-effect\"><i class=\"material-icons\">receipt</i> View Reports</a></li>\n              </ul>\n            </li>\n           <!--  <li class=\"waves-effect\">\n              <a  href=\"#\" data-target=\"radiology\" class=\"dropdown-toggle \">\n                <span class=\"material-icons\">donut_small</span>\n                <span class=\"link-text\"> Radiological Request</span>\n                <span class=\"material-icons\">keyboard_arrow_right</span>\n              </a>\n              <ul id='radiology' class='dropdown'>\n                <li><a [routerLink]=\"['radiological-request/new']\" class=\"waves-effect\"> <i class=\"material-icons\">add</i> New request</a></li>\n                <li><a [routerLink]=\"['radiological-request/requests']\" class=\"waves-effect\"><i class=\"material-icons\">receipt</i> View requests</a></li>\n              </ul>\n            </li> -->\n\n           <li *ngIf=\"isAdmin\">\n              <a [routerLink]=\"['/manage-labs']\" class=\"waves-effect\"  data-drop-down='dropdown1'>\n                <span class=\"material-icons \">local_hospital</span>\n                <span class=\"link-text\"> Manage Labs</span>\n                <!--<span class=\"material-icons arrow\">keyboard_arrow_right</span>-->\n              </a>\n            </li>\n\n            <li  *ngIf=\"isAdmin\">\n              <a [routerLink]=\"['/settings']\" class=\"waves-effect\"  data-drop-down='dropdown1'>\n                <span class=\"material-icons \">settings</span>\n                <span class=\"link-text\"> Settings</span>\n                <!--<span class=\"material-icons arrow\">keyboard_arrow_right</span>-->\n              </a>\n            </li>\n          </ul>\n        </div>\n      </div>\n    </div>\n  </div>\n</aside>"

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.scss":
/*!************************************************!*\
  !*** ./src/app/sidebar/sidebar.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/sidebar/sidebar.component.ts":
/*!**********************************************!*\
  !*** ./src/app/sidebar/sidebar.component.ts ***!
  \**********************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _settings_setting_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../settings/setting.service */ "./src/app/settings/setting.service.ts");
/* harmony import */ var _Auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Auth/auth.service */ "./src/app/Auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SidebarComponent = /** @class */ (function () {
    function SidebarComponent(setting, authService) {
        this.setting = setting;
        this.authService = authService;
        this.isAdmin = false;
        this.labName = '';
        this.subName = '';
        this.authService.onLogIn.subscribe(function () {
            alert('log in');
        });
    }
    SidebarComponent.prototype.updateAdmin = function () {
        if (this.authService) {
            this.isAdmin = this.authService.isAdmin();
        }
    };
    SidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.showTime();
        this.getSetting();
        this.setting.onSettingsUpdated.subscribe(function () {
            _this.getSetting();
        });
    };
    SidebarComponent.prototype.getSetting = function () {
        var set = this.setting.getLocalSettings();
        if (set) {
            this.labName = set.lab_name;
            this.subName = set.sub_name;
        }
    };
    SidebarComponent.prototype.ngAfterViewInit = function () {
        var drops = document.querySelectorAll('.dropdown-toggle');
        var that = this;
        [].map.call(drops, function (dropdown) {
            dropdown.onclick = function (event) {
                event.preventDefault();
                var trigger = event.currentTarget;
                var target = document.querySelector('#' + trigger.dataset.target);
                if (target.style.display === 'block') {
                    trigger.classList.remove('active');
                    target.style.display = 'none';
                    trigger.lastElementChild.textContent = 'keyboard_arrow_right';
                }
                else {
                    that.hideDropdowns(drops);
                    target.style.display = 'block';
                    trigger.classList.add('active');
                    trigger.lastElementChild.textContent = 'keyboard_arrow_down';
                }
            };
        });
    };
    SidebarComponent.prototype.hideDropdowns = function (drops) {
        [].map.call(drops, function (drop) {
            var target = document.querySelector('#' + drop.dataset.target);
            if (target.style.display === 'block') {
                drop.classList.remove('active');
                drop.lastElementChild.textContent = 'keyboard_arrow_right';
                target.style.display = 'none';
            }
        });
    };
    SidebarComponent.prototype.showTime = function () {
        var _this = this;
        setInterval(function () {
            _this.updateAdmin();
            _this.second = moment__WEBPACK_IMPORTED_MODULE_1__().format('ss');
            _this.minute = moment__WEBPACK_IMPORTED_MODULE_1__().format('mm');
            _this.hour = moment__WEBPACK_IMPORTED_MODULE_1__().subtract({ hour: 1 }).format('hh');
            _this.day = moment__WEBPACK_IMPORTED_MODULE_1__().format('dddd');
            _this.month = moment__WEBPACK_IMPORTED_MODULE_1__().format('MMMM');
            _this.date = moment__WEBPACK_IMPORTED_MODULE_1__().format('Do');
            _this.year = moment__WEBPACK_IMPORTED_MODULE_1__().format('YYYY');
        }, 500);
    };
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [_settings_setting_service__WEBPACK_IMPORTED_MODULE_2__["SettingService"], _Auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/title-panel/title-panel.component.ts":
/*!******************************************************!*\
  !*** ./src/app/title-panel/title-panel.component.ts ***!
  \******************************************************/
/*! exports provided: TitlePanelComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitlePanelComponent", function() { return TitlePanelComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TitlePanelComponent = /** @class */ (function () {
    function TitlePanelComponent() {
    }
    TitlePanelComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], TitlePanelComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], TitlePanelComponent.prototype, "breadcrumb", void 0);
    TitlePanelComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-title-panel',
            template: "\n      <div class=\"card-panel app-title animated slideInDown fast\">\n          <div class=\"card-title\">\n              <h5 class=\"title\">{{ title }}</h5>\n          </div>\n          <div class=\"card-content\">\n              <ul class=\"breadcrumbs\">\n                  <ng-content></ng-content>\n              </ul>\n          </div>\n      </div>\n  ",
            styles: []
        }),
        __metadata("design:paramtypes", [])
    ], TitlePanelComponent);
    return TitlePanelComponent;
}());



/***/ }),

/***/ "./src/assets/js/minitoast.js":
/*!************************************!*\
  !*** ./src/assets/js/minitoast.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

!function(t){var e={};function n(i){if(e[i])return e[i].exports;var o=e[i]={i:i,l:!1,exports:{}};return t[i].call(o.exports,o,o.exports,n),o.l=!0,o.exports}n.m=t,n.c=e,n.d=function(t,e,i){n.o(t,e)||Object.defineProperty(t,e,{configurable:!1,enumerable:!0,get:i})},n.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return n.d(e,"a",e),e},n.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},n.p="",n(n.s=1)}([function(t,e,n){"use strict";Object.defineProperty(e,"__esModule",{value:!0});var i=function(){function t(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,n,i){return n&&t(e.prototype,n),i&&t(e,i),e}}();var o=function(){function t(){!function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}(this,t),this.notif={timeout:5e3},this.msgs={s:["","Success","mt-success"],w:["","Warning","mt-warning"],e:["","Error","mt-error"],i:["","Info","mt-info"],d:["","Notification","mt-default"]},this.init()}return i(t,[{key:"init",value:function(){var t=document.getElementById("mt-cont");t||((t=document.createElement("div")).id="mt-cont",t.classList.add("mt-cont"),document.body.appendChild(t)),this.cont=t}},{key:"success",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;this.append(t||this.msgs.s[0],"s")}},{key:"warning",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;this.append(t||this.msgs.w[0],"w")}},{key:"error",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;this.append(t||this.msgs.e[0],"e")}},{key:"info",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;this.append(t||this.msgs.i[0],"i")}},{key:"default",value:function(){var t=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null;this.append(t||this.msgs.d[0],"d")}},{key:"append",value:function(t,e){var n=this.notif,i=this.msgs,o=document.createElement("div");o.classList.add(i[e][2],"mt-notif","mt-slide-fade"),o.style.animationDuration=n.timeout/1e3+"s";var r=document.createElement("p");r.innerText=i[e][1],o.appendChild(r);var u=document.createElement("p");u.innerText=t,o.appendChild(u),document.getElementById("mt-cont").appendChild(o),setTimeout(function(){document.getElementById("mt-cont").removeChild(document.getElementById("mt-cont").firstChild)},n.timeout)}}]),t}();e.default=o},function(t,e,n){n(0),n(2),t.exports=n(3)},function(t,e){},function(t,e){}]);

/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! c:\SAP\Lab\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map